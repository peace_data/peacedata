require('../../bootstrap');

window.Vue = require('vue');
window.axios = require('axios');

import store from './store'


// import Search from '../../components/public/products/search.vue'
import Search from '../../components/public/products/textarea_search.vue'
import Cart from '../../components/public/products/cart.vue'

const app = new Vue({
    el: '#product-search',
    store,
    components: {
        'cart': Cart,
        'search_product': Search,
    },

});
