import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import base_request from '../../api/base_request';

Vue.use(Vuex)

import merge from 'deepmerge'
// import { stat } from 'fs';

const store = new Vuex.Store({
    modules:{
        cart: {
            state: {
                preOrderProduct: { },
                cheapest: [],
                healthy: [],
                preOrderFilters: { },
                choosedProducts: [],
            },
            actions: {
                addProduct(context, payload){
                    base_request.retrieveData('trade_items_search/list', '', {
                        params: payload
                    }).then(response => {
                        context.commit('addToCarts', response)
                    })
                }
            },
            mutations: {
                addToCarts(state, product){
                    state.choosedProducts.push(product)
                },
                 clearCart(state){
                    state.choosedProducts = [];
                 }
            },
            getters:  {

                choosedProducts: (state) => {
                    return state.choosedProducts;
                },
                healthiests:(state)=>{
                    return state.choosedProducts.map(item => item.healthiest)
                },
                chipiests:(state)=>{
                    return state.choosedProducts.map(item => item.chipiest)
                }
                
                
            }
        }
        
    }
  
})
export default store