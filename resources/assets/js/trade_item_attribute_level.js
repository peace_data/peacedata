require('./bootstrap');

window.Vue = require('vue');
window.axios = require('axios');

import tradeItemAttributeLevel from './components/tradeItem/attribute_levels/List.vue'

const app = new Vue({
    el: '#list_trade_item_attribute_level',
    components: {
        'trade_item_attribute_level': tradeItemAttributeLevel
    },
});
