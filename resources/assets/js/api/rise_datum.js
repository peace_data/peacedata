import * as puth from './puth'


export default {
    getRiseDatumForAssignedValidoItems (callback) {
        axios.get(puth.GET_RISE_DATUM_FOR_ASSIGNED_VALIDOO_ITEMS)
            .then(response => {
                callback(response.data)
            });
    },
}