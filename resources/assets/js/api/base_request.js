export default {
  retrieveData(path, callback, opts){
    var params = {}
    if(opts && opts.params){
      params = opts.params;
    }
    return axios.get(path, {params: params})
      .then(response => {
          return response.data;
      });
  },
  saveData(args){
    return axios.post(args.url, args.data).then(response => {
      return response.data
    })
  }
}
