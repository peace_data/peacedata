import * as puth from './puth'


export default {
    getValidooItems (callback) {
        axios.get(puth.GET_VALIDOO_ITEMS_FOR_ASSIGN_CATEGORY_LEVEL + window.location.search)
            .then(response => {
                callback(response.data)
            });
    },
    saveProductCatalogoryLevel(callback, form){
        axios.post(puth.SAVE_PRODUCT_CATEGORY_LEVEL, form)
            .then(response => {
                callback(response)
            });
    }
}