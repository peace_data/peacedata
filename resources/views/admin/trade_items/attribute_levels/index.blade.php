@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <h2>List of Trade Items</h2>
        </div>

        <div class="card" >
            <div id="list_trade_item_attribute_level">
                <trade_item_attribute_level_attributes>

                </trade_item_attribute_level_attributes>
            </div>
             <div class="card-footer pagination-wrapper text-center">
                <div class="row">
                  <div class="col-md-8 col-sm-6">
                    {!! $tradeItems->appends(['search' => Request::get('search')])->render() !!}
                  </div>
                  @include('admin.blocks.custom_pagination')
                <div>
            </div>
        </div>

    </div>

    <script src="/js/trade_items/attribute_levels.js"></script>
@stop
