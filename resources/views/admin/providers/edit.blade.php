@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <h2>Edit Provider #{{ $provider->id }}</h2>
        </div>
        <div class="card">
            <div class="header text-right">
              @include('admin.blocks.btns.back', [
                'url' => url('/admin/providers')
              ])
            </div>
            <div class="card-body">
                {{--@if ($errors->any())--}}
                    {{--<ul class="alert alert-danger">--}}
                        {{--@foreach ($errors->all() as $error)--}}
                            {{--<li>{{ $error }}</li>--}}
                        {{--@endforeach--}}
                    {{--</ul>--}}
                {{--@endif--}}

                <form method="POST" action="{{ url('/admin/providers/' . $provider->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                    {{ method_field('PATCH') }}
                    {{ csrf_field() }}

                    @include ('admin.providers.form', ['submitButtonText' => 'Update'])

                </form>

            </div>
            <div class="card-footer"><br/></div>
        </div>
    </div>
@endsection
