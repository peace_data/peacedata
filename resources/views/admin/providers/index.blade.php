@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <h2>Providers</h2>
        </div>

        <div class="card">
            <div class="header row">
                <div class='col-sm-11'>
                  @include('admin.blocks.search')
                </div>

                <div class='col-sm-1 text-right'>
                  @include('admin.blocks.btns.add', [
                    'url' => url('/admin/providers/create'),
                    'title' => 'Add New Provider'
                  ])
                </div>

            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th class="sortable-column" data-column-name='title'>Title</th>
                                <th class="sortable-column" data-column-name='url'>Url</th>
                                <th class="sortable-column" data-column-name='crawler_name'>Crawler Name</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($providers as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->title }}</td>
                                <td>{{ $item->url }}</td>
                                <td>{{ $item->crawler_name }}</td>
                                <td class="actions with-three-btns">
                                    @include('admin.blocks.btns.view', [
                                      'url' => url('/admin/providers/' . $item->id),
                                      'title' => 'View Provider'
                                    ])
                                    @include('admin.blocks.btns.edit', [
                                      'url' => url('/admin/providers/' . $item->id . '/edit'),
                                      'title' => 'Edit Provider'
                                    ])
                                    @include('admin.blocks.btns.delete', [
                                      'url' => url('/admin/providers' . '/' . $item->id),
                                      'title' => 'Delete Provider'
                                    ])

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="pagination-wrapper"> {!! $providers->appends(['search' => Request::get('search')])->render() !!} </div>
                </div>
            </div>
        </div>
    </div>
@stop
