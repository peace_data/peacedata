<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    <label for="title" class="col-md-4 control-label">{{ 'Title' }}</label>
    <div class="col-md-6">
      <div class="form-line">
        <input class="form-control" name="title" type="text" id="title" value="{{ $provider->title or ''}}" >
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
</div>
<div class="form-group {{ $errors->has('url') ? 'has-error' : ''}}">
    <label for="url" class="col-md-4 control-label">{{ 'Url' }}</label>
    <div class="col-md-6">
      <div class="form-line">
        <input class="form-control" name="url" type="text" id="url" value="{{ $provider->url or ''}}" >
        {!! $errors->first('url', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
</div>
<div class="form-group {{ $errors->has('crawler_name') ? 'has-error' : ''}}">
    <label for="url" class="col-md-4 control-label">{{ 'Crawler Name' }}</label>
    <div class="col-md-6">
        <div class="form-line">
          <input class="form-control" name="crawler_name" type="text" id="url" value="{{ $provider->crawler_name or ''}}" >
          {!! $errors->first('crawler_name', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText or 'Create' }}">
    </div>
</div>
