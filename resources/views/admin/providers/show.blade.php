@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <h2>Provider {{ $provider->id }}</h2>
        </div>
        <div class="card">
            <div class="header text-right">
              @include('admin.blocks.btns.back', [
                'url' => url('/admin/providers/'),
              ])
              @include('admin.blocks.btns.edit', [
                'url' => url('/admin/providers/' . $provider->id . '/edit'),
                'title' => 'Edit'
              ])
              @include('admin.blocks.btns.delete', [
                'url' => url('/admin/providers' . '/' . $provider->id),
                'title' => 'Delete'
              ])
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                          @foreach($provider->getAttributes() as $key=>$value)
                            <tr>
                                <th>{{ ucfirst(camel_case($key)) }}</th><td>{{ $provider->$key }}</td>
                            </tr>
                            {{--<tr><th> Title </th><td> {{ $provider->title }} </td></tr><tr><th> Url </th><td> {{ $provider->url }} </td></tr>--}}
                        </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
