<div class="form-group {{ $errors->has('global_code') ? 'has-error' : ''}}">
    <label for="global_code" class="col-md-4 control-label">{{ 'Global Code' }}</label>
    <div class="col-md-6">
      <div class="form-line">
        <input class="form-control" name="global_code" type="number" id="global_code" value="{{ $additionalclimatescore->global_code or ''}}" >
        {!! $errors->first('global_code', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
</div>
<div class="form-group {{ $errors->has('global_name') ? 'has-error' : ''}}">
    <label for="global_name" class="col-md-4 control-label">{{ 'Global Name' }}</label>
    <div class="col-md-6">
      <div class="form-line">
        <input class="form-control" name="global_name" type="text" id="global_name" value="{{ $additionalclimatescore->global_name or ''}}" >
        {!! $errors->first('global_name', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
</div>
<div class="form-group {{ $errors->has('region_code') ? 'has-error' : ''}}">
    <label for="region_code" class="control-label col-md-4">{{ 'Region Code' }}</label>
    <div class="col-md-6">
      <div class="form-line">
        <input class="form-control" name="region_code" type="number" id="region_code" value="{{ $additionalclimatescore->region_code or ''}}" >
        {!! $errors->first('region_code', '<p class="help-block">:message</p>') !!}
      </div>
    </div>

</div>
<div class="form-group {{ $errors->has('region_name') ? 'has-error' : ''}}">
    <label for="region_name" class="col-md-4 control-label">{{ 'Region Name' }}</label>
    <div class="col-md-6">
      <div class="form-line">
        <input class="form-control" name="region_name" type="text" id="region_name" value="{{ $additionalclimatescore->region_name or ''}}" >
        {!! $errors->first('region_name', '<p class="help-block">:message</p>') !!}
      </div>
    </div>

</div>
<div class="form-group {{ $errors->has('sub_region_code') ? 'has-error' : ''}}">
    <label for="sub_region_code" class="col-md-4 control-label">{{ 'Sub Region Code' }}</label>
    <div class="col-md-6">
      <div class="form-line">
        <input class="form-control" name="sub_region_code" type="number" id="sub_region_code" value="{{ $additionalclimatescore->sub_region_code or ''}}" >
        {!! $errors->first('sub_region_code', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
</div>
<div class="form-group {{ $errors->has('sub_region_name') ? 'has-error' : ''}}">
    <label for="sub_region_name" class="col-md-4 control-label">{{ 'Sub Region Name' }}</label>
    <div class="col-md-6">
      <div class="form-line">
        <input class="form-control" name="sub_region_name" type="text" id="sub_region_name" value="{{ $additionalclimatescore->sub_region_name or ''}}" >
        {!! $errors->first('sub_region_name', '<p class="help-block">:message</p>') !!}
      </div>
    </div>

</div>
<div class="form-group {{ $errors->has('sub_region_score') ? 'has-error' : ''}}">
    <label for="sub_region_score" class="col-md-4 control-label">{{ 'Sub Region Score' }}</label>
    <div class="col-md-6">
      <div class="form-line">
        <input class="form-control" name="sub_region_score" type="number" id="sub_region_score" value="{{ $additionalclimatescore->sub_region_score or ''}}" >
        {!! $errors->first('sub_region_score', '<p class="help-block">:message</p>') !!}
      </div>
    </div>

</div>
<div class="form-group {{ $errors->has('intermediate_region_code') ? 'has-error' : ''}}">
    <label for="intermediate_region_code" class="col-md-4 control-label">{{ 'Intermediate Region Code' }}</label>
    <div class="col-md-6">
      <div class="form-line">
        <input class="form-control" name="intermediate_region_code" type="number" id="intermediate_region_code" value="{{ $additionalclimatescore->intermediate_region_code or ''}}" >
        {!! $errors->first('intermediate_region_code', '<p class="help-block">:message</p>') !!}
      </div>
    </div>

</div>
<div class="form-group {{ $errors->has('intermediate_region_name') ? 'has-error' : ''}}">
    <label for="intermediate_region_name" class="col-md-4 control-label">{{ 'Intermediate Region Name' }}</label>
    <div class="col-md-6">
      <div class="form-line">
        <input class="form-control" name="intermediate_region_name" type="text" id="intermediate_region_name" value="{{ $additionalclimatescore->intermediate_region_name or ''}}" >
        {!! $errors->first('intermediate_region_name', '<p class="help-block">:message</p>') !!}
      </div>
    </div>

</div>
<div class="form-group {{ $errors->has('coutry_of_area') ? 'has-error' : ''}}">
    <label for="coutry_of_area" class="control-label col-md-4">{{ 'Coutry Or Area' }}</label>
    <div class="col-md-6">
      <div class="form-line">
        <input class="form-control" name="country_or_area" type="text" id="coutry_of_area" value="{{ $additionalclimatescore->country_or_area or ''}}" >
        {!! $errors->first('country_or_area', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
</div>
<div class="form-group {{ $errors->has('m49_code') ? 'has-error' : ''}}">
    <label for="m49_code" class="control-label col-md-4">{{ 'M49 Code' }}</label>
    <div class="col-md-6">
      <div class="form-line">
        <input class="form-control" name="m49_code" type="number" id="m49_code" value="{{ $additionalclimatescore->m49_code or ''}}" >
        {!! $errors->first('m49_code', '<p class="help-block">:message</p>') !!}
      </div>
    </div>

</div>
<div class="form-group {{ $errors->has('iso_alpha3_code') ? 'has-error' : ''}}">
    <label for="iso_alpha3_code" class="col-md-4 control-label">{{ 'Iso Alpha3 Code' }}</label>
    <div class="col-md-6">
      <div class="form-line">
        <input class="form-control" name="iso_alpha3_code" type="text" id="iso_alpha3_code" value="{{ $additionalclimatescore->iso_alpha3_code or ''}}" >
        {!! $errors->first('iso_alpha3_code', '<p class="help-block">:message</p>') !!}
      </div>
    </div>

</div>
<div class="form-group {{ $errors->has('country_score') ? 'has-error' : ''}}">
    <label for="country_score" class="col-md-4 control-label">{{ 'Country Score' }}</label>
    <div class="col-md-6">
      <div class="form-line">
        <input class="form-control" name="country_score" type="number" id="country_score" value="{{ $additionalclimatescore->country_score or ''}}" >
        {!! $errors->first('country_score', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
</div>


<div class="form-group">
    @include('admin.blocks.btns.save')
</div>
