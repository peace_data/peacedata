
@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <h2>Edit AdditionalClimateScore</h2>
        </div>
        <div class="card">
            <div class="header text-right">
                @include('admin.blocks.btns.back', [
                  'url' => url('/admin/additional-climate-scores')
                ])
            </div>
            <div class="card-body">
                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
                {!! Form::model($additionalclimatescore, [
                    'method' => 'PATCH',
                    'url' => ['/admin/additional-climate-scores', $additionalclimatescore->id],
                    'class' => 'form-horizontal',
                    'files' => true
                ]) !!}
                {!! Form::open(['url' => '/admin/additional-climate-scores', 'class' => 'form-horizontal']) !!}

                    @include ('admin.additional-climate-score.form', ['formMode' => 'create', 'submitButtonText' => 'update'])
                {!! Form::close() !!}

                </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
