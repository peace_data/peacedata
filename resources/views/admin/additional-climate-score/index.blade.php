@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <h2>Additionals Climate Scores</h2>
        </div>

        <div class="card">
            <div class="header text-right">
                <div class="row">
                  <div class='col-sm-10'>
                    {{ Form::open(array('route' => 'admin.additional_climate_scores.upload', 'files' => true)) }}

                      <div class='col-sm-4 text-right' style="padding-top: 8px">
                          <h3 class='card-inside-title'>Upload Additional climate data</h3>
                      </div>
                      <div class='col-sm-5'>
                          {{ Form::file('xlsx_file', ['class' => 'btn btn-primary waves-effect']) }}
                      </div>

                      <div class='col-sm-2'>
                          {{  Form::submit('Upload', ['class' => 'btn btn-success  waves-effect']) }}
                      </div>
                    {{ Form::close() }}
                  </div>
                  <div class="col-sm-2">
                    @include('admin.blocks.btns.add', [
                      'url' => url('/admin/additional-climate-scores/create'),
                      'title' => 'Add New Additional Climate Score'
                    ])

                  </div>

                </div>
                <div class="search-form">
                  @include('admin.blocks.search')
                </div>

            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-borderless">
                        <thead>
                          <tr>
                            <th class="sortable-column" data-column-name='id'>#</th>
                            <th class="sortable-column" data-column-name='iso_alpha3_code'>ISO-alpha3 Code</th>
                            <th class="sortable-column" data-column-name='country_or_area'>Country or Area</th>
                            <th class="sortable-column" data-column-name='country_score'>Country score</th>
                            <th class="sortable-column" data-column-name='iso_3166_1'>ISO 3166-1</th>
                            <th class="sortable-column" data-column-name='region_name'>Region Name</th>
                            <th>Actions</th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach($additionalclimatescores as $item)
                          <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->iso_alpha3_code }}</td>
                            <td>{{ $item->country_or_area }}</td>
                            <td>{{ $item->country_score }}</td>
                            <td>{{ $item->iso_3166_1 }}</td>
                            <td>{{ $item->region_name }}</td>
                            <td class="actions with-three-btns">
                                @include('admin.blocks.btns.view', [
                                  'url' => url('/admin/additional-climate-scores/' . $item->id),
                                  'title' => 'View AdditionalClimateScore'
                                ])
                                @include('admin.blocks.btns.edit', [
                                  'url' => url('/admin/additional-climate-scores/' . $item->id . '/edit'),
                                  'title' => 'Edit AdditionalClimateScore'
                                ])
                                @include('admin.blocks.btns.delete', [
                                  'url' => url('/admin/additional-climate-scores' . '/' . $item->id),
                                  'title' => 'Delete AdditionalClimateScore'
                                ])

                            </td>
                          </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="pagination-wrapper">
                      {!! $additionalclimatescores->appends(['search' => Request::get('search')])->render() !!}
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
