
@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <h2>Additional Climate Score {{ $additionalclimatescore->id }}</h2>
        </div>
        <div class="card">
            <div class="header text-right">
              @include('admin.blocks.btns.back', [
                'url' => url('/admin/additional-climate-scores/'),
              ])
              @include('admin.blocks.btns.edit', [
                'url' => url('/admin/additional-climate-scores/' . $additionalclimatescore->id . '/edit'),
                'title' => 'Edit Label'
              ])
              @include('admin.blocks.btns.delete', [
                'url' => url('/admin/additional-climate-scores' . '/' . $additionalclimatescore->id),
                'title' => 'Delete Label'
              ])
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $additionalclimatescore->id }}</td>
                                    </tr>
                                    <tr><th> Global Code </th><td> {{ $additionalclimatescore->global_code }} </td></tr><tr><th> Global Name </th><td> {{ $additionalclimatescore->global_name }} </td></tr><tr><th> Region Code </th><td> {{ $additionalclimatescore->region_code }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
