@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <h2>Labels</h2>
        </div>

        <div class="card">
            <div class="header text-right">
              @include('admin.blocks.btns.add',[
                'url' => url('/admin/labels/create'),
                'title' => 'Add New Label'
              ])
            </div>
            <div class="card-body">
                <div class="table-responsive container-fluid">
                    <div class="table-head">
                      <div class="as-tr row as-thead">
                        <div class="col-sm-1">#</div>
                        <div class="col-sm-2">Title</div>
                        @foreach($labelTypes as $labelType)
                          <div class="col-sm-2">
                            {{$labelType->title}}
                          </div>
                        @endforeach
                        <div class="col-sm-2 actions">Actions</div>
                      </div>
                    </div>
                    <div class="table-body">
                      @foreach($labels as $item)
                          <div class="as-tr row">
                              <div class="col-sm-1">{{ $loop->iteration or $item->id }}</div>
                              <div class="col-sm-2">{{ $item->title }}</div>
                                {!! Form::model($item, [
                                    'method' => 'PATCH',
                                    'url' => ['/admin/labels', $item->id],
                                    'class' => 'form-horizontal',
                                    'files' => true
                                ]) !!}
                                  @foreach($labelTypes as $labelType)
                                    <div class="col-sm-2 ">
                                      <div class="form-group">
                                        <div class="form-line">
                                          <input class="form-control" type="hidden" name="values[{{$loop->iteration}}][label_type_id]" value="{{$labelType->id}}">
                                          <input class="form-control" type="number" step="0.01" name="values[{{$loop->iteration}}][value]" value="{{$item->valueByType($labelType->id)}}">
                                        </div>
                                      </div>
                                    </div>

                                  @endforeach
                                  <div class="col-sm-2">

                                    {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
                                  </div>

                                {!! Form::close() !!}

                              <div class="actions with-three-btns">
                                  @include('admin.blocks.btns.view', [
                                    'url' => url('/admin/labels/' . $item->id),
                                    'title' => 'View Label'
                                  ])
                                  @include('admin.blocks.btns.edit', [
                                    'url' => url('/admin/labels/' . $item->id . '/edit'),
                                    'title' => 'Edit Label'
                                  ])
                                  @include('admin.blocks.btns.delete', [
                                    'url' => url('/admin/labels' . '/' . $item->id),
                                    'title' => 'Delete Label'
                                  ])

                              </div>
                          </div>
                      @endforeach
                    </div>
                      <div class="pagination-wrapper">
                        {!! $labels->appends(['search' => Request::get('search')])->render() !!}
                      </div>
                  </div>

              </div>
        </div>
    </div>
@endsection
