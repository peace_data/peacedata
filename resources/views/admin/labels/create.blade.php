@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <h2>Create New Label</h2>
        </div>
        <div class="card">
            <div class="header text-right">
              @include('admin.blocks.btns.back', [
                'url' => url('/admin/labels')
              ])
            </div>
            <div class="card-body">
                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
                {!! Form::open(['url' => '/admin/labels', 'class' => 'form-horizontal']) !!}

                    @include ('admin.labels.form', ['formMode' => 'create', 'labelTypes' => $labelTypes])
                {!! Form::close() !!}

                </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
