<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
      {!! Form::label('title', 'Title', ['class' => 'col-md-4 control-label']) !!}
      <div class="col-md-6">
        <div class="form-line">
          {!! Form::text('title', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
          {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
        </div>
      </div>

</div>
@foreach($labelTypes as $labelType)
      <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
        <label class="col-md-4 control-label">{{$labelType->title}}</label>
        <div class="col-md-6">
          <div class="form-line">
            
            <input class="form-control" type="hidden" name="values[{{$loop->iteration}}][label_type_id]" value="{{$labelType->id}}">
            <input class="form-control" type="number" step="0.01" name="values[{{$loop->iteration}}][value]" value="{{$label->valueByType($labelType->id)}}">
          </div>
        </div>
      </div>
@endforeach 


<div class="form-group">
    <div class="col-md-offset-4 col-md-8">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>