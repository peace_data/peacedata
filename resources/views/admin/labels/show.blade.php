
@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <h2>Product {{ $label->id }}</h2>
        </div>
        <div class="card">
            <div class="header text-right">
              @include('admin.blocks.btns.back', [
                'url' => url('/admin/labels/'),
              ])
              @include('admin.blocks.btns.edit', [
                'url' => url('/admin/labels/' . $label->id . '/edit'),
                'title' => 'Edit Label'
              ])
              @include('admin.blocks.btns.delete', [
                'url' => url('/admin/labels' . '/' . $label->id),
                'title' => 'Delete Label'
              ])
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-borderless">
                        <tbody>
                            <tr>
                                <th>Title</th>
                                <td>{{ $label->title }}</td>

                            </tr>
                            @foreach($label->values as $value)
                              <tr>
                                <td>{{ $value->labelTypeTitle() }}</td>
                                <td>{{ $value->value }}</td>
                              </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
