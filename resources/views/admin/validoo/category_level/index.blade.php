@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <h2>Category Level of Trade Items</h2>
        </div>

        <div class="card">
            {{--@include('admin.validoo.blocks.uploader')--}}

            <div id="list_validoo_items">
              <validoo_index></validoo_index>
            </div>

           <div class="card-footer pagination-wrapper text-center">

              <div class="row">
                    <div class="col-md-8">
                      {!! $validooItems->appends(['search' => Request::get('search')])->render() !!}
                    </div>
                    @include('admin.blocks.custom_pagination')
              </div>
            </div>
        </div>
    </div>

    <script src="/js/validoo_item_category_level.js"></script>
@stop
