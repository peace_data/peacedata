@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <h2>List of Trade Items</h2>
            <p>TradeItems  {{ $numbersItems['tradeItem'] }}</p>
            <p>Validoo Items  {{ $numbersItems['validooTradeItem'] }}</p>
            <p>Validoo Item Subscriptions {{ $numbersItems['validooTradeItemSubscriptions'] }}</p>
        </div>
        <div class="card">
            @include('admin.validoo.blocks.uploader')
            <div class="body">
                <div class="row clearfix" >
                    <div class="col-md-2 sortable-column" data-column-name='gtin'><strong>GTIN</strong></div>
                    <div class="col-md-1 sortable-column" data-column-name='food'><strong>IsFood</strong></div>
                    <div class="col-md-2 sortable-column" data-column-name='product_name'><strong>Product Name</strong></div>
                    <div class="col-md-2 sortable-column" data-column-name='category'><strong>CategoryCode</strong></div>
                    <div class="col-md-1 sortable-column" data-column-name='brand_name'><strong>Brand</strong></div>
                    <div class="col-md-1"><strong>Category Level</strong></div>
                    <div class="col-md-2"><strong>Attribute Level</strong></div>
                    <div class="col-md-1"><strong>Action</strong></div>
                </div>

                @foreach($tradeItems as $item)
                    <div class= 'row clearfix' :class="{'div-disabled-row ' : !form.product.food }">

                        <div class="demo-checkbox col-md-2">
                            {{ $item->gtin }}
                        </div>
                        <div class="demo-checkbox col-md-1">
                            {{ $item->food ? "Yes" : "No" }}
                        </div>
                        <div class="demo-checkbox col-md-2">
                            {{ $item->product_name }}
                        </div>
                        <div class="demo-checkbox col-md-2">
                            {{ $item->category_code }}
                        </div>
                        <div class="demo-checkbox col-md-1">
                            {{ $item->brand_owner }}
                        </div>
                        <div class="demo-checkbox col-md-1">
                            @if($item->riseData->first() )
                                Assign
                            @else
                                Undefined
                            @endif
                        </div>
                        <div class="demo-checkbox col-md-2">
                            @foreach($item->labelScores() as $labelType => $labelScore)
                              <div>
                                {{$labelType}} : {{$labelScore}}
                              </div>
                            @endforeach
                            <!-- @if(count($item->validooItemAttributtes) > 0 )
                                Assign
                            @else
                                Undefined
                            @endif -->
                        </div>
                        <div class="col-md-1">
                          @include('admin.blocks.btns.view', [
                            'url' => url('/admin/validoo/trade_items/' . $item->id),
                            'title' => "View Product"
                          ])


                        </div>
                    </div>
                @endforeach
                <div class="card-footer pagination-wrapper text-center">
                   <div class="row">
                     <div class="col-md-8 col-sm-6">
                       {!! $tradeItems->appends(['search' => Request::get('search')])->render() !!}
                     </div>
                     @include('admin.blocks.custom_pagination')
                   <div>
               </div>
            </div>
        </div>
    </div>

    <style>
        .header-table{

        }
    </style>
@stop
