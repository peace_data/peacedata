<li><strong>{{$key}}: </strong></li>
<ul>
    @foreach($arr as $valueKey => $valAttr)
        @if(is_array($valAttr))
            @include('admin.validoo.trade_items.product_attr_array', [
                'arr' => $valAttr,
                'key' => $valueKey
            ])
        @else
            <li><strong>{{$valueKey}}: {{$valAttr}}</strong></li>
        @endif
    @endforeach
</ul>
