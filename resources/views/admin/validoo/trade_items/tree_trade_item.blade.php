<style>
    body {
        font-family: Menlo, Consolas, monospace;
        color: #444;
    }
    .item {
        cursor: pointer;
    }
    .bold {
        font-weight: bold;
    }
    ul {
        padding-left: 1em;
        line-height: 1.5em;
        list-style-type: dot;
    }
</style>
<div  class="container-fluid">
    <script type="text/x-template" id="item-template">
        <li v-if="model">
            <div
                    :class="{bold: isFolder}"
                    @click="toggle"
                    @dblclick="changeType">

                @{{ model.class_name }}

                <span v-if="isFolder">[@{{ open ? '-' : '+' }}]</span>
            </div>
            <ul v-show="open" v-if="isFolder">

                <li v-for="(attribute, index) in model.attributes">

                    @{{index + ': ' + attribute}}

                </li>

                <item
                        class="item"
                        v-for="(model, index) in model.children"
                        :key="index.attributes"
                        v-if="isEmptyModel(model)"
                        :model="model">
                </item>
            </ul>
        </li>
    </script>

    <p id="message_validoo">(You can double click on an item to turn it into a folder.)</p>
    <p id="message_validoo_undefined" class="hide">Undefined trade item!</p>

    <!-- the demo root element -->
    <ul id="show_trade_item">
        <item
                class="item"
                :model="treeData"
        >
        </item>
    </ul>
</div>
<script src="https://cdn.jsdelivr.net/npm/vue@2.5.17/dist/vue.js"></script>

<script>
    // define the item component
    Vue.component('item', {
        template: '#item-template',
        props: ['model'],
        data: function () {
            return {
                open: false
            }
        },
        computed: {
            isFolder: function () {
                return Array.isArray(this.model) || (this.model.children && this.model.children.length) || this.model.attributes
            },
        },

        methods: {
            getAttributes: function(){

                var res = {};
                var keys = Object.keys(this.model);

                for (var i = 0; i < keys.length; i++) {
                    res[keys[i]] = this.model[keys[i]];
                }
                return this.attributes;
            },

            toggle: function () {
                if (this.isFolder) {
                    this.open = !this.open
                }
            },
            changeType: function () {
                if (!this.isFolder) {
                    Vue.set(this.model, 'children', [])
                    this.addChild()
                    this.open = true
                }
            },
            addChild: function () {
                this.model.children.push({
                    name: 'new stuff'
                })
            },
            isEmptyModel: function(model){
                return (model.children && model.children.length) || model.attributes
            }

        }
    })
    document.addEventListener("DOMContentLoaded", function(event) {
        $.ajax({
            url: location.href,
            dataType: 'json',
            success: function(response){
                var demo = new Vue({
                    el: '#show_trade_item',
                    data: {
                        treeData: response
                    }
                })
            },
            error: function(res){
                $('#message_validoo').hide();
                $('#message_validoo_undefined').removeClass("hide");
            }
        })
    });

</script>