@extends('admin.layouts.app')

@section('content')
    @foreach($tradeItem->images() as $image)
      <img src="{{$image->middle()}}"></img>
    @endforeach
    
    @include('admin.validoo.trade_items.tree_trade_item')
    <div class="row">
        <div class="col-sm-12"
             id="rise"
             data-rise-data="{{ $tradeItem->riseData->first() }}"
             data-product-id="{{ $tradeItem->id }}"
        >
            <h5>Assigned climate score:</h5>
            <div v-if="riseData">
                <ul >
                    <li><strong>SLVName:</strong> @{{ riseData.slv_name }}</li>
                    <li><strong>ActivityName:</strong> @{{ riseData.activity_name }}</li>
                    <li><strong>GHGTotalValue:</strong> @{{ riseData.ghg_total_value }}</>
                </ul>
                {{--<button @click.prevent="requestDeleteRiseData">Delete</button>--}}
            </div>
            {{--<div v-else>--}}
                {{--<button @click.prevent="showRiseDataList">Asign rise data</button>--}}
            {{--</div>--}}

            <div v-if="showList">
                <table class="table">
                    <caption>List of rise data</caption>
                    <span><i>Chose row and use double click:</i></span>
                    <thead>
                    <tr>
                        <th scope="col">Cat_1</th>
                        <th scope="col">Cat_2</th>
                        <th scope="col">Cat_3</th>
                        <th scope="col">Slv</th>
                        <th scope="col">Activity</th>
                        <th scope="col">GHG</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><input v-model="search.food_cat_1_name"></td>
                        <td><input v-model="search.food_cat_2_name"></td>
                        <td><input v-model="search.food_cat_3_name"></td>
                        <td><input v-model="search.slv_name"></td>
                        <td><input v-model="search.activity_name"></td>
                    </tr>
                    <tr v-for="item in riseDataList" @dblclick="assignRiseData(item)" style="cursor:pointer">
                        <td>@{{item.food_cat_1_name}}</td>
                        <td>@{{item.food_cat_2_name}}</td>
                        <td>@{{item.food_cat_3_name}}</td>
                        <td>@{{item.slv_name}}</td>
                        <td>@{{item.activity_name}}</td>
                        <td>@{{item.ghg_total_value}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <h5>Assigned attribute score:</h5>
            <div>
                <ul >
                    @foreach($tradeItem->validooItemAttributtes as $attr)
                                <li><strong>{{$attr->attribute_name}} : </strong> {{$attr->value}}</li>
                    @endforeach
                </ul>
            </div>
            <h5>Additional climate score:</h5>
            <div>
                <ul>
                    @foreach($tradeItem->additionalClimateScore() as $key => $value )
                                <li><strong>{{$key}} : </strong> {{$value}}</li>
                    @endforeach
                </ul>
            </div>
            <h5>Products:</h5>
            <div>
                <ul>


                @foreach($tradeItem->products as $product )
                        <h6><strong>Store: </strong>{{$product->providerName()}} </h6>
                        <ul>
                            @foreach($product->toArray() as $key => $value)
                                @if(is_array($value))
                                    @include('admin.validoo.trade_items.product_attr_array', [
                                        'arr' => $value,
                                        'key' => $key
                                    ])
                                @else
                                    <li><strong>{{$key}}: </strong>{{$value}} </li>
                                @endif
                            @endforeach
                            
                        </ul>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/lodash@4.17.10/lodash.min.js"></script>
    <script type="text/javascript" src="{{URL::asset('js/admin/assign_climate.js')}}"></script>
@endsection
