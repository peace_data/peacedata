<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sap="http://www.saphety.com" xmlns:med="http://www.gs1services.net/MediaStore" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
   <soapenv:Header/>
   <soapenv:Body>
      <sap:CreateSubscription>

         <sap:request>
            <med:Password>{{$pass}}</med:Password>
            <med:Username>{{$username}}</med:Username>
            <med:EntityCode>{{$entity_code}}</med:EntityCode>
            <med:Subscription>
               <med:DownloadAlternativeFilename>Standard</med:DownloadAlternativeFilename>
               <med:DownloadFieldOfApplication>SalesAndMarketing</med:DownloadFieldOfApplication>
               <med:DownloadFileFormat>JPG300</med:DownloadFileFormat>
               <med:DownloadIncludeProductInformation>false</med:DownloadIncludeProductInformation>
               <med:DownloadProductInformationFormat>XML</med:DownloadProductInformationFormat>
               <med:NotificationSubscriptionSetup>ImageDeliveryWithNoNotification</med:NotificationSubscriptionSetup>
               <med:SearchAngleIdentifier i:nil="true"/>
               <med:SearchFaceIndicator i:nil="true"/>
               <med:SearchFileNature i:nil="true"/>
               <med:SearchGTIN>{{$gtin}}</med:SearchGTIN>
               <med:SearchTargetMarket>752</med:SearchTargetMarket>
               <med:SubcriptionContentType>OnlyImages</med:SubcriptionContentType>
               <med:SubscriptionId>0</med:SubscriptionId>
               <med:SubscriptionName>{{$gtin.'_images'}}</med:SubscriptionName>
            </med:Subscription>
         </sap:request>
      </sap:CreateSubscription>
   </soapenv:Body>
</soapenv:Envelope>
