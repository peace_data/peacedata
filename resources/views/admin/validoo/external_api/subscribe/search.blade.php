<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sap="http://www.saphety.com" xmlns:med="http://www.gs1services.net/MediaStore">
   <soapenv:Header/>
   <soapenv:Body>
      <sap:SearchSubscriptions>
         <sap:response>
            <med:Password>{{$pass}}</med:Password>
            <med:Username>{{$username}}</med:Username>
            <med:EntityCode>{{$entity_code}}</med:EntityCode>
            <med:NumberOfResults>100</med:NumberOfResults>
            <med:Page>1</med:Page>
         </sap:response>
      </sap:SearchSubscriptions>
   </soapenv:Body>
</soapenv:Envelope>
