<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sap="http://www.saphety.com" xmlns:med="http://www.gs1services.net/MediaStore" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
   <soapenv:Header/>
   <soapenv:Body>
      <sap:CreateSubscription>
         <sap:request>
            <med:Password>{{$pass}}</med:Password>
            <med:Username>{{$username}}</med:Username>
            <med:EntityCode>{{$entity_code}}</med:EntityCode>
            <med:Subscription>
               <med:DownloadAlternativeFilename i:nil="true"/>
               <med:DownloadFieldOfApplication i:nil="true"/>
               <med:DownloadFileFormat i:nil="true"/>
               <med:DownloadIncludeAllTradeItemInformation>true</med:DownloadIncludeAllTradeItemInformation>
               <med:DownloadIncludeProductInformation>false</med:DownloadIncludeProductInformation>
               <med:DownloadProductInformationFormat>XML</med:DownloadProductInformationFormat>
               <med:NotificationSubscriptionSetup>ImageDeliveryWithNoNotification</med:NotificationSubscriptionSetup>
               <med:SearchAngleIdentifier i:nil="true"/>
               <med:SearchBrandName i:nil="true"/>
               <med:SearchFaceIndicator i:nil="true"/>
               <med:SearchFileNature i:nil="true"/>
               <med:SearchGLN i:nil="true"/>
               <med:SearchGPC i:nil="true"/>
               <med:SearchGTIN>{{$gtin}}</med:SearchGTIN>
               <med:SearchSupplierName i:nil="true"/>
               <med:SearchTargetMarket>752</med:SearchTargetMarket>
               <med:SubcriptionContentType>OnlyItemInformation</med:SubcriptionContentType>
               <med:SubscriptionId>0</med:SubscriptionId>
               <med:SubscriptionName>{{$gtin.'_information'}}</med:SubscriptionName>
            </med:Subscription>
         </sap:request>
      </sap:CreateSubscription>
   </soapenv:Body>
</soapenv:Envelope>
