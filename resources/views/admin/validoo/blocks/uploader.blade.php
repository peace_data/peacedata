
{{ Form::open(array('route' => 'admin.validoo.trade_items.upload', 'files' => true)) }}
    <div class='row'>
        <div class='input-group m-t-15'>
            <div class='col-sm-6 text-right' style="padding-top: 8px">
                <h2 class='card-inside-title'>Upload test Xml</h2>
            </div>
            <div class='col-sm-4'>
                {{ Form::file('xml', ['class' => 'btn btn-primary waves-effect']) }}
            </div>
            <div class='col-sm-1'>
                {{  Form::submit('Upload', ['class' => 'btn btn-success upload-btn waves-effect']) }}
            </div>
        </div>
    </div>
{{ Form::close() }}
