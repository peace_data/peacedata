
@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <h2>Measurements</h2>
        </div>
        <div class="card">
            @if ($errors->has('xlsx_file'))
                <span class="text-danger">
                    <strong>{{ $errors->first('xlsx_file') }}</strong>
                </span>
            @endif
            <div class='header text-right'>
              <div class="row">
                <div class='col-sm-10'>
                  {{ Form::open(array('route' => 'admin.references.measurements.upload', 'files' => true)) }}

                    <div class='col-sm-4 text-right' style="padding-top: 8px">
                        <h2 class='card-inside-title'>Upload Measurement data</h2>
                    </div>
                    <div class='col-sm-5'>
                        {{ Form::file('xlsx_file', ['class' => 'btn btn-primary waves-effect']) }}
                    </div>

                    <div class='col-sm-1'>
                        {{  Form::submit('Upload', ['class' => 'btn btn-success  waves-effect']) }}
                    </div>
                  {{ Form::close() }}
                </div>
              </div>
              <div class="search-form">
                @include('admin.blocks.search')
              </div>
            </div>
            <div class="card-body">
            <div class="table-responsive">
                <table class="table table-borderless">
                    <thead>
                    <tr>
                        <th class="sortable-column" data-column-name='id'>#</th>
                        <th class="sortable-column" data-column-name='title'>Title</th>
                        <th class="sortable-column" data-column-name='code'>Code</th>
                        <th class="sortable-column" data-column-name='description'>Description</th>
                        <th>Child measurement </th>
                        <th>Child measurement quantity</th>
                        <!-- <th>Actions</th> -->

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($measurements as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->title }}</td>
                            <td>{{ $item->code }}</td>
                            <td>{{ $item->description }}</td>
                            <td>{{ $item->child_measurement }}</td>
                            <td>{{ $item->child_measurement_quantity }}</td>
                            <!-- <td class="actions with-three-btns">
                                @include('admin.blocks.btns.view', [
                                  'url' => url('/admin/references/package/' . $item->id),
                                  'title' => 'View Measurement reference'
                                ])
                                @include('admin.blocks.btns.edit', [
                                  'url' =>url('/admin/references/package/' . $item->id . '/edit') ,
                                  'title' => 'Edit rise_datum'
                                ])
                                @include('admin.blocks.btns.delete', [
                                  'url' => url('/admin/references/package/' . $item->id),
                                  'title' => 'Delete rise_datum'
                                ])
                            </td> -->
                            
                        </tr>
                        
                    @endforeach
                    </tbody>
                </table>
                <div class="pagination-wrapper"> {!! $measurements->appends(['search' => Request::get('search')])->render() !!} </div>
            </div>
        </div>
    </div>
@endsection
