<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    <label for="title" class="control-label">{{ 'Title' }}</label>
    <input class="form-control" name="title" type="text" id="title" value="{{ $references/package->title or ''}}" >
    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('code') ? 'has-error' : ''}}">
    <label for="code" class="control-label">{{ 'Code' }}</label>
    <input class="form-control" name="code" type="text" id="code" value="{{ $references/package->code or ''}}" >
    {!! $errors->first('code', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    <label for="description" class="control-label">{{ 'Description' }}</label>
    <textarea class="form-control" rows="5" name="description" type="textarea" id="description" >{{ $references/package->description or ''}}</textarea>
    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('isUsedInSweden') ? 'has-error' : ''}}">
    <label for="isUsedInSweden" class="control-label">{{ 'Isusedinsweden' }}</label>
    <div class="radio">
    <label><input name="isUsedInSweden" type="radio" value="1" {{ (isset($references/package) && 1 == $references/package->isUsedInSweden) ? 'checked' : '' }}> Yes</label>
</div>
<div class="radio">
    <label><input name="isUsedInSweden" type="radio" value="0" @if (isset($references/package)) {{ (0 == $references/package->isUsedInSweden) ? 'checked' : '' }} @else {{ 'checked' }} @endif> No</label>
</div>
    {!! $errors->first('isUsedInSweden', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
