@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <h2>List of Trade Items</h2>
        </div>

        <div class="card" >
            <div id="list_trade_item_attribute_level">
                <trade_item_attribute_level>

                </trade_item_attribute_level>
            </div>
             <div class="card-footer pagination-wrapper text-center">
                <div class="row">
                  <div class="col-sm-8">
                    {!! $validooItems->appends(['search' => Request::get('search')])->render() !!}
                  </div>
                  @include('admin.blocks.custom_pagination')
                <div>
            </div>
        </div>

    </div>

    <script src="/js/trade_item_attribute_level.js"></script>
@stop
