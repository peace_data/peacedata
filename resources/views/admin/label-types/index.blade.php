@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <h2>Label Types</h2>
        </div>

        <div class="card">
            <div class="header text-right">
              @include('admin.blocks.btns.add',[
                'url' => url('/admin/label-types/create'),
                'title' => 'Add New Label Types'
              ])
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th><th>Title</th><th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                          @foreach($labeltypes as $item)
                              <tr>
                                  <td>{{ $loop->iteration or $item->id }}</td>
                                  <td>{{ $item->title }}</td>
                                  <td class="actions">
                                      @include('admin.blocks.btns.edit', [
                                        'url' => url('/admin/label-types/' . $item->id . '/edit'),
                                        'title' => 'Edit Label'
                                      ])
                                      @include('admin.blocks.btns.delete', [
                                        'url' => url('/admin/label-types' . '/' . $item->id),
                                        'title' => 'Delete Label'
                                      ])

                                  </td>
                              </tr>
                          @endforeach
                        </tbody>
                    </table>
                    <div class="pagination-wrapper">
                      {!! $labeltypes->appends(['search' => Request::get('search')])->render() !!}
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
