

@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <h2>Edit LabelType #{{ $labeltype->id }}</h2>
        </div>
        <div class="card">
            <div class="header text-right">
                @include('admin.blocks.btns.back', [
                  'url' => url('/admin/label-types')
                ])
                
            </div>
            <div class="card-body">
                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

                {!! Form::model($labeltype, [
                    'method' => 'PATCH',
                    'url' => ['/admin/label-types', $labeltype->id],
                    'class' => 'form-horizontal',
                    'files' => true
                ]) !!}
                    @include ('admin.label-types.form', ['formMode' => 'edit', 'submitButtonText' => 'update'])

                {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
