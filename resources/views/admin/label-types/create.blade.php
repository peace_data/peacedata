@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <h2>Create New Label Type</h2>
        </div>
        <div class="card">
            <div class="header text-right">
              @include('admin.blocks.btns.back', [
                'url' => url('/admin/labels')
              ])
            </div>
            <div class="card-body">


                {!! Form::open(['url' => '/admin/label-types', 'class' => 'form-horizontal', 'files' => true]) !!}

                @include ('admin.label-types.form', ['formMode' => 'create'])

                {!! Form::close() !!}

            </div>
            <div class="card-footer">
                <br/>
            </div>
        </div>
    </div>
@endsection
