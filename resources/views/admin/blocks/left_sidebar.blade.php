<aside id="leftsidebar" class="sidebar">
    <!-- User Info -->
    <div class="user-info">
        <div class="toggle-sidebar">
          <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_left</i>
        </div>
        <div class="image">
            <img src="/images/user.png" width="48" height="48" alt="User" />
        </div>
        <div class="info-container">

            <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}</div>
            <div class="email">{{ Auth::user()->email }}</div>
            <div class="btn-group user-helper-dropdown">
                <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                <ul class="dropdown-menu pull-right">
                    <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                    <li role="separator" class="divider"></li>
                    <li role="separator" class="divider"></li>
                    <li><a  href="{{ route('logout') }}"
                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        <i class="material-icons">input</i></a></li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </ul>
            </div>
        </div>
    </div>
    <!-- #User Info -->
    <!-- Menu -->
    <div class="menu">
        <ul class="list">
            <li class="header">MAIN NAVIGATION</li>
            <!-- <li class="active">
                <a href="{{ url('/admin') }}">
                    <i class="material-icons">home</i>
                    <span>Home</span>
                </a>
            </li> -->
            <li>
                <a href="javascript:void(0);" class="menu-toggle waves-effect waves-block">
                            <i class="material-icons">perm_media</i>
                            <span>Validoo Items</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{ url('/admin/validoo/trade_items') }}" class=" waves-effect waves-block">List Items</a>
                            </li>
                            <li>
                                <a href="{{ url('/admin/category_levels') }}" class=" waves-effect waves-block">Category level</a>
                            </li>
                            <li>
                                <a href="{{ url('admin/validoo_item/attributte_levels') }}" class=" waves-effect waves-block">Attribute level</a>
                            </li>
                             <li>
                                <a href="{{ url('admin/trade_item/attributte_levels') }}" class=" waves-effect waves-block">Attribute labels</a>
                            </li>
                        </ul>
            </li>
    <!--         <li>
                <a href="{{ url('/admin/validoo/trade_items') }}">
                    <i class="material-icons">layers</i>
                    <span>Validoo Items</span>
                </a>
            </li> -->
            <li>
                <a href="{{ url('/admin/providers') }}">
                    <i class="material-icons">layers</i>
                    <span>Stores</span>
                </a>
            </li>
            <li>
                <a href="{{ url('/admin/products') }}">
                    <i class="material-icons">layers</i>
                    <span>Products</span>
                </a>
            </li>


            <!-- <li>
                <a href="javascript:void(0);">
                    <i class="material-icons col-light-blue">donut_large</i>
                    <span>Information</span>
                </a>
            </li> -->
            <li>
                <a href="javascript:void(0);" class="menu-toggle waves-effect waves-block">
                    <i class="material-icons">label</i>
                    <span>Labels</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="{{ url('/admin/labels') }}" class=" waves-effect waves-block">Labels list</a>
                    </li>
                    <li>
                        <a href="{{ url('/admin/label-types') }}" class=" waves-effect waves-block">Labels Types</a>
                    </li>
                </ul>
            </li>
            
            <li>
                <a href="javascript:void(0);" class="menu-toggle waves-effect waves-block">
                    <i class="material-icons">perm_media</i>
                    <span>References</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="{{ url('/admin/additional-climate-scores') }}">
                            <span>Additional climate scores</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/admin/rise_datum') }}">
                            <span>Rise Data</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/admin/references/package') }}">
                            <span>Package</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/admin/references/measurements') }}">
                            <span>Measurement</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
    <!-- #Menu -->
    <!-- Footer -->
    {{--<div class="legal">--}}
        {{--<div class="copyright">--}}
            {{--&copy; 2016 - 2017 <a href="javascript:void(0);">AdminBSB - Material Design</a>.--}}
        {{--</div>--}}
        {{--<div class="version">--}}
            {{--<b>Version: </b> 0.0.1--}}
        {{--</div>--}}
    {{--</div>--}}
    <!-- #Footer -->
</aside>
