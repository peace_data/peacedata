<div class="row">
  <div class="col-sm-8 col-sm-offset-2">
    {!! Form::open(['method' => 'GET', 'url' => Request::url(), 'class' => 'text-center', 'role' => 'search'])  !!}
      <div class="input-group">
          <div class="form-line">
              <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
          </div>
          <span class="input-group-addon">
            <button class="btn btn-default" type="submit">
              <i class="material-icons">search</i>
            </button>
          </span>
      </div>
    {!! Form::close() !!}
  </div>

</div>
