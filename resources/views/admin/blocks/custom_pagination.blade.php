<div class="col-md-2 col-sm-4">
  <div class="input-group pagination-page-input">
      <div class="form-line">
          <input type="text" class="form-control page-value" placeholder="Set page" data-rule="quantity">
      </div>
      <span class="input-group-addon">
          <i class="material-icons change-page">send</i>
      </span>
  </div>
</div>
