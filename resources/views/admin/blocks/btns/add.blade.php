<a href="{{ $url }}" class="btn btn-success btn-sm" title="{{ $title }}">
    <i class="material-icons">add</i>
</a>
