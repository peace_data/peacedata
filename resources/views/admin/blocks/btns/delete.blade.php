<form method="POST" action="{{ $url }}" accept-charset="UTF-8" style="display:inline">
    {{ method_field('DELETE') }}
    {{ csrf_field() }}
    <button type="submit" class="btn btn-danger btn-sm" title="{{$title}}" onclick="return confirm('Confirm delete?')">
      <i class="material-icons">delete</i>
    </button>
</form>
