<a href="{{ $url }}" title="{{ $title }}">
  <button class="btn btn-primary btn-sm">
    <i class="material-icons">edit</i>
  </button>
</a>
