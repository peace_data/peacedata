<a href="{{ $url }}" title="{{$title}}">
  <button class="btn btn-info btn-sm">
    <i class="material-icons">pageview</i>
  </button>
</a>
