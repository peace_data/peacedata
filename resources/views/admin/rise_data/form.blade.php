<div class="form-group {{ $errors->has('food_cat_1_id') ? 'has-error' : ''}}">
    {!! Form::label('food_cat_1_id', 'Food Cat 1 Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="form-line">
        {!! Form::number('food_cat_1_id', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('food_cat_1_id', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
</div>
<div class="form-group {{ $errors->has('food_cat_1_name') ? 'has-error' : ''}}">
    {!! Form::label('food_cat_1_name', 'Food Cat 1 Name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="form-line">
        {!! Form::text('food_cat_1_name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('food_cat_1_name', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
</div><div class="form-group {{ $errors->has('food_cat_2_id') ? 'has-error' : ''}}">
    {!! Form::label('food_cat_2_id', 'Food Cat 2 Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="form-line">
        {!! Form::number('food_cat_2_id', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('food_cat_2_id', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
</div><div class="form-group {{ $errors->has('food_cat_2_name') ? 'has-error' : ''}}">
    {!! Form::label('food_cat_2_name', 'Food Cat 2 Name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="form-line">
        {!! Form::text('food_cat_2_name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('food_cat_2_name', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
</div><div class="form-group {{ $errors->has('food_cat_3_id') ? 'has-error' : ''}}">
    {!! Form::label('food_cat_3_id', 'Food Cat 3 Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="form-line">
        {!! Form::number('food_cat_3_id', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('food_cat_3_id', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
</div><div class="form-group {{ $errors->has('food_cat_3_name') ? 'has-error' : ''}}">
    {!! Form::label('food_cat_3_name', 'Food Cat 3 Name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="form-line">
        {!! Form::text('food_cat_3_name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('food_cat_3_name', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
</div><div class="form-group {{ $errors->has('slv_id') ? 'has-error' : ''}}">
    {!! Form::label('slv_id', 'Slv Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="form-line">
        {!! Form::number('slv_id', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('slv_id', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
</div><div class="form-group {{ $errors->has('slv_name') ? 'has-error' : ''}}">
    {!! Form::label('slv_name', 'Slv Name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="form-line">
        {!! Form::text('slv_name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('slv_name', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
</div><div class="form-group {{ $errors->has('activity_id') ? 'has-error' : ''}}">
    {!! Form::label('activity_id', 'Activity Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="form-line">
        {!! Form::number('activity_id', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('activity_id', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
</div><div class="form-group {{ $errors->has('activity_name') ? 'has-error' : ''}}">
    {!! Form::label('activity_name', 'Activity Name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="form-line">
        {!! Form::text('activity_name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('activity_name', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
</div>
<div class="form-group {{ $errors->has('ghg_total_value') ? 'has-error' : ''}}">
    {!! Form::label('ghg_total_value', 'GHGTotalValue', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="form-line">
        {!! Form::text('ghg_total_value', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('ghg_total_value', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
</div>

<div class="form-group {{ $errors->has('region_id') ? 'has-error' : ''}}">
    {!! Form::label('region_id', 'Region Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="form-line">
        {!! Form::number('region_id', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('region_id', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
</div><div class="form-group {{ $errors->has('region_name') ? 'has-error' : ''}}">
    {!! Form::label('region_name', 'Region Name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="form-line">
        {!! Form::text('region_name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('region_name', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
</div><div class="form-group {{ $errors->has('region_source') ? 'has-error' : ''}}">
    {!! Form::label('region_source', 'Region Source', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="form-line">
        {!! Form::text('region_source', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('region_source', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
</div><div class="form-group {{ $errors->has('production_type') ? 'has-error' : ''}}">
    {!! Form::label('production_type', 'Production Type', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="form-line">
        {!! Form::text('production_type', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('production_type', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
</div><div class="form-group {{ $errors->has('production_type_source') ? 'has-error' : ''}}">
    {!! Form::label('production_type_source', 'Production Type Source', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="form-line">
        {!! Form::text('production_type_source', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('production_type_source', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
