@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <h2>Rise Data {{ $rise_datum->id }}</h2>
        </div>
        <div class="card">
            <div class="header text-right">
              @include('admin.blocks.btns.back', [
                'url' => url('/admin/rise_datum/'),
              ])
              @include('admin.blocks.btns.edit', [
                'url' => url('/admin/rise_datum/' . $rise_datum->id . '/edit'),
                'title' => 'Edit'
              ])
              @include('admin.blocks.btns.delete', [
                'url' => url('/admin/rise_datum' . '/' . $rise_datum->id),
                'title' => 'Delete'
              ])
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                                <th>ID</th><td>{{ $rise_datum->id }}</td>
                            </tr>
                            <tr>
                                <th>FoodCat1ID</th><td>{{ $rise_datum->food_cat_1_id }}</td>
                            </tr>
                            <tr>
                                <th>FoodCat1Name</th><td>{{ $rise_datum->food_cat_1_name }}</td>
                            </tr>
                            <tr>
                                <th>FoodCat2ID</th><td>{{ $rise_datum->food_cat_2_id }}</td>
                            </tr>
                            <tr>
                                <th>FoodCat2Name</th><td>{{ $rise_datum->food_cat_2_name }}</td>
                            </tr>
                            <tr>
                                <th>FoodCat3ID</th><td>{{ $rise_datum->food_cat_3_id }}</td>
                            </tr>
                            <tr>
                                <th>FoodCat3Name</th><td>{{ $rise_datum->food_cat_3_name }}</td>
                            </tr>
                            <tr>
                                <th>SLVID</th><td>{{ $rise_datum->slv_id }}</td>
                            </tr>
                            <tr>
                                <th>SLVIDName</th><td>{{ $rise_datum->slv_name }}</td>
                            </tr>
                            <tr>
                                <th>ActivityID</th><td>{{ $rise_datum->activity_id }}</td>
                            </tr>
                            <tr>
                                <th>ActivityName</th><td>{{ $rise_datum->activity_name }}</td>
                            </tr>
                            <tr>
                                <th>GHGTotalValue</th><td>{{ $rise_datum->ghg_total_value }}</td>
                            </tr>
                            <tr>
                                <th>RegionID</th><td>{{ $rise_datum->region_id }}</td>
                            </tr>
                            <tr>
                                <th>RegionName</th><td>{{ $rise_datum->region_name }}</td>
                            </tr>
                            <tr>
                                <th>RegionSource</th><td>{{ $rise_datum->region_source }}</td>
                            </tr>
                            <tr>
                                <th>ProductionType</th><td>{{ $rise_datum->production_type }}</td>
                            </tr>
                            <tr>
                                <th>ProductionTypeSource</th><td>{{ $rise_datum->production_type_source }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
