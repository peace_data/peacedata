<style>
    img {
        height: auto;
        max-width: 2.5rem;
        margin-right: 1rem;
    }

    .d-center {
        display: flex;
        align-items: center;
    }

    .selected img {
        width: auto;
        max-height: 23px;
        margin-right: 0.5rem;
    }

    .v-select .dropdown li {
        border-bottom: 1px solid rgba(112, 128, 144, 0.1);
    }

    .v-select .dropdown li:last-child {
        border-bottom: none;
    }

    .v-select .dropdown li a {
        padding: 10px 20px;
        width: 100%;
        font-size: 1.25em;
        color: #3c3c3c;
    }

    .v-select .dropdown-menu .active > a {
        color: #fff;
    }

    .rise-data{
        margin: 10px;
    }


</style>


<div id="rise_data" class="col-md-offset-4 col-md-8 rise-data" >
    <button v-if="!is_add_rise_data" @click.prevent="showAddRiseData">Add Rise Data</button>
    <button v-else="is_add_rise_data" @click.prevent="showAddRiseData">Hide Rise Data</button>

    <div v-if="is_add_rise_data">
        <button type="button"
                v-for="attribute in model_attributes"
                @click="selectedFilter(attribute)"
                :disabled="attribute.state == 'disabled'"
                :class="[attribute.state=='active' ? 'btn btn-primary' : 'btn btn-info']"
                >
            @{{ attribute.title }}
        </button>

        <v-select
                  label="id"
                  :filterable="false"
                  :options="options"
                  @search="onSearch"
                  v-model="current_selected"
        >
            {{--<template slot="no-options">--}}
                {{--type to search GitHub repositories..--}}
            {{--</template>--}}
            <template slot="option" slot-scope="option">
                <div class="d-center">
                    @{{ option[Object.keys(option)[0]] }}
                </div>
            </template>
            <template slot="selected-option" scope="option">
                <div class="selected d-center">
                    @{{ option[Object.keys(option)[0]] }}
                </div>
            </template>
        </v-select>

        <p>Current Filter:</p>
        <ul>
            <li v-for="selected in selectedAttributes">@{{ selected.title }} : @{{ selected.result }}</li>
        </ul>
    </div>

</div>

<script src="https://cdn.jsdelivr.net/npm/lodash@4.17.10/lodash.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue@2.5.17/dist/vue.js"></script>
<script src="https://unpkg.com/vue-select@latest"></script>

<script>

    document.addEventListener("DOMContentLoaded", function(event) {
        Vue.component('v-select', VueSelect.VueSelect);

        var demo = new Vue({
            el: '#rise_data',
            data: {
                is_add_rise_data: false,
                current_selected: '',
                model_attributes: [],
                options: []
            },
            mounted: function(){
                this.init_bottoms();
            },
            watch: {
                current_selected: function(){
                    if(this.current_selected == '' || this.current_selected == null) {
                        return false;
                    }

                   this.curentFilteredAttribute[0].result = this.current_selected[Object.keys(this.current_selected)[0]]
                   this.curentFilteredAttribute[0].state = 'disabled'
                   this.current_selected = ''
                   this.options = []
                   return false;
                }
            },
            computed: {

                selectedAttributes: function() {
                    return this.model_attributes.filter(function (item){
                       return item.result != null
                    })
                },
                curentFilteredAttribute: function(){
                    return this.model_attributes.filter(function (item){
                        return item.state == 'active'
                    })
                },
            },
            methods: {
               // initialize objext
                init_bottoms: function(){
                    this.model_attributes = [
                        {
                            title: 'Category 1 Name',
                            value: 'food_cat_1_name',
                            state: 'active',
                            result: null
                        },
                        {
                            title: 'Category 2 Name',
                            value: 'food_cat_2_name',
                            state: 'passive',
                            result: null
                        },
                        {
                            title: 'Category 3 Name',
                            value: 'food_cat_3_name',
                            state: 'passive',
                            result: null
                        },
                        {
                            title: 'Region Name',
                            value: 'region_name',
                            state: 'passive',
                            result: null
                        },

                    ]
                },

                // path of selected logic
                getTypeSearch: function(){
                    return this.curentFilteredAttribute[0].value
                },
                selectedFilter: function(attribute){
                    this.model_attributes.forEach(function(item, index, arr){
                        if(item.value == attribute.value){
                            arr[index].state = 'active'
                        }else{
                            if(arr[index].state != 'disabled') {
                                arr[index].state = 'passive'
                            }
                        }
                    })
                },
                showAddRiseData: function(e){
                    e.preventDefault();
                    this.is_add_rise_data = !this.is_add_rise_data;
                },

                // path of back
                onSearch(search, loading) {
                    loading(true);
                    var type = this.getTypeSearch();
                    this.search(loading, search, type, this);
                },

                search: _.debounce((loading, search, type, vm) => {
                    fetch(
                    `http://localhost:8080/admin/selected?type=${escape(type)}&search=${escape(search)}`
                ).then(res => {
                    res.json().then(json => (vm.options = json));
                     loading(false);
                });
                }, 350)
            }
        })
    })
</script>
