@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <h2>Rise_data</h2>
        </div>
        <div class="card">
            @if ($errors->has('csv_file'))
                <span class="text-danger">
                    <strong>{{ $errors->first('csv_file') }}</strong>
                                    </span>
            @endif
            <div class='header text-right'>
              <div class="row">
                <div class='col-sm-10'>
                  {{ Form::open(array('route' => 'admin.rise_datum.upload', 'files' => true)) }}

                    <div class='col-sm-4 text-right' style="padding-top: 8px">
                        <h2 class='card-inside-title'>Upload Rise climate data</h2>
                    </div>
                    <div class='col-sm-5'>
                        {{ Form::file('csv_file', ['class' => 'btn btn-primary waves-effect']) }}
                    </div>

                    <div class='col-sm-1'>
                        {{  Form::submit('Upload', ['class' => 'btn btn-success  waves-effect']) }}
                    </div>
                  {{ Form::close() }}
                </div>
                <div class="col-sm-2">
                  @include('admin.blocks.btns.add', [
                    'url' => url('/admin/rise_datum/create'),
                    'title' => 'Add New Rasi Data'
                  ])
                </div>
              </div>
              <div class="search-form">
                @include('admin.blocks.search')
              </div>
            </div>
            <div class="card-body">
            <div class="table-responsive">
                <table class="table table-borderless">
                    <thead>
                    <tr>
                        <th class="sortable-column" data-column-name='id'>#</th>
                        <th style="width:14%" class="sortable-column" data-column-name='food_cat_1_id'>Cat 1 Name</th>
                        <th style="width:16%" class="sortable-column" data-column-name='food_cat_2_id'>Cat 2 Name</th>
                        <th style="width:16%" class="sortable-column" data-column-name='food_cat_3_id'>Cat 3 Name</th>
                        <th style="width:16%" class="sortable-column" data-column-name='ghg_total_value'>GHGTotalValue</th>
                        <th style="width:16%" class="sortable-column" data-column-name='region_name'>RegionName</th>
                        <th style="width:16%">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($rise_data as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td style="width:14%">{{ $item->food_cat_1_name }}</td>
                            <td style="width:16%">{{ $item->food_cat_2_name }}</td>
                            <td style="width:16%">{{ $item->food_cat_3_name }}</td>

                            <td style="width:16%">{{ $item->ghg_total_value }}</td>
                            <td style="width:16%">{{ $item->region_name }}</td>
                            <td class="actions with-three-btns">
                                @include('admin.blocks.btns.view', [
                                  'url' => url('/admin/rise_datum/' . $item->id),
                                  'title' => 'View rise_datum'
                                ])
                                @include('admin.blocks.btns.edit', [
                                  'url' => url('/admin/rise_datum/' . $item->id . '/edit'),
                                  'title' => 'Edit rise_datum'
                                ])
                                @include('admin.blocks.btns.delete', [
                                  'url' => url('/admin/rise_datum' . '/' . $item->id),
                                  'title' => 'Delete rise_datum'
                                ])
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="pagination-wrapper"> {!! $rise_data->appends(['search' => Request::get('search')])->render() !!} </div>
            </div>
        </div>
    </div>
@endsection
