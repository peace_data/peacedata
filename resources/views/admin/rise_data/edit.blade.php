@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <h2>Edit Rise Data #{{ $rise_datum->id }}</h2>
        </div>

        <div class="card">
            <div class="header text-right">
              @include('admin.blocks.btns.back', [
                'url' => url('/admin/rise_datum')
              ])
            </div>
            <div class="card-body">
                {{--@if ($errors->any())--}}
                    {{--<ul class="alert alert-danger">--}}
                        {{--@foreach ($errors->all() as $error)--}}
                            {{--<li>{{ $error }}</li>--}}
                        {{--@endforeach--}}
                    {{--</ul>--}}
                {{--@endif--}}

                {!! Form::model($rise_datum, [
                    'method' => 'PATCH',
                    'url' => ['/admin/rise_datum', $rise_datum->id],
                    'class' => 'form-horizontal',
                    'files' => true
                ]) !!}

                @include ('admin.rise_data.form', ['submitButtonText' => 'Update'])

                {!! Form::close() !!}

            </div>
            <div class="card-footer"><br/></div>
        </div>
    </div>
@endsection
