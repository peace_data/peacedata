@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <h2>Create New Product</h2>
        </div>
        <div class="card">
            <div class="header text-right">
              @include('admin.blocks.btns.back', [
                'url' => url('/admin/products')
              ])            </div>
            <div class="card-body">
                {{--@if ($errors->any())--}}
                    {{--<ul class="alert alert-danger">--}}
                        {{--@foreach ($errors->all() as $error)--}}
                            {{--<li>{{ $error }}</li>--}}
                        {{--@endforeach--}}
                    {{--</ul>--}}
                {{--@endif--}}

                {!! Form::open(['url' => '/admin/products', 'class' => 'form-horizontal', 'files' => true]) !!}

                @include ('admin.products.form')

                {!! Form::close() !!}

            </div>
            <div class="card-footer">
                <br/>
            </div>
        </div>
    </div>
@endsection
