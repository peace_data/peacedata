<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    {!! Form::label('title', 'Title', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="form-line">
        {!! Form::text('title', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
</div>
<div class="form-group {{ $errors->has('weight') ? 'has-error' : ''}}">
    {!! Form::label('weight', 'Weight', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="form-line">

        {!! Form::text('weight', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('weight', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
</div><div class="form-group {{ $errors->has('price') ? 'has-error' : ''}}">
    {!! Form::label('price', 'Price', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="form-line">
        {!! Form::number('price', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
</div><div class="form-group {{ $errors->has('price_unit') ? 'has-error' : ''}}">
    {!! Form::label('price_unit', 'Price Unit', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="form-line">

        {!! Form::text('price_unit', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('price_unit', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
</div><div class="form-group {{ $errors->has('currency') ? 'has-error' : ''}}">
    {!! Form::label('currency', 'Currency', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="form-line">

        {!! Form::text('currency', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('currency', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
</div><div class="form-group {{ $errors->has('compare_price') ? 'has-error' : ''}}">
    {!! Form::label('compare_price', 'Compare Price', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="form-line">

        {!! Form::number('compare_price', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('compare_price', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
</div><div class="form-group {{ $errors->has('compare_unit') ? 'has-error' : ''}}">
    {!! Form::label('compare_unit', 'Compare Unit', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="form-line">

        {!! Form::text('compare_unit', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('compare_unit', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
</div><div class="form-group {{ $errors->has('brand') ? 'has-error' : ''}}">
    {!! Form::label('brand', 'Brand', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="form-line">

        {!! Form::text('brand', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('brand', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
</div><div class="form-group {{ $errors->has('informations') ? 'has-error' : ''}}">
    {!! Form::label('informations', 'Informations', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="form-line">

        {!! Form::textarea('informations', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'json here'] : ['class' => 'form-control', 'placeholder'=>'json  here']) !!}
        {!! $errors->first('informations', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
</div><div class="form-group {{ $errors->has('category') ? 'has-error' : ''}}">
    {!! Form::label('category', 'Category', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="form-line">

        {!! Form::text('category', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('category', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
</div><div class="form-group {{ $errors->has('isEko') ? 'has-error' : ''}}">
    {!! Form::label('isEko', 'Iseko', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <div class="checkbox">
    <label>{!! Form::radio('isEko', '1') !!} Yes</label>
</div>
<div class="checkbox">
    <label>{!! Form::radio('isEko', '0', true) !!} No</label>
</div>
        {!! $errors->first('isEko', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('external_key') ? 'has-error' : ''}}">
    {!! Form::label('external_key', 'Externalkey', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="form-line">

        {!! Form::text('external_key', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('external_key', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
</div>
<div class="form-group {{ $errors->has('provider_id') ? 'has-error' : ''}}">
    {!! Form::label('provider_id', 'Provider Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      <div class="form-line">

        {!! Form::number('provider_id', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('provider_id', '<p class="help-block">:message</p>') !!}
      </div>
    </div>
</div>





{{--<div class="form-group">--}}
    {{--@include ('admin.rise_data.select_partial')--}}
{{--</div>--}}




<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
