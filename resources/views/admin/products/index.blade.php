@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <h2>Products</h2>
        </div>

        <div class="card">
            <div class="header row text-right">
                <div class="col-sm-11">
                  @include('admin.blocks.search')
                </div>
                <div class="col-sm-1">
                  @include('admin.blocks.btns.add', [
                    'url' => url('/admin/products/create'),
                    'title' => 'Add New Product'
                  ])
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-borderless">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th class="sortable-column" data-column-name='external_key'>External_key</th>
                                <th class="sortable-column" data-column-name='category'>Category</th>
                                <th class="sortable-column" data-column-name='brand'>Brand Name</th>
                                <th class="sortable-column" data-column-name='title'>Product Name</th>
                                <th class="sortable-column" data-column-name='price'>Price</th>
                                <th class="sortable-column" data-column-name='weight'>Weight</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->external_key }}</td>
                                <td>{{ $item->category }}</td>
                                <td>{{ $item->brand }}</td>
                                <td>{{ $item->title }}</td>
                                <td>{{ format_money($item->price, $item->currency)}}</td>
                                <td>{{ $item->weight. ' ' . $item->price_unit }}</td>
                                <td class="actions with-three-btns">
                                    @include('admin.blocks.btns.view', [
                                      'url' => url('/admin/products/' . $item->id),
                                      'title' => 'View Product'
                                    ])
                                    @include('admin.blocks.btns.edit', [
                                      'url' => url('/admin/products/' . $item->id . '/edit'),
                                      'title' => 'Edit Product'
                                    ])
                                    @include('admin.blocks.btns.delete', [
                                      'url' => url('/admin/products' . '/' . $item->id),
                                      'title' => 'Delete Product'
                                    ])

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer pagination-wrapper text-center">
                <div class="row">
                  <div class="col-md-8">
                    {!! $products->appends(['search' => Request::get('search')])->render() !!}
                  </div>
                  @include('admin.blocks.custom_pagination')
                <div>
            </div>
        </div>
    </div>
@endsection
