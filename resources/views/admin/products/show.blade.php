@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <h2>Product {{ dd($product) }}</h2>
        </div>
        <div class="card">
            <div class="header text-right">
                @include('admin.blocks.btns.back', [
                  'url' => url('/admin/products/'),
                ])
                @include('admin.blocks.btns.edit', [
                  'url' => url('/admin/products/' . $product->id . '/edit'),
                  'title' => 'Edit'
                ])
                @include('admin.blocks.btns.delete', [
                  'url' => url('/admin/products' . '/' . $product->id),
                  'title' => 'Delete'
                ])
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-borderless">
                        <tbody>
                            <tr>
                                <th>ID</th>
                                <td>{{ $product->id }}</td>
                            </tr>
                            <tr>
                                <th>External Key</th>
                                <td> {{ $product->external_key }}</td>
                            </tr>
                            <tr>
                                <th> Title </th>
                                <td> {{ $product->title }} </td>
                            </tr>
                            <tr>
                                <th> Weight </th>
                                <td> {{ $product->weight }} </td>
                            </tr>
                            <tr>
                                <th> Price </th>
                                <td> {{ $product->price }} </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div style="margin-left: 10px">
                    <h5>Related trade item from validoo:</h5>
                    {{--@include('admin.validoo.trade_items.tree_trade_item')--}}
                </div>
            </div>
        </div>
    </div>
@endsection
