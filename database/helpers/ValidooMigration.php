<?php
// namespace Database\Helpers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ValidooMigration extends Migration{
  protected $tableName;
  protected $foreignKey;
  protected $foreignTable;

  protected function setTableName($name){
    $this->tableName = env('DB_VALIDOO_TABLE_PREFIX').$name;
  }
  protected function setForeignKey($key){
    $this->foreignKey = $key;
  }
  protected function setForeignTable($name){
    $this->foreignTable = env('DB_VALIDOO_TABLE_PREFIX').$name;
  }

  public function down()
  {
    Schema::table($this->tableName, function($table)
    {
       $table->dropForeign([$this->foreignKey]);
    });

    Schema::dropIfExists($this->tableName);
  }
}
