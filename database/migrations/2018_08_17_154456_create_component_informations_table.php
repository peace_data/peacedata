<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComponentInformationsTable extends ValidooMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function __construct(){
      $this->setTableName('component_informations');
      $this->setForeignKey('trade_item_component_id');
      $this->setForeignTable('trade_item_components');
    }

    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('component_number');
            $table->string('component_identification', 80);
            $table->unsignedInteger('component_quantity');
            $table->unsignedInteger('component_multiple_packed_quantity');
            $table->string('context_identification', 80);
            $table->string('gpc_category_code', 8);
            $table->jsonb('component_description');

            $table->integer($this->foreignKey)->unsigned();
            $table->foreign($this->foreignKey)
              ->references('id')->on($this->foreignTable)
              ->onDelete('cascade');
            $table->timestamps();
        });
    }

}
