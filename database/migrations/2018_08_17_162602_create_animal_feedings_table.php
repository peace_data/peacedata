<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnimalFeedingsTable extends ValidooMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function __construct(){
       $this->setTableName('animal_feedings');
       $this->setForeignKey('animal_feeding_information_id');
       $this->setForeignTable('animal_feeding_informations');
     }
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->increments('id');
            $table->string('feed_life_stage');
            $table->jsonb('maximum_weight_of_animal_fed');
            $table->jsonb('minimum_weight_of_animal_fed');
            $table->timestamps();

            $table->integer($this->foreignKey)->unsigned();
            $table->foreign($this->foreignKey)
              ->references('id')->on($this->foreignTable)
              ->onDelete('cascade');
        });
    }


}
