<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeMarkingsPackagingMarkedLabelAccreditationCodeColumnType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('validoo_markings', function (Blueprint $table) {
            $table->dropColumn('packaging_marked_label_accreditation_code');
        });
         Schema::table('validoo_markings', function (Blueprint $table) {
            $table->jsonb('packaging_marked_label_accreditation_code')->default('{}');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('validoo_markings', function (Blueprint $table) {
            $table->dropColumn('packaging_marked_label_accreditation_code');
        });
        Schema::table('validoo_markings', function (Blueprint $table) {
            // $table->string('packaging_marked_label_accreditation_code', 80);
        });
    }
}
