<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateValidooItemAttributtesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('validoo_item_attributtes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('validoo_item_id');
            $table->decimal('value')->nullable();
            $table->string('attribute_name');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('validoo_item_attributtes');
    }
}
