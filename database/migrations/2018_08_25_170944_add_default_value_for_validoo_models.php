<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultValueForValidooModels extends Migration
{
    const STANDART_COLUMNS = ['updated_at', 'created_at', 'id'];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      $models = get_list_models(app_path().'/Models/Validoo/');
      foreach ($models as $modelName) {
        $modelPath = 'App\Models\Validoo\\'.$modelName;
        $model = new $modelPath;
        Schema::table($model->getTable(), function (Blueprint $table) use($model) {
          $columns = Schema::getColumnListing($model->getTable());
          foreach ($columns as $column) {
            if( in_array($column, self::STANDART_COLUMNS) || $this->columnIsReferenceKey($column)){
              continue;
            }

            $type = Schema::getColumnType($model->getTable(), $column);
            if($type == 'datetime'){
                $table->$type($column)->nullable()->change();
            }else{
              $table->$type($column)->default($this->setDefaultValue($type))->change();
            }
            var_dump($this->setDefaultValue($type));

          }
        });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

    private function setDefaultValue($type){
      switch ($type) {
        case 'json':
          return '{}';
          break;
        case 'string':
          return '';
          break;
        case 'integer':
          return 0;
          break;
        case 'decimal':
          return 0.0;
          break;
        case 'boolean':
          return 0.0;
          break;

      }
    }
    private function columnIsReferenceKey($column){
      return strpos($column, '_id');
    }
}
