<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBatteriesTable extends ValidooMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function __construct(){
       $this->setTableName('batteries');
       $this->setForeignKey('trade_item_id');
       $this->setForeignTable('trade_items');
     }
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('are_batteries_included');
            $table->boolean('are_batteries_required');
            $table->integer($this->foreignKey)->unsigned();
            $table->foreign($this->foreignKey)
              ->references('id')->on($this->foreignTable)
              ->onDelete('cascade');
            $table->timestamps();
        });
    }

}
