<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetNullableForNutritient extends ValidooMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('validoo_nutritional_informations', function($table)
        {
            $table->json('nutritional_claim', 50)->default('{}')->change();
        });
        Schema::table('validoo_nutritial_headers', function($table)
        {
            $table->string('preparation_state_code', 80)->nullable()->change();
            $table->string('nutrient_basis_quantity_type_code', 80)->nullable()->change();
            $table->json('daily_value_intake_reference')->default('{}')->change();
            $table->json('nutrient_basis_quantity')->default('{}')->change();
            $table->json('serving_size')->default('{}')->change();
            $table->json('serving_size_description')->default('{}')->change();
        });

        Schema::table('validoo_nutrients', function($table)
        {
            $table->unsignedDecimal('daily_value_intake_percent')->nullable()->change();
            $table->string('nutrient_type_code', 80)->nullable()->change();
            $table->string('measurement_precision_code', 80)->nullable()->change();
            $table->json('quantity_contained')->default('{}')->change();
        });
        
        

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('validoo_nutritional_informations', function($table)
        {
            $table->json('nutritional_claim', 50)->nullable(false)->change();
        });
        Schema::table('validoo_nutritial_headers', function($table)
        {
            $table->string('preparation_state_code', 80)->nullable(false)->change();
            $table->string('nutrient_basis_quantity_type_code', 80)->nullable(false)->change();
            $table->json('daily_value_intake_reference')->nullable(false)->change();
            $table->json('nutrient_basis_quantity')->nullable(false)->change();
            $table->json('serving_size')->nullable(false)->change();
            $table->json('serving_size_description')->nullable(false)->change();
        });

        Schema::table('validoo_nutrients', function($table)
        {
            $table->unsignedDecimal('daily_value_intake_percent')->nullable(false)->change();
            $table->string('nutrient_type_code', 80)->nullable(false)->change();
            $table->string('measurement_precision_code', 80)->nullable(false)->change();
            $table->json('quantity_contained')->nullable(false)->change();
        });
    }
}
