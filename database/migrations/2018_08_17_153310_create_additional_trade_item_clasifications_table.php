<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdditionalTradeItemClasificationsTable extends ValidooMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function __construct(){
     $this->setTableName('additional_trade_item_clasifications');
     $this->setForeignKey('trade_item_header_id');
     $this->setForeignTable('trade_item_headers');
    }
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->increments('id');
            $table->string('additional_trade_item_classification_codelist_code', 80);
            $table->jsonb('additional_trade_item_classification_value');

            $table->integer($this->foreignKey)->unsigned();
            $table->foreign($this->foreignKey)
              ->references('id')->on($this->tableName)
              ->onDelete('cascade');
            $table->timestamps();
        });
    }

}
