<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnimalNutrientDetailsTable extends ValidooMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function __construct(){
       $this->setTableName('animal_nutrient_details');
       $this->setForeignKey('animal_feeding_detail_id');
       $this->setForeignTable('animal_feeding_details');
     }
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->increments('id');
            $table->string('animal_nutrient_type_code', 80);
            $table->unsignedDecimal('animal_nutrient_exact_percentage');
            $table->unsignedDecimal('animal_nutrient_minimum_percentage');
            $table->unsignedDecimal('animal_nutrient_maximum_percentage');
            $table->jsonb('animal_nutrient_quantity_contained');
            $table->integer($this->foreignKey)->unsigned();
            $table->foreign($this->foreignKey)
              ->references('id')->on($this->foreignTable)
              ->onDelete('cascade');
            $table->timestamps();
        });
    }

}
