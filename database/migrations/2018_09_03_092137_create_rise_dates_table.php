<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRiseDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('rise_data', function (Blueprint $table) {

            $table->charset = 'latin1';
            $table->collate = 'latin1_swedish_ci';

            $table->increments('id');
            $table->tinyInteger('food_cat_1_id');
            $table->string('food_cat_1_name');

            $table->tinyInteger('food_cat_2_id');
            $table->string('food_cat_2_name');

            $table->tinyInteger('food_cat_3_id');
            $table->string('food_cat_3_name');

            $table->tinyInteger('slv_id');
            $table->string('slv_name');

            $table->tinyInteger('activity_id');
            $table->string('activity_name');
            $table->float('ghg_total_value', 2, 2);

            $table->tinyInteger('region_id');
            $table->string('region_name');
            $table->string('region_source');

            $table->string('production_type');
            $table->string('production_type_source');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rise_data');
    }
}
