<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarrantyDurationsTable extends ValidooMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function __construct(){
      $this->setTableName('warranty_durations');
      $this->setForeignKey('warranty_id');
      $this->setForeignTable('warranties');
    }
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedDecimal('warranty_duration');
            $table->string('measurement_unit_code', 80);
            $table->integer($this->foreignKey)->unsigned();
            $table->foreign($this->foreignKey)
              ->references('id')->on($this->foreignTable)
              ->onDelete('cascade');
            $table->timestamps();
        });
    }

}
