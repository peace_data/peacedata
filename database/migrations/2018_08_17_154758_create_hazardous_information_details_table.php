<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHazardousInformationDetailsTable extends ValidooMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function __construct(){
     $this->setTableName('hazardous_information_details');
     $this->setForeignKey('dangerous_good_id');
     $this->setForeignTable('dangerous_goods');
    }
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->increments('id');
            $table->string('class_of_dangerous_goods',4 );
            $table->string('dangerous_goods_classification_code', 70);
            $table->string('dangerous_goods_packing_group', 35);
            $table->string('dangerous_goods_shipping_name', 200);
            $table->string('dangerous_goods_special_provisions', 70);
            $table->string('dangerous_goods_transport_category_code', 80);
            $table->string('united_nations_dangerous_goods_number', 4);

            $table->jsonb('dangerous_goods_technical_name');
            $table->jsonb('dangerous_hazardous_label');
            $table->integer($this->foreignKey)->unsigned();
            $table->foreign($this->foreignKey)
              ->references('id')->on($this->foreignTable)
              ->onDelete('cascade');

            $table->timestamps();
        });
    }

}
