<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTradeItemIdentitiesTable extends ValidooMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function __construct(){
     $this->setTableName('trade_item_identities');
     $this->setForeignKey('trade_item_header_id');
     $this->setForeignTable('trade_item_headers');
    }

    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->increments('id');
            $table->string('gtin', 14);
            $table->string('target_market_country_code', 80);
            $table->string('context_identification', 80)->default('');
            $table->string('preliminary_item_status_code', 80);
            $table->boolean('is_trade_item_a_base_unit');
            $table->boolean('is_trade_item_a_consumer_unit');
            $table->boolean('is_trade_item_a_despatch_unit');
            $table->boolean('is_trade_item_an_invoice_unit');
            $table->boolean('is_trade_item_an_orderable_unit');
            $table->boolean('is_trade_item_a_service');
            $table->string('is_trade_item_a_display_unit');
            $table->string('trade_item_unit_descriptor_code', 80);
            $table->dateTime('production_variant_effective_date_time');
//            $table->dateTime('additional_trade_item_identification_type_code_article_number');
            $table->jsonb('additional_trade_items_identities')->default('{}');
            $table->jsonb('reference_to_trade_item')->default('{}');
            $table->integer($this->foreignKey)->unsigned();

            $table->foreign($this->foreignKey)
              ->references('id')->on($this->foreignTable)
              ->onDelete('cascade');
            $table->timestamps();
        });
    }
}
