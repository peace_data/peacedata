<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateValidooImagesTable extends ValidooMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function __construct(){
       $this->setTableName('images');
       $this->setForeignKey('gtin');
       $this->setForeignTable('trade_items');
    }

    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->increments('id');
            $table->string('thumb');
            $table->string('src');
            $table->string('middle');

            $table->string($this->foreignKey);
            
            $table->timestamps();
        });
    }

}
