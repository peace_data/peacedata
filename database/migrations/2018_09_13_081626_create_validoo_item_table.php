<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateValidooItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('validoo_items', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('validoo_trade_item_id');
        $table->foreign('validoo_trade_item_id')
          ->references('id')
          ->on('validoo_trade_items');
        $table->boolean('food')->default(true);
        $table->boolean('published')->default(false);

        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

      Schema::dropIfExists('validoo_items');
    }
}
