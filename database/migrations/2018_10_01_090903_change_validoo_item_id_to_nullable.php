<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Database\Views\TradeItemView;
class ChangeValidooItemIdToNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        TradeItemView::destroy();
        Schema::table('trade_items', function (Blueprint $table) {
            $table->integer('validoo_trade_item_id')->nullable()->change();
        });  

        TradeItemView::create();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        TradeItemView::destroy();

        Schema::table('trade_items', function (Blueprint $table) {
            $table->integer('validoo_trade_item_id')->nullable(false)->change();
        });     
        TradeItemView::create();

    }
}
