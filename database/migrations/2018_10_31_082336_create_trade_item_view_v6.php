<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Database\Views\TradeItemViewV5;
use Database\Views\TradeItemViewV6;

class CreateTradeItemViewV6 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        TradeItemViewV6::create();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        TradeItemViewV5::create();

    }
}
