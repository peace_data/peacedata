<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTradeItemSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trade_item_subscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('gtin');
            $table->string('status_code');
            $table->string('subscribe_id');
            $table->string('message');
            $table->enum('type', ['image', 'info']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trade_item_subscriptions');
    }
}
