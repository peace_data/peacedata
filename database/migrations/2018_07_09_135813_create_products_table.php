<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('weight');
            $table->integer('price');
            $table->string('price_unit');
            $table->string('currency');
            $table->integer('compare_price');
            $table->string('compare_unit');
            $table->string('brand');
            $table->json('informations');
            $table->string('category');
            $table->boolean('isEko');
            $table->string('externalKey');
            $table->unsignedInteger('provider_id');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
