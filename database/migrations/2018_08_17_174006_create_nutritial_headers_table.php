<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNutritialHeadersTable extends ValidooMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function __construct(){
      $this->setTableName('nutritial_headers');
      $this->setForeignKey('nutritional_information_id');
      $this->setForeignTable('nutritional_informations');
    }
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->increments('id');
            $table->string('preparation_state_code', 80);
            $table->string('nutrient_basis_quantity_type_code', 80);
            $table->jsonb('daily_value_intake_reference');
            $table->jsonb('nutrient_basis_quantity');
            $table->jsonb('serving_size');
            $table->jsonb('serving_size_description');
            $table->integer($this->foreignKey)->unsigned();
            $table->foreign($this->foreignKey)
              ->references('id')->on($this->foreignTable)
              ->onDelete('cascade');
            $table->timestamps();
        });
    }

}
