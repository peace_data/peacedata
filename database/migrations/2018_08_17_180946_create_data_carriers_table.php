<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataCarriersTable extends ValidooMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function __construct(){
      $this->setTableName('data_carriers');
      $this->setForeignKey('trade_item_id');
      $this->setForeignTable('trade_items');
    }
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->increments('id');
            $table->string('application_identifier_type_code', 80);
            $table->string('data_carrier_family_type_code', 80);
            $table->string('data_carrier_presence_code', 80);
            $table->integer($this->foreignKey)->unsigned();
            $table->foreign($this->foreignKey)
              ->references('id')->on($this->foreignTable)
              ->onDelete('cascade');
            $table->timestamps();
        });
    }

}
