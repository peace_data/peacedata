<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemperaturesTable extends ValidooMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function __construct(){
       $this->setTableName('temperatures');
       $this->setForeignKey('trade_item_id');
       $this->setForeignTable('trade_items');
     }
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->increments('id');
            $table->string('trade_item_temperature_condition_type_code',80 )->default('');

            $table->unsignedInteger('trade_item_id');
            $table->foreign('trade_item_id')
              ->references('id')->on('validoo_trade_items')
              ->onDelete('cascade');

            $table->timestamps();
        });
    }

}
