<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdditionalTradeItemDimensionsTable extends ValidooMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function __construct(){
      $this->setTableName('additional_trade_item_dimensions');
      $this->setForeignKey('measurement_id');
      $this->setForeignTable('measurements');
    }
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->increments('id');
            $table->string('dimension_type_code',80 );

            $table->jsonb('width');
            $table->jsonb('height');
            $table->jsonb('depth');
            $table->jsonb('trade_item_nesting');
            $table->integer($this->foreignKey)->unsigned();
            $table->foreign($this->foreignKey)
              ->references('id')->on($this->foreignTable)
              ->onDelete('cascade');

            $table->timestamps();
        });
    }
}
