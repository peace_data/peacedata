<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeletedToValidooTradeItems extends ValidooMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function __construct(){
        $this->setTableName('trade_items');
    }

    public function up()
    {
        Schema::table($this->tableName, function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });
    }

    public function down()
    {
        Schema::table($this->tableName, function (Blueprint $table) {
            $table->dropColumn('deleted');
        });
    }
}
