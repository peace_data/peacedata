<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagingsTable extends ValidooMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function __construct(){
      $this->setTableName('packagings');
      $this->setForeignKey('trade_item_id');
      $this->setForeignTable('trade_items');
    }
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->increments('id');
            $table->string('packaging_feature_code', 80);
            $table->string('packaging_function_code', 80);
            $table->string('packaging_type_code', 80);
            $table->string('packaging_terms_and_conditions_code', 80);
            $table->string('platform_type_code', 80);
            $table->jsonb('packaging_weight');
            $table->jsonb('packaging_height');
            $table->integer($this->foreignKey)->unsigned();
            $table->foreign($this->foreignKey)
              ->references('id')->on($this->foreignTable)
              ->onDelete('cascade');
            $table->timestamps();
        });
    }

}
