<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBatteryInformationsTable extends ValidooMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function __construct(){
       $this->setTableName('battery_informations');
       $this->setForeignKey('battery_id');
       $this->setForeignTable('batteries');
     }

    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->increments('id');
            $table->string('battery_technology_type_code', 80);
            $table->string('are_batteries_built_in');
            $table->string('battery_type_code', 80);
            $table->unsignedInteger('quantity_of_batteries_required');
            $table->jsonb('battery_weight');
            $table->integer($this->foreignKey)->unsigned();
            $table->foreign($this->foreignKey)
              ->references('id')->on($this->foreignTable)
              ->onDelete('cascade');
            $table->timestamps();
        });
    }

}
