<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColoursTable extends ValidooMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function __construct(){
        $this->setTableName('colours');
        $this->setForeignKey('description_id');
        $this->setForeignTable('descriptions');
    }

    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->increments('id');

            $table->jsonb('colour_code');
            $table->jsonb('colour_description');

            $table->integer($this->foreignKey)->unsigned();
            $table->foreign($this->foreignKey)
                ->references('id')->on($this->foreignTable)
                ->onDelete('cascade');

            $table->timestamps();
        });
    }
}
