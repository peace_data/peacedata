<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTradeItemLabelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trade_item_label', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('trade_item_id')->unsigned();
            $table->foreign('trade_item_id')
                ->references('id')
                ->on('trade_items')->onDelete('cascade');

            $table->integer('label_id')->unsigned();
             $table->foreign('label_id')
                ->references('id')
                ->on('labels')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trade_item_label');
    }
}
