<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFishReportingInformationsTable extends ValidooMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function __construct(){
       $this->setTableName('fish_reporting_informations');
       $this->setForeignKey('dairy_fish_meate_poultry_item_id');
       $this->setForeignTable('dairy_fish_meate_poultry_items');
     }
    public function up()
    {
        Schema::create(env('DB_VALIDOO_TABLE_PREFIX').'fish_reporting_informations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('species_for_fishery_statistics_purposes_code', 80);
            $table->string('species_for_fishery_statistics_purposes_name', 500);
            $table->jsonb('fish_catch_information');
            $table->integer($this->foreignKey)->unsigned();
            $table->foreign($this->foreignKey)
              ->references('id')->on($this->foreignTable)
              ->onDelete('cascade');
            $table->timestamps();
        });
    }

}
