<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToRefferenceMeasurement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('references_measurements', function (Blueprint $table) {
            $table->string('child_measurement')->default('');
            $table->integer('child_measurement_quantity')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('references_measurements', function (Blueprint $table) {
            $table->dropColumn(['child_measurement', 'child_measurement_quantity']);
        });
    }
}
