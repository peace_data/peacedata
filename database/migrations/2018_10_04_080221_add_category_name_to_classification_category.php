<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoryNameToClassificationCategory  extends ValidooMigration
{
    public function __construct(){
        $this->setTableName('clasification_categories');
    }

    public function up()
    {
        Schema::table($this->tableName, function (Blueprint $table) {
            $table->string('gpc_category_name')->default('');
        });
    }

    public function down(){
        Schema::table($this->tableName, function (Blueprint $table) {
            $table->dropColumn('gpc_category_name');
        });
    }
}
