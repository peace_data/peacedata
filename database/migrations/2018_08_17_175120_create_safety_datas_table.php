<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSafetyDatasTable extends ValidooMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function __construct(){
      $this->setTableName('safety_datas');
      $this->setForeignKey('trade_item_id');
      $this->setForeignTable('trade_items');
    }
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->increments('id');
            $table->string('chemical_ingredient_name', 200);
            $table->string('gHS_signal_words_code', 80);
            $table->string('gHS_symbol_description_code', 35);
            $table->jsonb('hazard_statement');
            $table->jsonb('precautionary_statement');
            $table->integer($this->foreignKey)->unsigned();
            $table->foreign($this->foreignKey)
              ->references('id')->on($this->foreignTable)
              ->onDelete('cascade');

            $table->timestamps();
        });
    }

}
