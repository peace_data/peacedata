<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreparationsTable extends ValidooMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function __construct(){
      $this->setTableName('preparations');
      $this->setForeignKey('preparation_and_serving_id');
      $this->setForeignTable('preparation_and_servings');
    }
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->increments('id');
            $table->string('preparation_type_code', 80);
            $table->jsonb('maximum_optimum_consumption_temparature');
            $table->jsonb('minimum_optimum_consumption_temparature');
            $table->jsonb('preparation_instructions');
            $table->jsonb('serving_suggestions');
            $table->integer($this->foreignKey)->unsigned();
            $table->foreign($this->foreignKey)
              ->references('id')->on($this->foreignTable)
              ->onDelete('cascade');
            $table->timestamps();
        });
    }

}
