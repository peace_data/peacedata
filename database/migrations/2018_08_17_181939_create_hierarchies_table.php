<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHierarchiesTable extends ValidooMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function __construct(){
      $this->setTableName('hierarchies');
      $this->setForeignKey('trade_item_id');
      $this->setForeignTable('trade_items');
    }
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->increments('id');
            $table->string('is_trade_item_packed_irregularly');
            $table->unsignedInteger('quantity_of_complete_layers_contained_in_a_trade_item');
            $table->unsignedInteger('quantity_of_trade_items_contained_in_a_complete_layer');
            $table->jsonb('layer_height');
            $table->integer($this->foreignKey)->unsigned();
            $table->foreign($this->foreignKey)
              ->references('id')->on($this->foreignTable)
              ->onDelete('cascade');

            $table->timestamps();
        });
    }

}
