<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDangerousGoodsTable extends ValidooMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function __construct(){
      $this->setTableName('dangerous_goods');
      $this->setForeignKey('trade_item_id');
      $this->setForeignTable('trade_items');
    }
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->increments('id');
            $table->string('aDR_dangerous_goods_limited_quantities_code', 35);
            $table->string('aDR_tunnel_restriction_code', 70);
            $table->string('dangerous_goods_regulation_code', 70);
            $table->jsonb('flash_point_temperature');
            $table->jsonb('hazardous_material_additional_information');

            $table->integer($this->foreignKey)->unsigned();
            $table->foreign($this->foreignKey)
              ->references('id')->on($this->foreignTable)
              ->onDelete('cascade');
            $table->timestamps();
        });
    }
}
