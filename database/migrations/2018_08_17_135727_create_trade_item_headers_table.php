<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class CreateTradeItemHeadersTable extends ValidooMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */


    public function __construct(){
      $this->setTableName('trade_item_headers');
      $this->setForeignKey('trade_item_id');
      $this->setForeignTable('trade_items');
    }

    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('creation_date_time');
            $table->string('type', 17);
            $table->string('data_recipient', 13);
            $table->jsonb('information_provider_of_trade_item');
            $table->jsonb('manufacturer_of_trade_item');
            $table->jsonb('brand_owner');
            $table->jsonb('party_in_role');
            $table->jsonb('trade_item_synchronisation_dates');
            $table->jsonb('trade_item_contact_information');
            $table->integer($this->foreignKey)->unsigned();
            $table->foreign($this->foreignKey)
              ->references('id')->on($this->foreignTable)
              ->onDelete('cascade');
            $table->timestamps();
        });
    }
}
