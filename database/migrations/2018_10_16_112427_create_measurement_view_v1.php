<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Database\Views\Measurements\V1;

class CreateMeasurementViewV1 extends Migration
{

    public function up()
    {
        V1::create();
    }

    public function down()
    {
        V1::destroy();
    }
}
