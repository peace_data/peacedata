<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdditionalClimateScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('additional_climate_scores', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('global_code');
            $table->string('global_name')->nullable();
            $table->integer('region_code')->nullable();
            $table->string('region_name')->nullable();
            $table->integer('sub_region_code')->nullable();
            $table->string('sub_region_name')->nullable();
            $table->integer('sub_region_score')->nullable();
            $table->integer('intermediate_region_code')->nullable();
            $table->string('intermediate_region_name')->nullable();
            $table->string('country_or_area')->nullable();
            $table->integer('m49_code');
            $table->string('iso_3166_1')->nullable();
            $table->string('iso_alpha3_code')->nullable();
            $table->integer('country_score')->nullable();
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('additional_climate_scores');
    }
}
