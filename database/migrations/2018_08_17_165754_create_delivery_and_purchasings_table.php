<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryAndPurchasingsTable extends ValidooMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function __construct(){
     $this->setTableName('delivery_and_purchasings');
     $this->setForeignKey('trade_item_id');
     $this->setForeignTable('trade_items');
    }
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('consumer_first_availability_date_time');
            $table->dateTime('first_ship_date_time');
            $table->string('is_one_time_buy');
            $table->dateTime('start_availability_date_time');
            $table->dateTime('end_availability_date_time');
            $table->integer($this->foreignKey)->unsigned();
            $table->foreign($this->foreignKey)
              ->references('id')->on($this->foreignTable)
              ->onDelete('cascade');

            $table->timestamps();
        });
    }

}
