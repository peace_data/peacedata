<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnimalFeedingDetailsTable extends ValidooMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function __construct(){
       $this->setTableName('animal_feeding_details');
       $this->setForeignKey('animal_feeding_id');
       $this->setForeignTable('animal_feedings');
     }
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->increments('id');
            $table->jsonb('animal_nutrient_quantity_contained_basis');
            $table->jsonb('feeding_amount');
            $table->jsonb('feeding_amount_basis_description');
            $table->jsonb('maximum_feeding_amount');
            $table->jsonb('minimum_feeding_amount');
            $table->jsonb('recomended_frequency_of_feeding');
            $table->integer($this->foreignKey)->unsigned();
            $table->foreign($this->foreignKey)
              ->references('id')->on($this->foreignTable)
              ->onDelete('cascade');
            $table->timestamps();
        });
    }

}
