<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePackagingMarkedFreeFromCodeType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('validoo_health_and_wellness_markings', function (Blueprint $table) {
            $table->dropColumn('packaging_marked_free_from_code');
        });
         Schema::table('validoo_health_and_wellness_markings', function (Blueprint $table) {
            $table->jsonb('packaging_marked_free_from_code')->default('{}');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('validoo_health_and_wellness_markings', function (Blueprint $table) {
            $table->dropColumn('packaging_marked_free_from_code');
        });
         Schema::table('validoo_health_and_wellness_markings', function (Blueprint $table) {
            $table->string('packaging_marked_free_from_code', 80)->default('');
        });
    }
}
