<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Database\Views\TradeItemViewV6;
use Database\Views\TradeItemViewV7;

class CreateTradeItemViewV7 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        TradeItemViewV7::create();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        TradeItemViewV6::create();
    }
}
