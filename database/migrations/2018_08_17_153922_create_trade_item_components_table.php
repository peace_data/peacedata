<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTradeItemComponentsTable extends ValidooMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */


    public function __construct(){
     $this->setTableName('trade_item_components');
     $this->setForeignKey('trade_item_header_id');
     $this->setForeignTable('trade_item_headers');
    }
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('number_of_pieces_in_set');
            $table->unsignedInteger('total_number_of_components');
            $table->unsignedInteger('multiple_container_quantity');

            $table->integer($this->foreignKey)->unsigned();
            $table->foreign($this->foreignKey)
              ->references('id')->on($this->foreignTable)
              ->onDelete('cascade');
            $table->timestamps();
        });
    }
}
