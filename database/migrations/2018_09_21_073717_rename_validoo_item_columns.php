<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameValidooItemColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('validoo_item_attributtes', function (Blueprint $table) {
            $table->renameColumn('validoo_item_id', 'trade_item_id');
        });    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('validoo_item_attributtes', function (Blueprint $table) {
            $table->renameColumn('trade_item_id', 'validoo_item_id');
        }); 
    }
}
