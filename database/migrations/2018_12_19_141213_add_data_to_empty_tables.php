<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDataToEmptyTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('validoo_dangerous_substances', function (Blueprint $table) {
            $table->jsonb('data_for_empty_model')->default('{}');
        });
        Schema::table('validoo_dairy_fish_meate_poultry_items', function (Blueprint $table) {
            $table->jsonb('data_for_empty_model')->default('{}');
        });
        Schema::table('validoo_ingredients', function (Blueprint $table) {
            $table->jsonb('ingredient_statements')->default('{}');
            $table->jsonb('additive_informations')->default('{}');
        });
        Schema::table('validoo_warranties', function (Blueprint $table) {
            $table->jsonb('data_for_empty_model')->default('{}');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('validoo_dangerous_substances', function (Blueprint $table) {
            $table->dropColumn('data_for_empty_model')->default('{}');
        });
        Schema::table('validoo_dairy_fish_meate_poultry_items', function (Blueprint $table) {
            $table->dropColumn('data_for_empty_model')->default('{}');
        });
        Schema::table('validoo_ingredients', function (Blueprint $table) {
            $table->dropColumn('ingredient_statements')->default('{}');
            $table->dropColumn('additive_informations')->default('{}');
        });
        Schema::table('validoo_warranties', function (Blueprint $table) {
            $table->dropColumn('data_for_empty_model')->default('{}');
        });
    }
}
