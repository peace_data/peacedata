<?php
namespace Database\Views;

use DB;

class TradeItemViewV5{

    public static function create(){
        DB::statement("DROP VIEW IF EXISTS trade_items_view");

        DB::statement("CREATE VIEW trade_items_view AS
                        with brand_name_information as 
                            (select case jsonb_typeof(validoo_descriptions.brand_name_information) 
                                when 'array' 
                                    then brand_name_information 
                                else jsonb_build_array(brand_name_information) 
                                end 
                            from validoo_descriptions, jsonb_array_elements_text(brand_name_information) as t)
                        SELECT DISTINCT trade_item_identity.gtin as gtin,
                            tr_i.id as id,
                            trade_item_header.information_provider_of_trade_item,
                            trade_item_header.creation_date_time,
                            clasification_category.gpc_category_code as category_code,
                            clasification_category.gpc_category_name as category_name,
                            -- descriptions.description_short->{'0'},
                            descriptions.description_short->0->'0' as product_name,
                            brand_name_information->0->'brandName' as brand_name,
                            validoo_sales_informations.price_comparison_content_type_code,
                            validoo_sales_informations.price_comparison_measurement->0->'0' as compare_measurement,
                            validoo_sales_informations.price_comparison_measurement->0->'@attributes'->'measurementUnitCode' as measurement_unit_code
                        FROM trade_items AS tr_i
                        INNER JOIN validoo_trade_items AS val_t_i ON tr_i.validoo_trade_item_id = val_t_i.id
                        INNER JOIN validoo_trade_item_headers AS trade_item_header ON trade_item_header.trade_item_id = val_t_i.id
                        INNER JOIN validoo_trade_item_identities AS trade_item_identity  ON trade_item_identity.trade_item_header_id = trade_item_header.id
                        INNER JOIN validoo_clasification_categories AS clasification_category ON clasification_category.trade_item_header_id = trade_item_header.id
                        INNER JOIN validoo_descriptions AS descriptions ON descriptions.trade_item_id = val_t_i.id
                        LEFT JOIN validoo_sales_informations ON validoo_sales_informations.trade_item_id = val_t_i.id    
                    ");
    }
    public static function destroy(){
        DB::statement("DROP VIEW IF EXISTS trade_items_view");
    }

}


