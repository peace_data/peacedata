<?php
namespace Database\Views;

use DB;

class TradeItemViewV2{

    public static function create(){
        DB::statement("DROP VIEW IF EXISTS trade_items_view");

        DB::statement("CREATE VIEW trade_items_view AS
                        SELECT DISTINCT trade_item_identity.gtin as gtin,
                            trade_item_header.information_provider_of_trade_item,
                            trade_item_header.creation_date_time,
                            clasification_category.gpc_category_code as category_code,
                            clasification_category.gpc_category_name as category_name,
                            -- descriptions.description_short->{'0'},
                            descriptions.description_short->0->'0' as product_name,
                            (jsonb_array_elements_text(descriptions.brand_name_information)::jsonb)->'brandName' as brand_name
                        FROM trade_items AS tr_i
                        INNER JOIN validoo_trade_items AS val_t_i ON tr_i.validoo_trade_item_id = val_t_i.id
                        INNER JOIN validoo_trade_item_headers AS trade_item_header ON trade_item_header.trade_item_id = val_t_i.id
                        INNER JOIN validoo_trade_item_identities AS trade_item_identity  ON trade_item_identity.trade_item_header_id = trade_item_header.id
                        INNER JOIN validoo_clasification_categories AS clasification_category ON clasification_category.trade_item_header_id = trade_item_header.id
                        INNER JOIN validoo_descriptions AS descriptions ON descriptions.trade_item_id = val_t_i.id

                        ");
    }
    public static function destroy(){
        DB::statement("DROP VIEW IF EXISTS trade_items_view");

    }

}
