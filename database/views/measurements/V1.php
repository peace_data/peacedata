<?php
namespace Database\Views\Measurements;

use DB;

class V1{

    public static function create(){
        DB::statement("DROP VIEW IF EXISTS measurements_view");
        DB::statement("CREATE VIEW measurements_view AS
                        SELECT DISTINCT validoo_sales_informations.price_comparison_content_type_code,
                            validoo_sales_informations.price_comparison_measurement->0->'0' as measurement,
                            validoo_sales_informations.price_comparison_measurement->0->'@attributes'->'measurementUnitCode' as measurement_unit_code,
                            references_measurements.code,
                            references_measurements.title as compare_measurement_title
                        FROM validoo_sales_informations
                        INNER JOIN references_measurements ON validoo_sales_informations.price_comparison_content_type_code = references_measurements.code
                        ");
    }
    public static function destroy(){
        DB::statement("DROP VIEW IF EXISTS measurements_view");

    }

}
