<?php

use Illuminate\Database\Seeder;

use App\Models\Provider;
use App\Models\ProviderVersion;


class ProviderVersionsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        ProviderVersion::truncate();

        $providers = Provider::all();

        foreach($providers as $provider) {
            ProviderVersion::create([
                'provider_id' => $provider->id,
                'version' => 1,
                'created_at' => \Carbon\Carbon::now()
            ]);
        }
    }
}
