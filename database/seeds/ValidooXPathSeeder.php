<?php

use Illuminate\Database\Seeder;
use App\Models\Validoo\References\XPath;

class ValidooXPathSeeder extends Seeder
{
    const UNBOUNDED = 'unbounded';
    const SINGLE = 'single';
    private $module_puth;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      XPath::truncate();
      $this->module_puth = $this->init_module_path();
      $this->tradeItemHeader();
      $this->additionalTradeItemClasification();
      $this->additionalTradeItemDimension();
      $this->alcohol();
      $this->animalFeeding();
      $this->allergen();
      $this->animalFeedingDetail();
      $this->animalFeedingInformation();
      $this->animalNutrientDetail();
      $this->battery();
      $this->batteryInformation();
      $this->clasificationCategory();
      $this->componentInformation();
      $this->consumerInstruction();
      $this->dairyFishMeatePoultryItem();
      $this->dangerousGood();
      $this->dangerousSubstance();
      $this->dataCarrier();
      $this->deliveryAndPurchasing();
      $this->description();
      $this->colour();
      $this->diet();
      $this->disposal();
      $this->dutyFeeTax();
      $this->fileReference();
      $this->fishReportingInformation();
      $this->handling();
      $this->hazardousInformationDetail();
      $this->health();
      $this->healthAndWellnessMarkings();
      $this->hierarchy();
      $this->humidity();
      $this->ingredient();
      $this->itemActivity();
      $this->itemRegulation();
      $this->lifespan();
      $this->marketingInformations();
      $this->marking();
      $this->measurement();
      $this->nonFoodIngredientInformation();
      $this->nutrientDetail();
      $this->nutritialHeader();
      $this->nutritionalInformation();
      $this->packaging();
      $this->packagingHierarchy();
      $this->packagingMaterial();
      $this->preparation();
      $this->preparationAndServing();
      $this->productYieldInformation();
      $this->returnableAsset();
      $this->riskPhrase();
      $this->safetyData();
      $this->salesInformation();
      $this->size();
      $this->season();
      $this->sustainability();
      $this->temperature();
      $this->temperatureInformation();
      $this->tradeItemComponent();
      $this->tradeItemIdentity();
      $this->variableUnit();
      $this->warrantyDuration();
    }

    private function init_module_path()
    {
        return 'catalogue_item_notification:catalogueItemNotification/catalogueItem/tradeItem/tradeItemInformation/extension/';
    }

    private function additionalTradeItemClasification()
    {
        $model = ucfirst( __FUNCTION__);
        XPath::create($this->buildAssocArr($model, 'catalogue_item_notification:catalogueItemNotification/catalogueItem/tradeItem/gdsnTradeItemClassification/additionalTradeItemClassification/additionalTradeItemClassificationSystemCode', 'single', 'T4238', 'additional_trade_item_classification_codelist_code'));
        XPath::create($this->buildAssocArr($model, 'catalogue_item_notification:catalogueItemNotification/catalogueItem/tradeItem/gdsnTradeItemClassification/additionalTradeItemClassification/additionalTradeItemClassificationValue', 'unbounded', 'T4239', 'additional_trade_item_classification_value'));
    }

    private function additionalTradeItemDimension()
    {
        $model = ucfirst( __FUNCTION__);

        XPath::create($this->buildAssocArr($model, 'trade_item_measurements:tradeItemMeasurementsModule/tradeItemMeasurements/additionalTradeItemDimensions', 'unbounded', 'T3816', 'dimension_type_code'));
        XPath::create($this->buildAssocArr($model, 'trade_item_measurements:tradeItemMeasurementsModule/tradeItemMeasurements/additionalTradeItemDimensions/width', 'single', '', 'width'));
        XPath::create($this->buildAssocArr($model, 'trade_item_measurements:tradeItemMeasurementsModule/tradeItemMeasurements/additionalTradeItemDimensions/height', 'single', '', 'height'));
        XPath::create($this->buildAssocArr($model, 'trade_item_measurements:tradeItemMeasurementsModule/tradeItemMeasurements/additionalTradeItemDimensions/depth', 'single', '', 'depth'));
        XPath::create($this->buildAssocArr($model, 'trade_item_measurements:tradeItemMeasurementsModule/tradeItemMeasurements/tradeItemNesting', 'unbounded', '', 'trade_item_nesting'));
    }

    private function alcohol()
    {
        $model = ucfirst( __FUNCTION__);

        XPath::create($this->buildAssocArr($model, 'alcohol_information:alcoholInformationModule/alcoholInformation/vintage', self::UNBOUNDED, 'T4203', 'vintage'));
        XPath::create($this->buildAssocArr($model, 'alcohol_information:alcoholInformationModule/alcoholInformation/percentageOfAlcoholByVolume', self::SINGLE, 'T2208', 'percentage_of_alcohol_by_volume'));
    }

    private function animalFeeding()
    {
        $model = ucfirst( __FUNCTION__);

        XPath::create($this->buildAssocArr($model, 'animal_feeding:animalFeedingModule/animalFeeding/feedLifestage', self::SINGLE, '', 'feed_life_stage'));
        XPath::create($this->buildAssocArr($model, 'animal_feeding:animalFeedingModule/animalFeeding/maximumWeightOfAnimalBeingFed', self::UNBOUNDED, '', 'maximum_weight_of_animal_fed'));
        XPath::create($this->buildAssocArr($model, 'animal_feeding:animalFeedingModule/animalFeeding/minimumWeightOfAnimalBeingFed', self::UNBOUNDED, '', 'minimum_weight_of_animal_fed'));
    }

    private function allergen()
    {
        $model = ucfirst( __FUNCTION__);

        XPath::create($this->buildAssocArr($model, $this->module_puth.'allergen_information:allergenInformationModule/allergenRelatedInformation/allergenSpecificationAgency', self::SINGLE, 'T4080', 'allergen_specification_agency'));
        XPath::create($this->buildAssocArr($model, $this->module_puth.'allergen_information:allergenInformationModule/allergenRelatedInformation/allergenSpecificationName', self::SINGLE, 'T4081', 'allergen_specification_name'));
        XPath::create($this->buildAssocArr($model, $this->module_puth.'allergen_information:allergenInformationModule/allergenRelatedInformation/allergenStatement', self::UNBOUNDED, '', 'allergen_statement'));
        XPath::create($this->buildAssocArr($model, $this->module_puth.'allergen_information:allergenInformationModule/allergenRelatedInformation/allergen', self::UNBOUNDED, '', 'allergen'));

    }

    private function animalFeedingDetail()
    {
        $model = ucfirst( __FUNCTION__);

        XPath::create($this->buildAssocArr($model, $this->module_puth.'animal_feeding:animalFeedingModule/animalFeeding/animalFeedingDetail/animalNutrientQuantityContainedBasis', self::UNBOUNDED, '', 'animal_nutrient_quantity_contained_basis'));
        XPath::create($this->buildAssocArr($model, $this->module_puth.'animal_feeding:animalFeedingModule/animalFeeding/animalFeedingDetail/feedingAmount', self::UNBOUNDED, '', 'feeding_amount'));
        XPath::create($this->buildAssocArr($model, $this->module_puth.'animal_feeding:animalFeedingModule/animalFeeding/animalFeedingDetail/feedingAmountBasisDescription', self::UNBOUNDED, '', 'feeding_amount_basis_description'));
        XPath::create($this->buildAssocArr($model, $this->module_puth.'animal_feeding:animalFeedingModule/animalFeeding/animalFeedingDetail/maximumFeedingAmount', self::SINGLE, '', 'maximum_feeding_amount'));
        XPath::create($this->buildAssocArr($model, $this->module_puth.'animal_feeding:animalFeedingModule/animalFeeding/animalFeedingDetail/minimumFeedingAmount', self::SINGLE, '', 'minimum_feeding_amount'));
        XPath::create($this->buildAssocArr($model, $this->module_puth.'animal_feeding:animalFeedingModule/animalFeeding/animalFeedingDetail/recommendedFrequencyOfFeeding', self::UNBOUNDED, '', 'recomended_frequency_of_feeding'));
    }

    private function animalFeedingInformation()
    {
        $model = ucfirst( __FUNCTION__);

        XPath::create($this->buildAssocArr($model, $this->module_puth.'animal_feeding:animalFeedingModule/feedType', self::UNBOUNDED, 'T4223', 'feed_type'));
        XPath::create($this->buildAssocArr($model, $this->module_puth.'animal_feeding:animalFeedingModule/targetedConsumptionBy', self::UNBOUNDED, 'T4222', 'targeted_consumption_by'));
        XPath::create($this->buildAssocArr($model, $this->module_puth.'animal_feeding:animalFeedingModule/animalNutritionalClaim', self::UNBOUNDED, '', 'animal_nutrition_claim'));

    }

    private function animalNutrientDetail()
    {
        $model = ucfirst( __FUNCTION__);

        XPath::create($this->buildAssocArr($model, $this->module_puth.'animal_feeding:animalFeedingModule/animalFeeding/animalFeedingDetail/animalNutrientDetail/animalNutrientTypeCode', self::SINGLE, 'T4217', 'animal_nutrient_type_code'));
        XPath::create($this->buildAssocArr($model, $this->module_puth.'animal_feeding:animalFeedingModule/animalFeeding/animalFeedingDetail/animalNutrientDetail/animalNutrientExactPercentage', self::SINGLE, 'T4218', 'animal_nutrient_exact_percentage'));
        XPath::create($this->buildAssocArr($model, $this->module_puth.'animal_feeding:animalFeedingModule/animalFeeding/animalFeedingDetail/animalNutrientDetail/animalNutrientMinimumPercentage', self::SINGLE, 'T4220', 'animal_nutrient_minimum_percentage'));
        XPath::create($this->buildAssocArr($model, $this->module_puth.'animal_feeding:animalFeedingModule/animalFeeding/animalFeedingDetail/animalNutrientDetail/animalNutrientMaximumPercentage', self::SINGLE, 'T4219', 'animal_nutrient_maximum_percentage'));
        XPath::create($this->buildAssocArr($model, $this->module_puth.'animal_feeding:animalFeedingModule/animalFeeding/animalFeedingDetail/animalNutrientDetail/animalNutrientQuantityContained', self::UNBOUNDED, '', 'animal_nutrient_quantity_contained'));
    }

    private function battery()
    {
        $model = ucfirst( __FUNCTION__);

        XPath::create($this->buildAssocArr($model, 'battery_information:batteryInformationModule/areBatteriesIncluded', self::SINGLE, 'T3800', 'are_batteries_included'));
        XPath::create($this->buildAssocArr($model, 'battery_information:batteryInformationModule/areBatteriesRequired', self::SINGLE, 'T3801', 'are_batteries_required'));
    }

    private function batteryInformation()
    {
        $model = ucfirst( __FUNCTION__);

        XPath::create($this->buildAssocArr($model, 'battery_information:batteryInformationModule/batteryDetail/batteryTechnologyTypeCode', self::UNBOUNDED, 'T3803', 'battery_technology_type_code'));
        XPath::create($this->buildAssocArr($model, 'battery_information:batteryInformationModule/batteryDetail/areBatteriesBuiltIn', self::SINGLE, 'T3802', 'are_batteries_built_in'));
        XPath::create($this->buildAssocArr($model, 'battery_information:batteryInformationModule/batteryDetail/batteryTypeCode', self::SINGLE, 'T3804', 'battery_type_code'));
        XPath::create($this->buildAssocArr($model, 'battery_information:batteryInformationModule/batteryDetail/quantityOfBatteriesRequired', self::SINGLE, 'T3805', 'quantity_of_batteries_required'));
        XPath::create($this->buildAssocArr($model, 'battery_information:batteryInformationModule/batteryDetail/batteryWeight', self::SINGLE, '', 'battery_weight'));
    }

    private function clasificationCategory()
    {
        $model = ucfirst( __FUNCTION__);

        XPath::create($this->buildAssocArr($model, 'catalogue_item_notification:catalogueItemNotification/catalogueItem/tradeItem/gdsnTradeItemClassification/gpcCategoryCode', self::SINGLE, 'T0280', 'gpc_category_code'));
        XPath::create($this->buildAssocArr($model, 'catalogue_item_notification:catalogueItemNotification/catalogueItem/tradeItem/gdsnTradeItemClassification/gpcCategoryName', self::SINGLE, 'AD1', 'gpc_category_name'));
        XPath::create($this->buildAssocArr($model, 'catalogue_item_notification:catalogueItemNotification/catalogueItem/tradeItem/gdsnTradeItemClassification/gDSNTradeItemClassificationAttribute', self::UNBOUNDED, '', 'g_dsn_trade_item_classification_attribute'));
    }

    private function componentInformation()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'catalogue_item_notification:catalogueItemNotification/catalogueItem/tradeItem/tradeItemInformation/tradeItemComponents/componentInformation/';

        XPath::create($this->buildAssocArr($model, $root.'componentNumber', self::SINGLE, 'T3834', 'component_number'));
        XPath::create($this->buildAssocArr($model, $root.'componentIdentification', self::SINGLE, 'T3836', 'component_identification'));
        XPath::create($this->buildAssocArr($model, $root.'componentQuantity', self::SINGLE, 'T3837', 'component_quantity'));
        XPath::create($this->buildAssocArr($model, $root.'componentMultiplePackedQuantity', self::SINGLE, 'T3839', 'component_multiple_packed_quantity'));
        XPath::create($this->buildAssocArr($model, $root.'contextIdentification', self::SINGLE, 'T3815', 'context_identification'));
        XPath::create($this->buildAssocArr($model, $root.'gpcCategoryCode', self::SINGLE, 'T0280', 'gpc_category_code'));
        XPath::create($this->buildAssocArr($model, $root.'componentDescription', self::UNBOUNDED, '', 'component_description'));
    }

    private function consumerInstruction()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'consumer_instructions:consumerInstructionsModule/consumerInstructions/';

        XPath::create($this->buildAssocArr($model, $root.'consumerStorageInstructions', self::UNBOUNDED, '', 'consumer_storage_instructions'));
        XPath::create($this->buildAssocArr($model, $root.'consumerUsageInstructions', self::UNBOUNDED, '', 'consumer_usage_instructions'));
    }

    private function dairyFishMeatePoultryItem()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'dairy_fish_meat_poultry:dairyFishMeatPoultryItemModule';
        XPath::create($this->buildAssocArr($model, $root, self::SINGLE, '', 'data_for_empty_model'));
    }

    private function dangerousGood()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'transportation_hazardous_classification:transportationHazardousClassificationModule/transportationClassification/regulatedTransportationMode/hazardousInformationHeader/';

        XPath::create($this->buildAssocArr($model, $root.'aDRDangerousGoodsLimitedQuantitiesCode', self::SINGLE, 'T4140', 'aDR_dangerous_goods_limited_quantities_code'));
        XPath::create($this->buildAssocArr($model, $root.'aDRTunnelRestrictionCode', self::SINGLE, 'T4140', 'aDR_tunnel_restriction_code'));
        XPath::create($this->buildAssocArr($model, $root.'dangerousGoodsRegulationCode', self::SINGLE, 'T4022', 'dangerous_goods_regulation_code'));
        XPath::create($this->buildAssocArr($model, $root.'flashPointTemperature', self::UNBOUNDED, '', 'flash_point_temperature'));
        XPath::create($this->buildAssocArr($model, $root.'hazardousMaterialAdditionalInformation', self::UNBOUNDED, '', 'hazardous_material_additional_information'));
    }

    private function dangerousSubstance()
    {
        $model = ucfirst( __FUNCTION__);
        XPath::create($this->buildAssocArr($model, 'dangerous_substance_information:dangerousSubstanceInformationModule/dangerousSubstanceInformation', self::UNBOUNDED, '', 'data_for_empty_model'));

    }

    private function dataCarrier()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'trade_item_data_carrier_and_identification:tradeItemDataCarrierAndIdentificationModule/dataCarrier/';

        XPath::create($this->buildAssocArr($model, $root.'applicationIdentifierTypeCode', self::UNBOUNDED, 'T3748', 'application_identifier_type_code'));
        XPath::create($this->buildAssocArr($model, $root.'dataCarrierFamilyTypeCode', self::SINGLE, 'T0187', 'data_carrier_family_type_code'));
        XPath::create($this->buildAssocArr($model, $root.'dataCarrierPresenceCode', self::SINGLE, 'T3747', 'data_carrier_presence_code'));

    }

    private function deliveryAndPurchasing()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'delivery_purchasing_information:deliveryPurchasingInformationModule/deliveryPurchasingInformation/';

        XPath::create($this->buildAssocArr($model, $root.'consumerFirstAvailabilityDateTime', self::SINGLE, 'T2227', 'consumer_first_availability_date_time'));
        XPath::create($this->buildAssocArr($model, $root.'firstShipDateTime', self::SINGLE, 'T3742', 'first_ship_date_time'));
        XPath::create($this->buildAssocArr($model, $root.'isOneTimeBuy', self::SINGLE, 'T4150', 'is_one_time_buy'));
        XPath::create($this->buildAssocArr($model, $root.'startAvailabilityDateTime', self::SINGLE, 'T4727', 'start_availability_date_time'));
        XPath::create($this->buildAssocArr($model, $root.'endAvailabilityDateTime', self::SINGLE, 'T4726', 'end_availability_date_time'));
    }

    private function description()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'trade_item_description:tradeItemDescriptionModule/tradeItemDescriptionInformation/';

        XPath::create($this->buildAssocArr($model, $root.'descriptionShort', self::UNBOUNDED, '', 'description_short'));
        XPath::create($this->buildAssocArr($model, $root.'functionalName', self::UNBOUNDED, '', 'functional_name'));
        XPath::create($this->buildAssocArr($model, $root.'regulatedProductName', self::UNBOUNDED, '', 'regulated_product_name'));
        XPath::create($this->buildAssocArr($model, $root.'tradeItemDescription', self::UNBOUNDED, '', 'trade_item_description'));
        XPath::create($this->buildAssocArr($model, $root.'brandNameInformation', self::SINGLE, '', 'brand_name_information'));
    }

    private function colour()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'trade_item_description:tradeItemDescriptionModule/tradeItemDescriptionInformation/colour/';

        XPath::create($this->buildAssocArr($model, $root.'colourCode', self::SINGLE, '', 'colour_code'));
        XPath::create($this->buildAssocArr($model, $root.'colourDescription', self::UNBOUNDED, '', 'colour_description'));
    }

    private function diet()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'diet_information:dietInformationModule/dietInformation/';

        XPath::create($this->buildAssocArr($model, $root.'dietTypeCode', self::UNBOUNDED, 'T4066', 'diet_type_code'));
    }

    private function disposal()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'trade_item_disposal_information:tradeItemDisposalInformationModule/tradeItemDisposalInformation/';

        XPath::create($this->buildAssocArr($model, $root.'areHazardousComponentsRemovable', self::UNBOUNDED, 'T4144', 'are_hazardous_components_removable'));
    }

    private function dutyFeeTax()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'duty_fee_tax_information:dutyFeeTaxInformationModule/dutyFeeTaxInformation/';

        XPath::create($this->buildAssocArr($model, $root.'dutyFeeTaxAgencyCode', self::SINGLE, 'T4033', 'duty_fee_tax_agency_code'));
        XPath::create($this->buildAssocArr($model, $root.'dutyFeeTaxTypeCode', self::SINGLE, 'T0194', 'duty_fee_tax_agency_code_type'));
        XPath::create($this->buildAssocArr($model, $root.'dutyFeeTax/dutyFeeTaxRate', self::SINGLE, 'T0194', 'duty_fee_tax_rate'));
    }

    private function fileReference()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'referenced_file_detail_information:referencedFileDetailInformationModule/referencedFileHeader/';

        XPath::create($this->buildAssocArr($model, $root.'referencedFileTypeCode', self::UNBOUNDED, 'T2231', 'referenced_file_type_code'));
        XPath::create($this->buildAssocArr($model, $root.'fileFormatName', self::SINGLE, 'T2238', 'file_format_name'));
        XPath::create($this->buildAssocArr($model, $root.'fileName', self::SINGLE, 'T2233', 'file_name'));
        XPath::create($this->buildAssocArr($model, $root.'uniformResourceIdentifier', self::SINGLE, 'T3405', 'uniform_resource_identifier'));
    }

    private function fishReportingInformation()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'dairy_fish_meat_poultry:dairyFishMeatPoultryItemModule/dairyFishMeatPoultryInformation/fishReportingInformation/';

        XPath::create($this->buildAssocArr($model, $root.'speciesForFisheryStatisticsPurposesCode', self::SINGLE, 'T4228', 'species_for_fishery_statistics_purposes_code'));
        XPath::create($this->buildAssocArr($model, $root.'speciesForFisheryStatisticsPurposesName', self::SINGLE, 'T4229', 'species_for_fishery_statistics_purposes_name'));
        XPath::create($this->buildAssocArr($model, $root.'fishCatchInformation', self::UNBOUNDED, '', 'fish_catch_information'));
    }

    private function handling()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'trade_item_handling:tradeItemHandlingModule/tradeItemHandlingInformation/';

        XPath::create($this->buildAssocArr($model, $root.'handlingInstructionsCodeReference', self::UNBOUNDED, 'T4040', 'handling_instructions_code_reference'));
        XPath::create($this->buildAssocArr($model, $root.'tradeItemStacking', self::UNBOUNDED, 'T4040', 'stacking_factor'));
    }

    private function hazardousInformationDetail()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'transportation_hazardous_classification:transportationHazardousClassificationModule/transportationClassification/regulatedTransportationMode/hazardousInformationHeader/hazardousInformationDetail/';

        XPath::create($this->buildAssocArr($model, $root.'classOfDangerousGoods', self::SINGLE, 'T0263', 'class_of_dangerous_goods'));
        XPath::create($this->buildAssocArr($model, $root.'dangerousGoodsClassificationCode', self::SINGLE, 'T3743', 'dangerous_goods_classification_code'));
        XPath::create($this->buildAssocArr($model, $root.'dangerousGoodsPackingGroup', self::SINGLE, 'T0264', 'dangerous_goods_packing_group'));
        XPath::create($this->buildAssocArr($model, $root.'dangerousGoodsShippingName', self::UNBOUNDED, 'T4026', 'dangerous_goods_shipping_name'));
        XPath::create($this->buildAssocArr($model, $root.'dangerousGoodsSpecialProvisions', self::UNBOUNDED, 'T4142', 'dangerous_goods_special_provisions'));
        XPath::create($this->buildAssocArr($model, $root.'dangerousGoodsTransportCategoryCode', self::SINGLE, 'T3744', 'dangerous_goods_transport_category_code'));
        XPath::create($this->buildAssocArr($model, $root.'unitedNationsDangerousGoodsNumber', self::SINGLE, 'T0169', 'united_nations_dangerous_goods_number'));
        XPath::create($this->buildAssocArr($model, $root.'dangerousGoodsTechnicalName', self::UNBOUNDED, '', 'dangerous_goods_technical_name'));
        XPath::create($this->buildAssocArr($model, $root.'dangerousHazardousLabel', self::UNBOUNDED, '', 'dangerous_hazardous_label'));
    }

    private function health()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'health_related_information:healthRelatedInformationModule/healthRelatedInformation/';

        XPath::create($this->buildAssocArr($model, $root.'compulsoryAdditiveLabelInformation', self::UNBOUNDED, '', 'compulsory_additive_label_information'));
        XPath::create($this->buildAssocArr($model, $root.'healthClaimDescription', self::UNBOUNDED, '', 'health_claim_description'));
    }

    private function healthAndWellnessMarkings()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'health_wellness_packaging_marking:healthWellnessPackagingMarkingModule/healthWellnessPackagingMarking/';

        XPath::create($this->buildAssocArr($model, $root.'packagingMarkedDietAllergenCode', self::UNBOUNDED, 'T4028', 'packaging_marked_diet_allergen_code'));
        XPath::create($this->buildAssocArr($model, $root.'packagingMarkedFreeFromCode', self::UNBOUNDED, 'T4031', 'packaging_marked_free_from_code'));
    }

    private function hierarchy()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'trade_item_hierarchy:tradeItemHierarchyModule/tradeItemHierarchy/';

        XPath::create($this->buildAssocArr($model, $root.'isTradeItemPackedIrregularly', self::SINGLE, 'T2239', 'is_trade_item_packed_irregularly'));
        XPath::create($this->buildAssocArr($model, $root.'quantityOfCompleteLayersContainedInATradeItem', self::SINGLE, 'T4021', 'quantity_of_complete_layers_contained_in_a_trade_item'));
        XPath::create($this->buildAssocArr($model, $root.'quantityOfTradeItemsContainedInACompleteLayer', self::SINGLE, 'T0160', 'quantity_of_trade_items_contained_in_a_complete_layer'));
        XPath::create($this->buildAssocArr($model, $root.'layerHeight', self::SINGLE, '', 'layer_height'));
    }

    private function humidity()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'trade_item_humidity_information:tradeItemHumidityInformationModule/tradeItemHumidityInformation/';

        XPath::create($this->buildAssocArr($model, $root.'humidityQualifierCode', self::SINGLE, 'T3847', 'humidity_qualifier_code'));
        XPath::create($this->buildAssocArr($model, $root.'maximumHumidityPercentage', self::SINGLE, 'T0166', 'maximum_humidity_percentage'));
        XPath::create($this->buildAssocArr($model, $root.'minimumHumidityPercentage', self::SINGLE, 'T0165', 'minimum_humidity_percentage'));
    }

    private function ingredient()
    {
       $model = ucfirst( __FUNCTION__);
       XPath::create($this->buildAssocArr($model, 'food_and_beverage_ingredient:foodAndBeverageIngredientModule/ingredientStatement', self::UNBOUNDED, '', 'ingredient_statements'));
       XPath::create($this->buildAssocArr($model, 'food_and_beverage_ingredient:foodAndBeverageIngredientModule/additiveInformation', self::UNBOUNDED, '', 'additive_informations'));

    }

    private function itemActivity()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'place_of_item_activity:placeOfItemActivityModule/';

        XPath::create($this->buildAssocArr($model, $root.'placeOfProductActivity/countryOfOrigin/countryCode', self::UNBOUNDED, 'T0168', 'country_code'));
        XPath::create($this->buildAssocArr($model, $root.'countryOfOriginStatement', self::UNBOUNDED, '', 'country_of_origin_statement'));
        XPath::create($this->buildAssocArr($model, $root.'provenanceStatement', self::UNBOUNDED, '', 'provenance_statement'));
        XPath::create($this->buildAssocArr($model, $root.'importClassification', self::UNBOUNDED, '', 'import_classification'));
        XPath::create($this->buildAssocArr($model, $root.'productActivityDetails', self::UNBOUNDED, '', 'product_activity_details'));

    }

    private function itemRegulation()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'regulated_trade_item:regulatedTradeItemModule/regulatoryInformation/';

        XPath::create($this->buildAssocArr($model, $root.'regulationTypeCode', self::UNBOUNDED, 'T3825', 'regulation_type_code'));
        XPath::create($this->buildAssocArr($model, $root.'regulatoryAct', self::SINGLE, 'T5039', 'regulatory_act'));
        XPath::create($this->buildAssocArr($model, $root.'regulatoryAgency', self::SINGLE, 'T5039', 'regulatory_agency'));
        XPath::create($this->buildAssocArr($model, $root.'permitIdentification', self::UNBOUNDED, '', 'permit_identification'));
    }

    private function lifespan()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'trade_item_lifespan:tradeItemLifespanModule/tradeItemLifespan/';

        XPath::create($this->buildAssocArr($model, $root.'minimumTradeItemLifespanFromTimeOfProduction', self::SINGLE, 'T0167', 'minimum_trade_item_lifespan_from_time_of_production'));
        XPath::create($this->buildAssocArr($model, $root.'minimumTradeItemLifespanFromTimeOfArrival', self::SINGLE, 'T0185', 'minimum_trade_item_lifespan_from_time_of_arrival'));
        XPath::create($this->buildAssocArr($model, $root.'openedTradeItemLifespan', self::SINGLE, 'T2521', 'opened_trade_item_lifespan'));
    }

    private function marketingInformations()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'marketing_information:marketingInformationModule/marketingInformation/';

        XPath::create($this->buildAssocArr($model, $root.'tradeItemFeatureCodeReference', self::UNBOUNDED, 'T3826', 'trade_item_feature_code_reference'));
        XPath::create($this->buildAssocArr($model, $root.'tradeItemKeyWords', self::UNBOUNDED, '', 'trade_item_key_words'));
        XPath::create($this->buildAssocArr($model, $root.'tradeItemMarketingMessage', self::UNBOUNDED, '', 'trade_item_marketing_message'));
        XPath::create($this->buildAssocArr($model, $root.'shortTradeItemMarketingMessage', self::UNBOUNDED, '', 'short_trade_item_marketing_message'));
    }

    private function marking()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'packaging_marking:packagingMarkingModule/packagingMarking/';

        XPath::create($this->buildAssocArr($model, $root.'isPackagingMarkedReturnable', self::SINGLE, 'T0277', 'is_packaging_marked_returnable'));
        XPath::create($this->buildAssocArr($model, $root.'isPriceOnPack', self::SINGLE, 'T1190', 'is_price_on_pack'));
        XPath::create($this->buildAssocArr($model, $root.'packagingMarkedLabelAccreditationCode', self::UNBOUNDED, 'T3777', 'packaging_marked_label_accreditation_code'));
        XPath::create($this->buildAssocArr($model, $root.'packagingDate', self::UNBOUNDED, '', 'date_marking'));
    }

    private function measurement()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'trade_item_measurements:tradeItemMeasurementsModule/tradeItemMeasurements/';

        XPath::create($this->buildAssocArr($model, $root.'depth', self::SINGLE, '', 'depth'));
        XPath::create($this->buildAssocArr($model, $root.'height', self::SINGLE, '', 'height'));
        XPath::create($this->buildAssocArr($model, $root.'width', self::SINGLE, '', 'width'));
        XPath::create($this->buildAssocArr($model, $root.'tradeItemWeight/grossWeight', self::SINGLE, '', 'gross_weight'));
        XPath::create($this->buildAssocArr($model, $root.'netContent', self::UNBOUNDED, '', 'net_content'));
        XPath::create($this->buildAssocArr($model, $root.'tradeItemCompositionWidth', self::UNBOUNDED, '', 'trade_item_composition_width'));
    }

    private function nonFoodIngredientInformation()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'nonfood_ingredient:nonfoodIngredientModule/';

        XPath::create($this->buildAssocArr($model, $root.'nonfoodIngredientStatement', self::UNBOUNDED, '', 'non_food_ingredient_statement'));
        XPath::create($this->buildAssocArr($model, $root.'nonfoodIngredient', self::UNBOUNDED, '', 'non_food_ingredient'));
    }

    private function nutrientDetail()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'nutritional_information:nutritionalInformationModule/nutrientHeader/nutrientDetail/';

        XPath::create($this->buildAssocArr($model, $root.'dailyValueIntakePercent', self::SINGLE, 'T4076', 'daily_value_intake_percent'));
        XPath::create($this->buildAssocArr($model, $root.'nutrientTypeCode', self::SINGLE, 'T4073', 'nutrient_type_code'));
        XPath::create($this->buildAssocArr($model, $root.'measurementPrecisionCode', self::SINGLE, 'T4075', 'measurement_precision_code'));
        XPath::create($this->buildAssocArr($model, $root.'quantityContained', self::UNBOUNDED, '', 'quantity_contained'));
    }

    private function nutritialHeader()
    {

        $model = ucfirst( __FUNCTION__);
        $root = 'nutritional_information:nutritionalInformationModule/nutrientHeader/';

        XPath::create($this->buildAssocArr($model, $root.'preparationStateCode', self::SINGLE, 'T4069', 'preparation_state_code'));
        XPath::create($this->buildAssocArr($model, $root.'nutrientBasisQuantityTypeCode', self::SINGLE, 'T3820', 'nutrient_basis_quantity_type_code'));
        XPath::create($this->buildAssocArr($model, $root.'dailyValueIntakeReference', self::UNBOUNDED, '', 'daily_value_intake_reference'));
        XPath::create($this->buildAssocArr($model, $root.'nutrientBasisQuantity', self::SINGLE, '', 'nutrient_basis_quantity'));
        XPath::create($this->buildAssocArr($model, $root.'servingSize', self::UNBOUNDED, '', 'serving_size'));
        XPath::create($this->buildAssocArr($model, $root.'servingSizeDescription', self::UNBOUNDED, '', 'serving_size_description'));
    }

    private function nutritionalInformation()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'nutritional_information:nutritionalInformationModule/';

        XPath::create($this->buildAssocArr($model, $root.'nutritionalClaim', self::UNBOUNDED, '', 'nutritional_claim'));
    }

    private function packaging()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'packaging_information:packagingInformationModule/packaging/';

        XPath::create($this->buildAssocArr($model, $root.'packagingFeatureCode', self::UNBOUNDED, 'T4237', 'packaging_feature_code'));
        XPath::create($this->buildAssocArr($model, $root.'packagingFunctionCode', self::UNBOUNDED, 'T4124', 'packaging_function_code'));
        XPath::create($this->buildAssocArr($model, $root.'packagingTypeCode', self::SINGLE, 'T0137', 'packaging_type_code'));
        XPath::create($this->buildAssocArr($model, $root.'packagingTermsAndConditionsCode', self::UNBOUNDED, 'T0189', 'packaging_terms_and_conditions_code'));
        XPath::create($this->buildAssocArr($model, $root.'platformTypeCode', self::SINGLE, 'T2244', 'platform_type_code'));
        XPath::create($this->buildAssocArr($model, $root.'packagingWeight', self::SINGLE, '', 'packaging_weight'));
        XPath::create($this->buildAssocArr($model, $root.'packagingHeight', self::SINGLE, '', 'packaging_height'));
    }

    private function packagingHierarchy()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'catalogue_item_notification:catalogueItemNotification/catalogueItem/tradeItem/nextLowerLevelTradeItemInformation/';

        XPath::create($this->buildAssocArr($model, $root.'quantityOfChildren', self::SINGLE, 'T4034', 'quantity_of_children'));
        XPath::create($this->buildAssocArr($model, $root.'totalQuantityOfNextLowerLevelTradeItem', self::SINGLE, 'T4035', 'total_quantity_of_next_lower_level_trade_item'));
        XPath::create($this->buildAssocArr($model, $root.'childTradeItem', self::UNBOUNDED, '', 'child_trade_item'));
    }

    private function packagingMaterial()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'packaging_information:packagingInformationModule/packaging/packagingMaterial/';

        XPath::create($this->buildAssocArr($model, $root.'packagingMaterialTypeCode', self::SINGLE, 'T1188', 'packaging_material_type_code'));
        XPath::create($this->buildAssocArr($model, $root.'packagingMaterialCompositionQuantity', self::UNBOUNDED, '', 'packaging_material_composition_quantity'));
    }

    private function preparation()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'food_and_beverage_preparation_serving:foodAndBeveragePreparationServingModule/preparationServing/';

        XPath::create($this->buildAssocArr($model, $root.'preparationTypeCode', self::SINGLE, 'T4082', 'preparation_type_code'));
        XPath::create($this->buildAssocArr($model, $root.'maximumOptimumConsumptionTemperature', self::UNBOUNDED, '', 'maximum_optimum_consumption_temparature'));
        XPath::create($this->buildAssocArr($model, $root.'minimumOptimumConsumptionTemperature', self::UNBOUNDED, '', 'minimum_optimum_consumption_temparature'));
        XPath::create($this->buildAssocArr($model, $root.'preparationInstructions', self::UNBOUNDED, '', 'preparation_instructions'));
        XPath::create($this->buildAssocArr($model, $root.'servingSuggestion', self::UNBOUNDED, '', 'serving_suggestions'));
    }

    private function preparationAndServing()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'food_and_beverage_preparation_serving:foodAndBeveragePreparationServingModule/';

        XPath::create($this->buildAssocArr($model, $root.'manufacturerPreparationTypeCode', self::UNBOUNDED, 'T4138', 'manufacturer_preparation_type_code'));
        XPath::create($this->buildAssocArr($model, $root.'servingQuantityInformation/maximumNumberOfSmallestUnitsPerPackage', self::SINGLE, 'T4093', 'maximum_number_of_smallest_units_per_package'));
        XPath::create($this->buildAssocArr($model, $root.'servingQuantityInformation/numberOfServingsPerPackage', self::SINGLE, 'T4092', 'number_of_servings_per_package'));
        XPath::create($this->buildAssocArr($model, $root.'servingQuantityInformation/numberOfSmallestUnitsPerPackage', self::SINGLE, 'T4036', 'number_of_smallest_units_per_package'));
    }

    private function productYieldInformation()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'food_and_beverage_preparation_serving:foodAndBeveragePreparationServingModule/preparationServing/productYieldInformation/';

        XPath::create($this->buildAssocArr($model, $root.'productYieldTypeCode', self::SINGLE, 'T4084', 'product_yield_type_code'));
        XPath::create($this->buildAssocArr($model, $root.'productYieldVariationPercentage', self::SINGLE, 'T4085', 'product_yield_variation_percentage'));
        XPath::create($this->buildAssocArr($model, $root.'productYield', self::SINGLE, '', 'product_yield'));
    }

    private function returnableAsset()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'packaging_information:packagingInformationModule/packaging/returnableAsset/';

        XPath::create($this->buildAssocArr($model, $root.'grai', self::SINGLE, 'T3828', 'grai'));
        XPath::create($this->buildAssocArr($model, $root.'returnableAssetPackageDeposit', self::UNBOUNDED, '', 'package_deposit'));
        XPath::create($this->buildAssocArr($model, $root.'returnableAssetsContainedQuantity', self::UNBOUNDED, '', 'returnamble_assets_contained_quantity'));
    }

    private function riskPhrase()
    {
        $model = ucfirst( __FUNCTION__);
        $root = '';

        XPath::create($this->buildAssocArr($model, $root.'dangerous_substance_information:dangerousSubstanceInformationModule/dangerousSubstanceInformation/dangerousSubstanceProperties/riskPhraseCode/enumerationValueInformation/enumerationValue', self::UNBOUNDED, 'T4139', 'enumeration_value'));
        XPath::create($this->buildAssocArr($model, $root.'dangerous_substance_information:dangerousSubstanceInformationModule/dangerousSubstanceInformation/dangerousSubstanceProperties/riskPhraseCode/externalAgencyName', self::SINGLE, 'T4155', 'external_agency_name'));
    }

    private function safetyData()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'safety_data_sheet:safetyDataSheetModule/safetyDataSheetInformation';

        XPath::create($this->buildAssocArr($model, $root.'chemicalInformation/chemicalIngredient/chemicalIngredientName', self::UNBOUNDED, 'T3778', 'chemical_ingredient_name'));
        XPath::create($this->buildAssocArr($model, $root.'/gHSDetail/gHSSignalWordsCode', self::SINGLE, 'T5044', 'gHS_signal_words_code'));
        XPath::create($this->buildAssocArr($model, $root.'/gHSDetail/gHSSymbolDescriptionCode', self::UNBOUNDED, 'T3745', 'gHS_symbol_description_code'));
        XPath::create($this->buildAssocArr($model, $root.'/gHSDetail/hazardStatement', self::UNBOUNDED, '', 'hazard_statement'));
        XPath::create($this->buildAssocArr($model, 'safety_data_sheet:safetyDataSheetModule/safetyDataSheetInformation/gHSDetail/precautionaryStatement/precautionaryStatementsCode', self::UNBOUNDED, '', 'precautionary_statement'));
    }

    private function salesInformation()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'sales_information:salesInformationModule/';

        XPath::create($this->buildAssocArr($model, $root.'salesInformation/consumerSalesConditionCode', self::UNBOUNDED, 'T4131', 'consumer_sales_condition_code'));
        XPath::create($this->buildAssocArr($model, $root.'salesInformation/isDiscountingIllegal', self::SINGLE, 'T4132', 'is_discounting_illegal'));
        XPath::create($this->buildAssocArr($model, $root.'salesInformation/priceComparisonContentTypeCode', self::SINGLE, 'T0145', 'price_comparison_content_type_code'));
        XPath::create($this->buildAssocArr($model, $root.'tradeItemPriceInformation/additionalTradeItemPrice/tradeItemPrice', self::UNBOUNDED, '', 'trade_item_price'));
        XPath::create($this->buildAssocArr($model, $root.'salesInformation/priceComparisonMeasurement', self::UNBOUNDED, '', 'price_comparison_measurement'));
    }

    private function size()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'trade_item_size:tradeItemSizeModule/size/';

        XPath::create($this->buildAssocArr($model, $root.'descriptiveSize', self::SINGLE, '', 'descriptive_size'));
    }

    private function season()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'marketing_information:marketingInformationModule/marketingInformation/season/';

        XPath::create($this->buildAssocArr($model, $root.'isTradeItemSeasonal', self::SINGLE, 'T4120', 'is_trade_item_seasonal'));
        XPath::create($this->buildAssocArr($model, $root.'seasonParameterCode', self::UNBOUNDED, 'T4121', 'season_parameter_code'));
    }

    private function sustainability()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'sustainability_module:sustainabilityModule/sustainabilityInformation/';

        XPath::create($this->buildAssocArr($model, $root.'isTradeItemROHSCompliant', self::SINGLE, 'T2522', 'is_trade_item_RoHS_compliant'));
    }

    private function temperature()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'trade_item_temperature_information:tradeItemTemperatureInformationModule/';

        XPath::create($this->buildAssocArr($model, $root.'tradeItemTemperatureConditionTypeCode', self::SINGLE, 'T4242', 'trade_item_temperature_condition_type_code'));
    }

    private function temperatureInformation()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'trade_item_temperature_information:tradeItemTemperatureInformationModule/tradeItemTemperatureInformation/';

        XPath::create($this->buildAssocArr($model, $root.'temperatureQualifierCode', self::SINGLE, 'T3822', 'temperature_qualifier_code'));
        XPath::create($this->buildAssocArr($model, $root.'maximumTemperature', self::SINGLE, '', 'maximum_temperature'));
        XPath::create($this->buildAssocArr($model, $root.'minimumTemperature', self::SINGLE, '', 'minimum_temperature'));
    }

    private function TradeItem()
    {
        /*TODO model is empty*/
    }

    private function tradeItemComponent()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'catalogue_item_notification:catalogueItemNotification/catalogueItem/tradeItem/tradeItemInformation/tradeItemComponents/';

        XPath::create($this->buildAssocArr($model, $root.'numberOfPiecesInSet', self::SINGLE, 'T3832', 'number_of_pieces_in_set'));
        XPath::create($this->buildAssocArr($model, $root.'totalNumberOfComponents', self::SINGLE, 'T3833', 'total_number_of_components'));
        XPath::create($this->buildAssocArr($model, $root.'multipleContainerQuantity', self::SINGLE, 'T3838', 'multiple_container_quantity'));
    }

    private function tradeItemIdentity()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'catalogue_item_notification:catalogueItemNotification/catalogueItem/tradeItem/';

        XPath::create($this->buildAssocArr($model, $root.'gtin', self::SINGLE, 'T0154', 'gtin'));
        XPath::create($this->buildAssocArr($model, $root.'targetMarket/targetMarketCountryCode', self::SINGLE, 'T4011', 'target_market_country_code'));
        XPath::create($this->buildAssocArr($model, $root.'contextIdentification', self::SINGLE, 'T3815', 'context_identification'));
        XPath::create($this->buildAssocArr($model, $root.'preliminaryItemStatusCode', self::SINGLE, 'T3495', 'preliminary_item_status_code'));
        XPath::create($this->buildAssocArr($model, $root.'isTradeItemABaseUnit', self::SINGLE, 'T4012', 'is_trade_item_a_base_unit'));
        XPath::create($this->buildAssocArr($model, $root.'isTradeItemAConsumerUnit', self::SINGLE, 'T4037', 'is_trade_item_a_consumer_unit'));
        XPath::create($this->buildAssocArr($model, $root.'isTradeItemADespatchUnit', self::SINGLE, 'T4038', 'is_trade_item_a_despatch_unit'));
        XPath::create($this->buildAssocArr($model, $root.'isTradeItemAnInvoiceUnit', self::SINGLE, 'T4014', 'is_trade_item_an_invoice_unit'));
        XPath::create($this->buildAssocArr($model, $root.'isTradeItemAnOrderableUnit', self::SINGLE, 'T0017', 'is_trade_item_an_orderable_unit'));
        XPath::create($this->buildAssocArr($model, $root.'isTradeItemAService', self::SINGLE, 'T4119', 'is_trade_item_a_service'));
        XPath::create($this->buildAssocArr($model, $root.'displayUnitInformation/isTradeItemADisplayUnit', self::SINGLE, 'T2207', 'is_trade_item_a_display_unit'));
        XPath::create($this->buildAssocArr($model, $root.'tradeItemUnitDescriptorCode', self::SINGLE, 'T4010', 'trade_item_unit_descriptor_code'));
        XPath::create($this->buildAssocArr($model, $root.'tradeItemInformation/productionVariantEffectiveDateTime', self::SINGLE, 'T5043', 'production_variant_effective_date_time'));
        XPath::create($this->buildAssocArr($model, $root.'additionalTradeItemIdentification', self::UNBOUNDED, '', 'additional_trade_items_identities'));
        XPath::create($this->buildAssocArr($model, $root.'referencedTradeItem', self::UNBOUNDED, '', 'reference_to_trade_item'));
    }

    private function variableUnit()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'variable_trade_item_information:variableTradeItemInformationModule/variableTradeItemInformation/';

        XPath::create($this->buildAssocArr($model, $root.'isTradeItemAVariableUnit', self::SINGLE, 'T0186', 'is_trade_item_a_variable_unit'));
    }

    private function Warranty()
    {
        $model = ucfirst( __FUNCTION__);
        XPath::create($this->buildAssocArr($model, 'warranty_information:warrantyInformationModule/warrantyInformation', self::UNBOUNDED, '', 'data_for_empty_model'));
    }

    private function warrantyDuration()
    {
        $model = ucfirst( __FUNCTION__);
        $root = 'warranty_information:warrantyInformationModule/warrantyInformation/warrantyConditions/';

        XPath::create($this->buildAssocArr($model, $root.'warrantyDuration', self::SINGLE, 'T2229', 'warranty_duration'));
        XPath::create($this->buildAssocArr($model, $root.'warrantyDuration/@measurementUnitCode', self::SINGLE, 'T3780', 'measurement_unit_code'));
    }



    private function tradeItemHeader()
    {
        $model = ucfirst( __FUNCTION__);

        XPath::create($this->buildAssocArr($model, 'catalogue_item_notification:catalogueItemNotification/creationDateTime', 'single', 'T0151', 'creation_date_time'));
        XPath::create($this->buildAssocArr($model, 'transaction/documentCommand/documentCommandHeader/@type','single', 'T0153', 'type'));
        XPath::create($this->buildAssocArr($model, 'catalogue_item_notification:catalogueItemNotification/catalogueItem/dataRecipient','single', 'T3809', 'data_recipient'));
        XPath::create($this->buildAssocArr($model, 'catalogue_item_notification:catalogueItemNotification/catalogueItem/tradeItem/informationProviderOfTradeItem','single', '', 'information_provider_of_trade_item'));
        XPath::create($this->buildAssocArr($model, 'catalogue_item_notification:catalogueItemNotification/catalogueItem/tradeItem/manufacturerOfTradeItem', 'unbounded', '', 'manufacturer_of_trade_item'));
        XPath::create($this->buildAssocArr($model, 'catalogue_item_notification:catalogueItemNotification/catalogueItem/tradeItem/brandOwner', 'single', '', 'brand_owner'));
        XPath::create($this->buildAssocArr($model, 'catalogue_item_notification:catalogueItemNotification/catalogueItem/tradeItem/partyInRole', 'unbounded', '', 'party_in_role'));
        XPath::create($this->buildAssocArr($model, 'catalogue_item_notification:catalogueItemNotification/catalogueItem/tradeItem/tradeItemSynchronisationDates', 'single', '', 'trade_item_synchronisation_dates'));
        XPath::create($this->buildAssocArr($model, 'catalogue_item_notification:catalogueItemNotification/catalogueItem/tradeItem/tradeItemContactInformation', 'unbounded', '', 'trade_item_contact_information'));
    }

    private function buildAssocArr($modelName, $xpath, $cardinality, $code = '', $fieldName = ''){
      if(!$fieldName){
        $xpathAsArr = explode('/', $xpath);
        $fieldName = snake_case(end($xpathAsArr));
      }
      return [
        'x_path' => $xpath,
        'model_name' => $modelName,
        'code' => $code,
        'field_name' => $fieldName,
        'cardinality' => $cardinality
      ];
    }
}
