<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\Provider;

class ProviderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('TRUNCATE providers CASCADE');
        DB::table('providers')->insert([
            'title' => 'Coop',
            'url' => 'https://www.coop.se/handla-online/',
            'crawler_name' => 'Coop',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('providers')->insert([
            'title' => 'paradiset.com/stockholm',
            'url' => 'https://paradiset.com/stockholm/handla',
            'crawler_name' => 'Paradiset',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('providers')->insert([
            'title' => 'Maxi ICA Stormarknad Nacka',
            'url' => 'https://www.ica.se/handla/maxi-ica-stormarknad-nacka-id_11005/',
            'crawler_name' => 'IcaNacka',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('providers')->insert([
            'title' => 'Willys Stockholm',
            'url' => 'https://www.willys.se/',
            'crawler_name' => 'Willys',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('providers')->insert([
            'title' => 'Maxi ICA Stormarknad Lindhagen',
            'url' => 'https://www.ica.se/handla/maxi-ica-stormarknad-lindhagen-id_13164/',
            'crawler_name' => 'IcaLindhagen',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('providers')->insert([
            'title' => 'ICA Kvantum Täby Centrum',
            'url' => 'https://www.ica.se/handla/ica-kvantum-taby-centrum-id_15076/',
            'crawler_name' => 'IcaCentrum',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
