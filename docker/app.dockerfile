FROM php:7.2.2-fpm


RUN apt-get update

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    incron \
    libpq-dev \
    nano \
    libpng-dev \
    rsyslog \
    vim \
    supervisor \
    zlib1g-dev \
    && docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
    && docker-php-ext-install pdo pdo_pgsql pgsql zip mbstring exif gd

RUN apt-get update && apt-get install -y libmagickwand-dev imagemagick --no-install-recommends \
    && pecl install imagick \
    && docker-php-ext-enable imagick


RUN	echo 'root' >> /etc/incron.allow
COPY docker/incron_validoo/root  /var/spool/incron/root

RUN chown -R www-data:www-data /var/www
RUN usermod -u 1000 www-data

COPY docker/entrypoint.sh /
RUN chmod +x /entrypoint.sh

COPY docker/worker/app-worker.conf /etc/supervisor/conf.d/app-worker.conf
COPY docker/php.ini /usr/local/etc/php/
ENTRYPOINT /entrypoint.sh
