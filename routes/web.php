<?php

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware'=>'auth'] ,function () {
    // Controllers Within The "App\Http\Controllers\Admin" Namespace
    Route::get('/', 'HomeController@index');
    Route::resource('/providers', "ProviderController");
    Route::resource('/products', 'ProductsController');
    Route::resource('/references/package', 'References\PackageController', ['as' => 'admin.references']);
    Route::post('/references/package/uploads', "References\PackageController@upload")->name('admin.references.package.upload');

    Route::resource('/references/measurements', 'References\MeasurementsController');
    Route::post('/references/measurements/uploads', "References\MeasurementsController@upload")->name('admin.references.measurements.upload');

    Route::resource('rise_datum', 'RiseDataController');
    Route::get('rise_datum_for_assigned', 'RiseDataController@for_assigned_validoo_item');


    Route::post('/upload', "RiseDataController@upload")->name('admin.rise_datum.upload');
    Route::get('/selected', "RiseDataController@selected");

    Route::resource('category_levels', 'ProductCategoryLevelsController');
    Route::resource('validoo_item/attributte_levels',
        'ValidooItemAttributeLevelsController')->only('index');
    Route::resource('validoo_item.attributte_levels',
        'ValidooItemAttributeLevelsController')->only('store');

    Route::resource('trade_item/attributte_levels', 'TradeItemAttributeLevelController')->only(['index', 'store']);

    Route::resource('labels', 'LabelsController');
    Route::resource('label-types', 'LabelTypesController');
    Route::resource('additional-climate-scores', 'AdditionalClimateScoresController');
    Route::post('additional-climate-scores/uploads', "AdditionalClimateScoresController@upload")->name('admin.additional_climate_scores.upload');
    Route::group(['prefix' => 'validoo', 'namespace' => 'Validoo'], function(){
        Route::resource('/trade_items', "TradeItemsController");
        Route::post('/uploads', "TradeItemsController@upload")->name('admin.validoo.trade_items.upload');
    });

});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/debug', 'HomeController@debug');
Route::get('/', 'HomeController@index');
Route::get('/search', 'HomeController@search')->where('query','.+');
Route::get('trade_items_search/by_query', 'TradeItemSearchController@byQuery');
Route::get('trade_items_search/list', 'TradeItemSearchController@list');
Route::resource('trade_items', 'TradeItemsController')->only(['index', 'show']);

