<?php

namespace App\Jobs\Validoo;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Validoo\ExternalApi\Subscribe\Services\Create;

class CreateSubscriptionJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    private $gtin = null;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($gtin)
    {
        $this->gtin = $gtin;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        if($this->gtin !== null ) {
            Create::call($this->gtin );
            echo '---handler job ' . __CLASS__;
        }else{
            echo '---GTIN for job' . __CLASS__ .' required';
        }
    }
}
