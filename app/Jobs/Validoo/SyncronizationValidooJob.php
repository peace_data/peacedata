<?php

namespace App\Jobs\Validoo;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Validoo\Handlers\Handler;

class SyncronizationValidooJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    private $file = null;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($file)
    {
        $this->file = $file;
        $this->onQueue('low');

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        echo '---start handler job ' . __CLASS__;

        $filePath = storage_path('app/validoo/'.$this->file);
        Handler::call($this->file, $filePath);
        echo '---finish handler job ' . __CLASS__;

    }
}
