<?php
namespace App\Repositories\Paradiset;

use App\Repositories\Paradiset\Parsers\Product as Parser;
use App\Repositories\Paradiset\Builders\Product as ProductBuilder;

use Goutte;
/**
 * This class grab information from https://www.coop.se
 * And will return assoc array in standart format
 */

class Scrabber
{

  const MAX_PER_PAGE = 16;
  const MAX_PAGE = 43;

  public static function all(){
    return array_map(function($category){
//      if($category['category'] == 'Mejeri & Ost'){
//        return self::grabByCategory($category);
//      }
        return self::grabByCategory($category);
    }, self::retrieveTree());
  }

  private static function retrieveTree(){
    $crawler = Goutte::request('GET', 'https://paradiset.com/stockholm/handla');

    return $crawler->filter('.level0.category-tree__branch a.category-tree__branch')->each(function ($node) {
      return [
        'url' => $node->attr('href'),
        'category' => $node->first()->text()
      ];
    });
  }

  private static function grabByCategory($category){
    $results = [];
    for($i = 0; $i < self::MAX_PAGE; $i++){
      $products = self::productsInPage($category['url'].'?p='.$i);
      $parsedProducts = $products->each(function ($node) use($category){
        try {
          $product = self::retrieveProduct($node->filter('.product-item-link')->first()->attr('href'));
          $product->setCategory($category['category']);
          return $product;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
      });
      $results = array_merge($results, $parsedProducts);

    };

    return $results;
  }

  private static function productsInPage($url){
    $crawler = Goutte::request('GET', $url);
    return $crawler->filter('.item.product.product-item');
  }

  private static function retrieveProduct($url){
    $crawler = Goutte::request('GET', $url);
    $node = $crawler->filter('.column.main')->first();
    $parser = new Parser($node);
    $res = $parser->parse(new ProductBuilder);
    return $res;
  }
}
