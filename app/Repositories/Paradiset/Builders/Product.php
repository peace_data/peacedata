<?php
namespace App\Repositories\Paradiset\Builders;
use App\Repositories\Interfaces\Builders\ProductInterface as ProductInterface;
use App\Repositories\Models\Product as ProductModel;
use App\Repositories\Models\Ingredient as IngredientModel;
use App\Repositories\Models\Nutritional as NutritionalModel;
use App\Repositories\Models\Information as InformationModel;
use App\Repositories\Models\Image as ImageModel;
use Illuminate\Database\Eloquent\Model;

class Product implements ProductInterface
{
  private $product;

  public function createProduct($args){
    $this->product = new ProductModel($args);
  }

  public function addIngredients($ingredients){
    $cleanStr = str_replace('Ingredienser:', '', $ingredients);
    $this->product->setIngredients(array_map(function($title){
        return new IngredientModel($title);
      }, explode(',', $cleanStr))
    );
  }

  public function addInformations($arr){
    $this->product->setInformations(array_map(function($title){
        return new InformationModel($title);
      }, $arr)
    );
  }

  public function addNutritionals($arr){
    $this->product->setNutritionals(array_map(function($nutritional){
        $explodedStr = explode(' ', $nutritional);
        $title = array_shift($explodedStr);
        $value = join($explodedStr);
        return new NutritionalModel($title, $value);
      }, $arr)
    );
  }
  public function addImages($arr){
    $this->product->setImages(array_map(function($image){
        return new ImageModel($image['img'], $image['thumb']);
      }, $arr)
    );
  }

    public function addProvider(Model $model){
        $this->product->setProviderId($model->id);
    }



  public function getProduct(): ProductModel{
    return $this->product;
  }
}
