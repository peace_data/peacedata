<?php

namespace App\Repositories\Paradiset\Parsers;
use App\Repositories\Paradiset\Builders\Product as ProductBuilder;
use App\Repositories\Paradiset\Parsers\Price as PriceParser;
use App\Repositories\Paradiset\Parsers\ExternalKey as ExternalKeyParser;
use App\Repositories\Interfaces\ParserInterface;
use App\Repositories\Interfaces\Builders\ProductInterface;
use App\Models\Provider;

class Product implements ParserInterface
{
  const PROVIDER = 'paradiset.com/stockholm';
  const SELECTORS = [
    'title' => '.page-title .base',
    'weight' => '.unit-block .unit-of-measurement',
    'price' => '.price-wrapper .price',
    'currency' => '.price-wrapper .price .currency-symbol',
    'compare_price' => '.compare-price__wrapper .price',
    'brand' => '.brand-title',
    'ingredients' => '.rs-ingredients__content',
    'nutritionals' => '.rs-nutrition .rs-nutrition__list .rs-nutrition__item',
    'informations' => '.rs-generic .rs-generic__item',
    'images' => '.gallery-placeholder._block-content-loading + script + script',
  ];

  private $provider = null;

  private $node;

  public function __construct($node){
    $this->node = $node;
     $this->provider = Provider::find(2);
  }

  public function parse(ProductInterface $builder, $category=null){
    $productInfo = $this->parseInfo();
    $productInfo['external_key'] = (new ExternalKeyParser($this->node))->key;
    $productInfo['provider'] = self::PROVIDER;

    $builder->createProduct($productInfo);
    if(isset($productInfo['ingredients'])){
      $builder->addIngredients($productInfo['ingredients']);
    }
    if(isset($productInfo['informations'])){
      $builder->addInformations($productInfo['informations']);
      if(in_array('Ekologisk', $productInfo['informations'])){
        $builder->getProduct()->setIsEko();
      }
    }
    if(isset($productInfo['nutritionals'])){
      $builder->addNutritionals($productInfo['nutritionals']);
    }
    if(isset($productInfo['images'])){
      $builder->addImages($productInfo['images']);
    }

    $builder->addProvider($this->provider);


    return $builder->getProduct();
  }

  private function parseInfo(){
    $node = $this->node;
    $res = [
      'title' => trim($node->filter(self::SELECTORS['title'])->first()->text()),
      'weight' => trim($node->filter(self::SELECTORS['weight'])->first()->text()),
      'price' => (new PriceParser($node->filter(self::SELECTORS['price'])->first()->text()))->price,
      'currency' => trim($node->filter(self::SELECTORS['currency'])->first()->text()),
      'compare_price' => (new PriceParser($node->filter(self::SELECTORS['compare_price'])->first()->text()))->price
    ];
    if($node->filter(self::SELECTORS['brand'])->count()){
      $res['brand'] = trim($node->filter(self::SELECTORS['brand'])->first()->text());
    }
    if($node->filter(self::SELECTORS['ingredients'])->count()){
      $res['ingredients'] = trim($node->filter(self::SELECTORS['ingredients'])->text());
    }

    if($node->filter(self::SELECTORS['nutritionals'])->count()){
      $res['nutritionals'] = $node->filter(self::SELECTORS['nutritionals'])->each(function($nutr_node){
        return $nutr_node->text();
      });
    }
    if($node->filter(self::SELECTORS['informations'])->count()){
      $res['informations'] = $node->filter(self::SELECTORS['informations'])->each(function($info_node){
        return $info_node->text();
      });
    }
    if($node->filter(self::SELECTORS['images'])->count()){
      $images = json_decode($node->filter(self::SELECTORS['images'])->first()->text(),true);
      if(isset($images['[data-gallery-role=gallery-placeholder]']) && $images['[data-gallery-role=gallery-placeholder]']['mage/gallery/gallery'] && $images['[data-gallery-role=gallery-placeholder]']['mage/gallery/gallery']['data']){
        $res['images'] = $images['[data-gallery-role=gallery-placeholder]']['mage/gallery/gallery']['data'];
      }
    }
    return $res;
  }
}
