<?php

namespace App\Repositories\Paradiset\Parsers;

class Price
{
  public $price;
  
  public function __construct($price){
    $this->price = explode(chr(0xC2).chr(0xA0),trim($price))[0];
  }
}
