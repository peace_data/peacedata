<?php

namespace App\Repositories\Paradiset\Parsers;

class ExternalKey
{
  public $key;

  public function __construct($node){
    $tmp = explode('/', $node->getUri());
    $this->key = array_pop($tmp);
  }
}
