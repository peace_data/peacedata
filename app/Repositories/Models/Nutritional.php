<?php
namespace App\Repositories\Models;

class Nutritional
{
    public $title;
    public $value;

    public function __construct($title, $value)
    {
        $this->title = $title;
        $this->value = $value;
    }

    public function toArray()
    {
        return [
            'title' => $this->title,
            'value' => $this->value,
        ];
    }

}
