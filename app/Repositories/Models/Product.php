<?php
/**
 * Created by PhpStorm.
 * User: shesser
 * Date: 05.07.18
 * Time: 21:47
 */
namespace App\Repositories\Models;

class Product
{
    private $title; // String
    private $weight; // String
    private $price; // Int
    private $currency; // String
    private $compare_price; // Int
    private $brand; // String
    private $ingredients; // Array of Ingredient classes
    private $nutritionals; // Array of Nutritional classes
    private $informations; // Array of Information classes
    private $images; // Array of Image classes
    private $category; // String
    private $isEko; // Int
    private $external_key; // String
    private $provider_id; // id
    private $compare_unit;
    private $price_unit;


    public function __construct($args)
    {
        $this->title = $args['title'];
        $this->weight = $args['weight'];
        $this->price = $this->initPrice($args['price']);
        $this->price_unit = isset($args['price_unit']) ? $args['price_unit'] : '';
        $this->compare_unit = isset($args['compare_unit']) ? $args['compare_unit'] : "";

        $this->currency = $args['currency'];

        $this->compare_price = isset($args['compare_price']) ?  $this->initPrice($args['compare_price']) : '';
        $this->brand = $args['brand'];
        $this->external_key = $this->setExternalKeyAttribute($args['external_key']);
        //      move to provider_id
        //      $this->provider = $args['provider'];
        $this->isEko = isset($args['isEko']) ? $args['isEko'] : false;
        $this->category =  isset($args['category']) ? $args['category'] : '';
    }

    private function initPrice($value)
    {
        if(is_int($value)) { return $value; }

        return (int)(floatval($value) * 100);
    }

    public function setProviderId($provider_id)
    {
        $this->provider_id = $provider_id;
    }

    public function setIngredients($ingredients){
        if(!is_array($ingredients)) {
            get_class($ingredients);
        }

      $this->ingredients = is_array($ingredients) ? $ingredients : [];
    }

    public function setNutritionals($nutritionals){
      $this->nutritionals = is_array($nutritionals) ? $nutritionals : [];
    }

    public function setInformations($informations){
      $this->informations = is_array($informations) ? $informations : [];
    }

    public function setImages($images){
      $this->images = $images;
    }

    public function is_unique(){
        // TODO: to need implement
        return true;
    }

    public function setCategory($category){
      $this->category = $category;
    }

    public function setIsEko(){
      $this->isEko = true;
    }

    public function toArray()
    {
       return [
           'title' => $this->title,
           'price' => $this->price, // Int
           'weight' => $this->weight, // String
           'currency' => $this->currency, // String
           'compare_price' => $this->compare_price, // Int
           'brand' => $this->brand,// String
           'ingredients' => $this->ingredientsToArray(), // Array of Ingredient classes
           'nutritionals' => $this->nutritionalsToArray(), // Array of Nutritional classes
           'informations'=> json_encode(''), // Array of Information classes
           'images'=> $this->imagesToArray(), // Array of Image classes
           'category' => $this->category, // String
           'isEko' => $this->isEko, // Int
           'external_key' => $this->external_key, // String
           'provider_id' => $this->provider_id,
           'price_unit' => $this->price_unit,
           'compare_unit' => $this->compare_unit
       ];
    }


    private function ingredientsToArray()
    {
        if(!is_array($this->ingredients)) {
            return [];
        }

        return array_map(function($ingredient){
            return $ingredient->toArray();
        }, $this->ingredients);
    }

    private function informationsToArray()
    {
        return '';
//        return array_map(function($information){
//            return $information->toArray();
//        }, $this->informations);
    }

    private function nutritionalsToArray()
    {
        if(!is_array($this->nutritionals)) {
            return [];
        }
        return array_map(function($nutritional){
            return $nutritional->toArray();
        }, $this->nutritionals);
    }


    private function imagesToArray()
    {
        return array_map(function($image){
            return $image->toArray();
        }, $this->images);
    }

    public function valid(){
      return true;
    }

    public function setExternalKeyAttribute($value)
    {
       $length = strlen($value);
       $prefix = '';

        for($i = 0; $i < 14 - $length; $i++){
            $prefix .='0';
        }

        return $prefix.$value;
    }

    // Десь тут буде підніматися моделька Product// Та можливо валідіруватися
}
