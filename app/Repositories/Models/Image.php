<?php
namespace App\Repositories\Models;

class Image
{
  public $src;
  public $thumb;

  public function __construct($src, $thumb=null)
  {
    $this->src = $src;
    $this->thumb = $thumb;
  }


    /**
     * @return array
     */
  public function toArray()
  {
      return [
          'src' => $this->src,
          'thumb' => $this->thumb,
      ];
  }
}
