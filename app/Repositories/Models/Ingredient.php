<?php
namespace App\Repositories\Models;

class Ingredient
{
    public $title;

    public function __construct($title)
    {
        $this->title = $title;
    }

    public function toArray()
    {
        return [
            'title' => $this->title
        ];
    }
}
