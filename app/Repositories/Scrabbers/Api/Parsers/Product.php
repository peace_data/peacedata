<?php

namespace App\Repositories\Scrabbers\Api\Parsers;

use App\Repositories\Scrabbers\Api\Builders\Product as ProductBuilder;
use App\Repositories\Interfaces\ParserInterface;
use App\Repositories\Interfaces\Builders\ProductInterface;
use App\Models\Provider;

class Product implements ParserInterface
{
  private $provider;
  private $productData;
  public function __construct($productData, $provider){
    $this->productData = $productData;
    $this->provider = $provider;
  }

  public function parse(ProductInterface $builder, $category=null){
    $image = $this->productData['image'];
    $productInfo = [
      'title' => $this->productData['name'],
      'weight' => $this->productData['displayVolume'],
      'price' => $this->productData['priceValue'],
      'currency' => $this->productData['priceUnit'],
      'compare_price' => $this->productData['comparePrice'],
      'brand' => $this->findBrand($this->productData['manufacturer']),
      'compare_unit' => $this->productData['comparePriceUnit'],
      'price_unit' => $this->productData['priceUnit']
    ];
    if(!empty($image)){
      $productInfo['external_key'] = (new ExternalKeyParser($image['url']))->key;
    }
    $builder->createProduct($productInfo);

    if(empty($image)){
      $image = [];
    }else{
      $image = [$image];
    }
    $builder->addImages($image);

    $builder->addInformations('');
    $builder->addNutritionals([]);
    $builder->addIngredients([]);


    $builder->addProvider($this->provider);

    return $builder->getProduct();
  }

  private function findBrand($brand){
    if(empty($brand)){
      return 'Not seted';
    }
    return $brand;
  }
}
