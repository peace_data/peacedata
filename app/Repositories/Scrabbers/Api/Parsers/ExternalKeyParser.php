<?php

namespace App\Repositories\Scrabbers\Api\Parsers;

class ExternalKeyParser
{
  public $key;
  public function __construct($imgUrl){
    $this->key = $this->parse($imgUrl);
  }

  private function parse($imgUrl){
    $explodedUrl = explode('/', $imgUrl);
    $res = explode('.', end($explodedUrl));
    $res = array_shift($res);
    if(strlen($res)> 15){
      $res = explode('_', $res);
      $res = array_shift($res);
    }
    return $res;
  }
}
