<?php
namespace App\Repositories\Scrabbers\Api;

use App\Repositories\Scrabbers\Api\Parsers\Product as Parser;
use App\Repositories\Scrabbers\Api\Builders\Product as ProductBuilder;

use Goutte\Client;
use Log;
use App\Models\Provider;

class Scrabber
{
    private $provider;
    protected $scrabberName;
    private $categories = [];
    const CATEGORY_PATH = '/leftMenu/categorytree';
    const PRODUCT_BY_CATEGORY_PATH = '/c/:category?page=:page&size=200';

    public function __construct($scrabberName)
    {
      $this->scrabberName = $scrabberName;
      $this->provider = $this->initProvider();
      $this->categories = $this->retrieveCategories();
    }

    public static function all()
    {
        Log::debug('--------------start task---------------------');

        $scrabber = new self(static::SCRABER_NAME);
        return array_map(function($category) use($scrabber) {
          // if($category['url'] == "Tidningar-och-bocker"){
              return $scrabber->grabByCategory($category);
          // }
        }, $scrabber->categories['children']);
    }

    private function grabByCategory($category)
    {
        $products = [];

        $page = 0;
        do{
          $url = $this->buildCategoryUrl($category['url'], $page);
          $newProducts = [];
          if($this->retrieveData($url)){
            echo $url;
            $retrivedProducts = $this->retrieveData($url);
            $retrivedProductSize = 0;
            if(isset($retrivedProducts['results']) && $retrivedProducts['results']){
              $retrivedProductSize = count($retrivedProducts['results']);
              $newProducts = $this->parseProducts($retrivedProducts, $category);
              $products = array_merge($products, $newProducts);
            }
          }
          $page++;

        }while ($retrivedProductSize == 200);
        return $products;
    }

    private function buildCategoryUrl($category, $page){
      $url = $this->baseUrl().self::PRODUCT_BY_CATEGORY_PATH;
      $url = str_replace(':category', $category, $url);
      $url = str_replace(':page', $page, $url);
      return $url;
    }
    /**
     * @return string
     */
    private function initProvider()
    {
      return Provider::where('crawler_name', $this->scrabberName)->first();
    }


    private function retrieveData($url){
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

      return json_decode(curl_exec($ch), true);
    }

    /**
     * Get all categories from nav bar
     * @param $client
     * @return mixed
     */
    private function retrieveCategories()
    {
      return $this->retrieveData($this->baseUrl().self::CATEGORY_PATH);
    }

    private function parseProducts($products, $category){
      return array_map(function($product) use ($category){
        $parser = new Parser($product, $this->provider);
        $product = $parser->parse(new ProductBuilder);
        $product->setCategory($category['title']);
        return $product;
      }, $products['results']);
    }

    protected function baseUrl(){
      $url = parse_url($this->provider->url);
      return $url['scheme'].'://'.$url['host'];
    }


    protected function getClient()
    {
        $client = new Client();
        return $client;
    }

}
