<?php
namespace App\Repositories\Scrabbers\Api\Builders;
use App\Repositories\Interfaces\Builders\ProductInterface as ProductInterface;
use App\Repositories\Models\Product as ProductModel;
use App\Repositories\Models\Ingredient as IngredientModel;
use App\Repositories\Models\Nutritional as NutritionalModel;
use App\Repositories\Models\Information as InformationModel;
use App\Repositories\Models\Image as ImageModel;
use Illuminate\Database\Eloquent\Model;

class Product implements ProductInterface
{
  private $product;

  public function createProduct($args){
    $this->product = new ProductModel($args);
  }

  public function addIngredients($ingredients){
    $this->product->setIngredients([]);
  }

  public function addInformations($arr){
    $this->product->setInformations([]);
  }

  public function addNutritionals($arr){
    $this->product->setNutritionals([]);
  }
  public function addImages($arr = []){
    $this->product->setImages(array_map(function($image){
        return new ImageModel($image['url'], $image['url']);
      }, $arr)
    );
  }

  public function addProvider(Model $model){
      $this->product->setProviderId($model->id);
  }

  public function getProduct(): ProductModel{
    return $this->product;
  }
}
