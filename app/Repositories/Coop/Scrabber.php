<?php
/**
 * Created by PhpStorm.
 * User: shesser
 * Date: 08.07.18
 * Time: 16:18
 */

namespace App\Repositories\Coop;

use App\Repositories\Coop\Parsers\Product as Parser;
use App\Repositories\Coop\Builders\Product as ProductBuilder;
use Symfony\Component\DomCrawler\Crawler;
use Goutte\Client;
use Log;
use App\Models\Provider;

class Scrabber
{
    private $url;
    private $tree_selectors = [];
    const CATEGORY_NODE = '.ItemTeaser.js-product';
    const PAGINATE_MAX = 10;

    public static function all()
    {
        Log::debug('--------------start task---------------------');
        $scrabber = new self;
        $client = $scrabber->get_client();

        return array_map(function($category) use($scrabber) {
            //TODO for debaging/ only one category
//            if($category['category'] == "Mejeri"){
//                return $scrabber->grabByCategory($category);
//            }

            return $scrabber->grabByCategory($category);

        }, $scrabber->retrieveTree($client) );
    }

    /**
     * Scrabber constructor.
     */
    public function __construct()
    {
        $this->tree_selectors = $this->init_tree_selectors();
        $this->url = $this->init_url();
    }

    /**
     * @return string
     */
    private function init_url()
    {
        $class_name = explode('\\', __NAMESPACE__);
        $provider = Provider::where('crawler_name', end($class_name))->first();

        return $provider->url;
    }

    /**
     * @param $category
     * @return array
     */
    private function grabByCategory($category)
    {
        $parse = parse_url($this->url);
        $current_url = 'https://'.$parse['host'].$category['url'];
        $pages = $this->retrivePaginations($current_url);

        $products = [];
        foreach($pages as $page){
            $products = array_merge($products, $this->grabProducts($category, $page));
        }
        return $products;
    }

    /**
     * @param $current_url
     * @return array
     */
    private function retrivePaginations($current_url)
    {
        $pages = [];
        for($i=1; $i <=self::PAGINATE_MAX; $i++ ) {
            $page = $current_url.'/?page='.$i;
            array_push($pages,$page);
        }

        return $pages;
    }

    /**
     * @param $category
     * @param $current_url
     * @return array
     */
    private function grabProducts($category, $current_url)
    {
        $client = $this->get_client();
        $crawler = $client->request('GET', $current_url);

        $products = $crawler->filter(self::CATEGORY_NODE)->each(function (Crawler $node, $i) use ($category) {
            $parser = new Parser($node);
            return $parser->parse(new ProductBuilder, $category['category']);
        });
        return $products;
    }

    /**
     * Get all categories from nav bar
     * @param $client
     * @return mixed
     */
    private function retrieveTree($client)
    {
        $crawler = $client->request('GET', $this->url);

        $children = $crawler->filter($this->tree_selectors['first_level'])->each(function (Crawler $node, $i) {
            $sub_node = $node->filter($this->tree_selectors['second_level'])->first()->children()->first();
            return
            [
                'url' => $sub_node->attr('href'),
                'category' =>$sub_node->children()->first()->text()
            ];
        });

        Log::debug('Searching urls with products from navbar');
        return $children;
    }

    /**
     * @return Client
     */
    protected function get_client(){
        $client = new Client();
        $client->setHeader('user-agent', "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36");

        return $client;
    }

    /**
     * TOTO maybe to need retrieve it from setting of provider
     * @return array
     */
    private function init_tree_selectors()
    {
        return [
            'first_level' => '.SidebarNav-item.js-sidebarNavItem',
            'second_level' => '.SidebarNav-heading.js-sectionHeader',
            'pagination' => 'a.Pagination-page'
        ] ;
    }

}
