<?php
namespace App\Repositories\Coop\Builders;

use App\Repositories\Interfaces\Builders\ProductInterface as ProductInterface;
use App\Repositories\Models\Product as ProductModel;
use App\Repositories\Models\Ingredient as IngredientModel;
use App\Repositories\Models\Nutritional as NutritionalModel;
use App\Repositories\Models\Information as InformationModel;
use Illuminate\Database\Eloquent\Model;
use App\Repositories\Models\Image as ImageModel;

class Product implements ProductInterface
{
    private $product;

    /**
     * @param $args
     */
    public function createProduct($args)
    {
        $this->product = new ProductModel($args);
    }

    /**
     * @param Model $model
     */
    public function addProvider(Model $model)
    {
        $this->product->setProviderId($model->id);
    }

    /**
     * @param $ingredients
     */
    public function addIngredients($ingredients)
    {
        $this->product->setIngredients(array_map(function($item){
                return new IngredientModel($item);
            }, $ingredients)
        );
    }

    /**
     * @param $arr
     */
    public function addInformations($arr)
    {
//        $this->product->setInformations(array_map(function($title){
//                return new InformationModel($title);
//            }, $arr)
//        );
    }

    /**
     * @param $arr
     */
    public function addNutritionals($arr)
    {
        $this->product->setNutritionals(array_map(function($item){
                return new NutritionalModel($item['title'], $item['value']);
            }, $arr)
        );
    }

    /**
     * @param $arr
     */
    public function addImages($arr)
    {
        $this->product->setImages(array_map(function($image){
                return new ImageModel($image['img']);
            }, $arr)
        );
    }

    /**
     * @return ProductModel
     */
    public function getProduct(): ProductModel
    {
        return $this->product;
    }
}
