<?php
/**
 * Created by PhpStorm.
 * User: shesser
 * Date: 08.07.18
 * Time: 16:27
 */
namespace App\Repositories\Coop\Parsers;
use App\Repositories\Interfaces\ParserInterface;
use App\Repositories\Interfaces\Builders\ProductInterface;
use App\Models\Provider;

class Product implements ParserInterface
{
    private $provider = null;
    private $node = null;

    /**
     * Product constructor.
     * @param $node
     */
    public function __construct($node)
    {
        $this->node = json_decode($node->children()->first()->text());
        $this->provider = $this->initProvider();
    }

    /**
     * @return mixed
     */
    private function initProvider()
    {
        $class_name = explode('\\', get_called_class());
        return Provider::where('crawler_name', $class_name[2])->first();
    }

    /**
     * @param ProductInterface $builder
     * @param null $category
     * @return \App\Repositories\Models\Product
     */
    public function parse(ProductInterface $builder, $category=null)
    {
      $main_info = [
          'title' => html_entity_decode($this->node->name),
          'weight' => $this->parseWeight(),
          'price' => $this->node->price,
          'price_unit'=> $this->node->priceUnit,
          'currency'=> $this->parseComparePriceBlock()['currency'],
          'brand'=> html_entity_decode($this->node->manufacturer),
          'compare_price' => $this->parseComparePriceBlock()['compare_price'],
          'compare_unit' => $this->compareUnit(),
          'external_key' => $this->node->ean,
          'isEko' => $this->getIsEko(),
      ];

      if($category) {
          $main_info['category'] = $category;
      }

      $builder->createProduct($main_info);
      $builder->addProvider($this->provider);
      $builder->addImages($this->getImages());
      $builder->addIngredients($this->getIngradients());
      $builder->addNutritionals($this->getNutritions());
      $builder->addInformations($this->getInformations());

      return $builder->getProduct();
    }

    /**
     * @return string
     */
    private function compareUnit()
    {
        if(!isset($this->node->comparativePrice)) {
            return '';
        }

        return $this->node->comparativeUnit;
    }

    /**
     * @return mixed
     */
    private function parseWeight()
    {
        $size =  $this->node->details->size;

        return $size->size;
    }

    /**
     * @return array
     */
    private function parseComparePriceBlock()
    {
       if(!isset($this->node->comparativePrice)) {
           return ['compare_price' => 0.00, 'currency' => ''];
       }

        $node = $this->node->comparativePrice;

        $items = explode(' ', $node);
        return [
            'compare_price' => str_replace(',', '.', $items[0]),
            'currency' => $items[1]
        ];
    }

    /**
     * @return array
     */
    private function getIngradients()
    {
        $items = array_filter( explode( "\r\n", $this->node->details->description));
        $arr = [];

        foreach($items as $item) {
            $item = strip_tags(trim($item));
            if(!empty($item)) {
                array_push($arr, $item);
            }
        }

        return $arr;
    }

    /**
     * @return array
     */
    private function getImages()
    {
        if(!isset($this->node->image)) {
           return [['img' => '']];
        }

        $node = $this->node->image;

        if(!is_array($node)) {
            $node = [$node];
        }

        return array_map(function($image){
            return ['img' => $image->url];
        },$node);
    }

    /**
     * @return array
     */
    private function getNutritions()
    {
        return array_map(function($item){
            return $this->parseNutrition($item);
        }, $this->node->details->nutritionFacts);
    }

    /**
     * @param $item
     * @return array
     */
    private function parseNutrition($item)
    {
        $words = explode(' ', $item);
        $size = count($words);

        $title = '';
        for($i=0; $i < $size - 2; $i++) { $title .= $words[$i].' '; }

        return [
          'title' =>trim($title),
          'value' =>$words[$size - 2] . " " . $words[$size - 1]
        ];
    }

    /**
     * TODO not implement
     * @return array
     */
    private function getInformations()
    {
        return [];
    }

    /**
     * @return mixed
     */
    private function getIsEko()
    {
        return $this->node->details->isEko;
    }
}
