<?php
/**
 * Created by PhpStorm.
 * User: shesser
 * Date: 08.07.18
 * Time: 16:17
 */
namespace App\Repositories\Coop;

use App\Repositories\Interfaces\ProductInterface as ProductInterface;
use App\Repositories\Coop\Scrabber;
use App\Models\Product;
use App\Models\Provider;

class CoopRepository implements ProductInterface
{
    public static function all()
    {
        return Scrabber::all();
    }

    public static function find($id)
    {
        return 'find';
    }

    /**
     * For development
     */
    public static function destroyAll()
    {
        $items = Product::byProviderId(1);

        $count = $items->count();
        $items->delete();
        return $count;
    }
}