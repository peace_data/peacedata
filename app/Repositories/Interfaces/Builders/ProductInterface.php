<?php
namespace App\Repositories\Interfaces\Builders;
use App\Repositories\Models\Product;
use Illuminate\Database\Eloquent\Model;

interface ProductInterface
{
    public function createProduct($args);

    public function addProvider(Model $model);

    public function addIngredients($ingredients);

    public function addInformations($arr);

    public function addNutritionals($arr);

    public function addImages($arr);

    public function getProduct(): Product;
}
