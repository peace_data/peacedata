<?php
namespace App\Repositories\Interfaces;

interface ProductInterface {

    public static function all();

    public static function find($id);

    // public function delete($id);
}
