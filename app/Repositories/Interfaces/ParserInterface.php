<?php
namespace App\Repositories\Interfaces;

use App\Repositories\Interfaces\Builders\ProductInterface;
interface ParserInterface {

  public function parse(ProductInterface $builder, $category=null);

}
