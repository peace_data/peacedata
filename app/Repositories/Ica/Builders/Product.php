<?php
/**
 * Created by PhpStorm.
 * User: shesser
 * Date: 29.08.18
 * Time: 14:08
 */
namespace App\Repositories\Ica\Builders;

use App\Repositories\Interfaces\Builders\ProductInterface as ProductInterface;
use App\Repositories\Models\Product as ProductModel;
use App\Repositories\Models\Ingredient as IngredientModel;
use App\Repositories\Models\Nutritional as NutritionalModel;
use App\Repositories\Models\Information as InformationModel;
use Illuminate\Database\Eloquent\Model;
use App\Repositories\Models\Image as ImageModel;


class Product implements ProductInterface
{
    protected $product;

    public function createProduct($args){
        $this->product = new ProductModel($args);
    }

    public function addProvider(Model $model){
        $this->product->setProviderId($model->id);
    }

    public function addIngredients($ingredients){
        $this->product->setIngredients(array_map(function($item){
                return new IngredientModel($item);
            }, $ingredients)
        );
    }

    public function addInformations($arr){
//        $this->product->setInformations(array_map(function($title){
//                return new InformationModel($title);
//            }, $arr)
//        );
    }

    public function addNutritionals($arr){
        $this->product->setNutritionals(array_map(function($item){
                return new NutritionalModel($item['title'], $item['value']);
            }, $arr)
        );
    }

    public function addImages($arr){
        $this->product->setImages(array_map(function($image){
                return new ImageModel($image['img']);
            }, $arr)
        );
    }

    public function getProduct(): ProductModel{
        return $this->product;
    }
}