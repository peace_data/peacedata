<?php
/**
 * Created by PhpStorm.
 * User: shesser
 * Date: 29.08.18
 * Time: 14:07
 */
namespace App\Repositories\Ica\Parsers;

use App\Repositories\Interfaces\Builders\ProductInterface;
use App\Models\Provider;

class Product
{

    protected $provider = null;
    protected $node = null;
    protected $main_info = null;

    const CART_ITEM = 0;
    const TITLE = 1;
    const ADDITIONAL = 2;
    const SKU_ID = 3;

    /**
     * Product constructor.
     * @param $node
     */
    public function __construct($node)
    {
        $this->node =  $node;
        $this->main_info = $this->extractMainInfo($node);
        $this->provider = $this->initProvider();
    }

    /**
     * @return mixed
     */
    private function initProvider()
    {
        $class_name = explode('\\', get_called_class());
        return Provider::where('crawler_name', $class_name[2])->first();
    }

    /**
     * @param $node
     * @return array
     */
    private function extractMainInfo($node)
    {
        $info = $node->extract([
            'data-cartitem',
            'data-title',
            'data-addtional-info',
            'data-product-sku-id'
        ]);

        return [
            json_decode($info[0][self::CART_ITEM]),
            $info[0][self::TITLE],
            json_decode($info[0][self::ADDITIONAL]),
            $info[0][self::SKU_ID],
        ];
    }

    /**
     * @param ProductInterface $builder
     * @param null $category
     * @return \App\Repositories\Models\Product
     */
    public function parse(ProductInterface $builder, $category=null)
    {

        try {
            $main_info = [
                'title' => $this->main_info[self::TITLE],
                'weight' => $this->main_info[self::CART_ITEM]->meanWeight,
                'price' => $this->main_info[self::ADDITIONAL]->price,
                'price_unit'=> $this->main_info[self::CART_ITEM]->unit,
                'currency'=> $this->currency(),
                'brand'=> $this->main_info[self::ADDITIONAL]->brand,
                'category' => $this->main_info[self::ADDITIONAL]->category,
                'compare_price' => $this->compare_price(),
                'compare_unit' => $this->main_info[self::CART_ITEM]->unitOfMeasure,
                'external_key' => $this->main_info[self::SKU_ID],
                'isEko' => 'true',
            ];
        }catch(\ErrorException $e) {
           //TODO to need solving this exception
            echo $e;
            var_dump($this->main_info); exit;
        }

        $builder->createProduct($main_info);
        $builder->addProvider($this->provider);
        /**
         * TODO to need implement those sections
         */
        $builder->addImages([]);
        $builder->addIngredients([]);
        $builder->addNutritionals([]);
        $builder->addInformations([]);

        return $builder->getProduct();
    }

    /**
     * @return float
     */
    protected function compare_price()
    {
        $price = $this->node->filter('.comparisonPrice');
        $arr = explode(' ', trim(preg_replace("/\s|&nbsp;/",' ', $price->text())));

        if(count($arr) < 2) {
            return 0.00;
        }

        return (float) str_replace(',', '.', $arr[1]);
    }

    /**
     * @return mixed|null
     */
    protected function currency()
    {
        $price = $this->node->filter('.rainview');

        $arr = [];
        if($price->count()){
            $arr = explode(' ', trim($price->text()));
        }else{
            $price = $this->node->filter('.title');

            $arr = explode(' ', trim(preg_replace("/\s|&nbsp;/",' ', $price->text())));

            if($arr == null){
                return null;
            }
        }

        return end($arr);
    }

}
