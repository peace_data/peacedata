<?php


namespace App\Repositories\Ica;

use App\Repositories\Interfaces\ProductInterface;


class IcaRepository implements ProductInterface
{

    public static function all()
    {
        $class = get_called_class();
        $namespace = '\\'.$class::getNamespace().'\\';
        $scrabber = $namespace.'Scrabber';

        return $scrabber::all();
    }

    public static function find($id){}
}