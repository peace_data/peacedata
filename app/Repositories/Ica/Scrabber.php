<?php
namespace App\Repositories\Ica;
/**
 * Created by PhpStorm.
 * User: shesser
 * Date: 29.08.18
 * Time: 14:07
 */
use App\Repositories\Ica\Parsers\Product as Parser;
use App\Repositories\Ica\Builders\Product as ProductBuilder;
use Symfony\Component\DomCrawler\Crawler;
use Goutte\Client;
use Log;
use App\Models\Provider;


class Scrabber
{
    protected $url;
    protected $tree_selectors = [];
    const CATEGORY_NODE = '[class="product"]';
    const PAGINATE_MAX = 10;
    private $headers = null;

    /**
     * @return array
     */
    public static function all()
    {
        Log::debug('--------------start task---------------------');

        $class = get_called_class();
        $scrabber = new $class;

        return array_map(function($category) use($scrabber) {
            return $scrabber->grabByCategory($category);

        }, $scrabber->retrieveTree() );
    }

    /**
     * @param $category
     * @return array
     */
    protected function grabByCategory($category)
    {
        $parse = parse_url($this->url);
        $current_url = $parse['scheme'].'://'.$parse['host'].$category['url'];
//        $pages = $this->retrivePaginations($current_url);
//
        $products = [];
        $products = array_merge($products, $this->grabProducts($category, $current_url));

//        foreach($pages as $page){
//            $products = array_merge($products, $this->grabProducts($category, $page));
//        }
        return $products;
    }

    /**
     * @param $category
     * @param $current_url
     * @return array
     */
    protected function grabProducts($category, $current_url)
    {
        $client = $this->getClient($this->headers);

        $crawler = $client->request('GET', $this->prepareUrlForCurrentCategory($current_url));

        $products = $crawler->filter(self::CATEGORY_NODE)->each(function (Crawler $node, $i) use ($category) {
            $items = $node->filter('article');

            $parser_name = str_replace('Scrabber', 'Parsers\\', get_called_class());
            $parser_name .= 'Product';

            $parser = new $parser_name ($items);

            if(!$parser) {return null;}

            return $parser->parse(new ProductBuilder, $category['category']);
        });
        return $products;
    }

    /**
     * @param $url
     * @return mixed
     */
    protected function prepareUrlForCurrentCategory($url)
    {
        return str_replace('#s=', 'cujFlow/', $url);
    }

    /**
     * Scrabber constructor.
     */
    public function __construct()
    {
        $this->tree_selectors = $this->initTreeSelectors();
        $this->url = $this->initUrl($this->init_scrabber_name());
    }

    /**
     * @return mixed
     */
    private function init_scrabber_name()
    {
        $arr = explode('\\', get_called_class());
        return $arr[2];
    }
    /**
     * @return string
     */
    protected function initUrl($scraber_name)
    {
        $provider = Provider::where('crawler_name', $scraber_name)->first();
        return $provider->url;
    }

    /**
     * @return array
     */
    protected function initTreeSelectors()
    {
        // Мабуть будемо брати з бази
        return [
            'first_level' => '.category-tree__node',
            'second_level' => '.category-tree__link',
//            'pagination' => 'a.Pagination-page'
        ] ;
    }

    /**
     * Get all categories from nav bar
     * @return mixed
     */
    protected function retrieveTree()
    {
        $client = $this->getClient();

        $crawler = $client->request('GET', $this->url);

        $this->headers = $this->getHeaderCookies($client);

        $children = $crawler->filter($this->tree_selectors['first_level'])->each(function (Crawler $node, $i) {

            $sub_node = $node->filter($this->tree_selectors['second_level']);

            return
                [
                    'url' => $sub_node->attr('href'),
                    'category' =>$sub_node->children()->first()->text()
                ];
        });

        Log::debug('Searching urls with products from navbar');
        return $children;
    }

    /**
     * @param $client
     * @return array
     */
    protected function getHeaderCookies($client)
    {
        $cookies = $client->getResponse()->getHeaders();
        $cookies = $cookies['Set-Cookie'];
        $str = '';
        foreach($cookies as $cookie) {
            $str  .= $cookie.';';
        }

        return [
            'Cookie' => $str
        ];
    }

    /**
     * @param array $headers
     * @return Client
     */
    protected function getClient($headers = [])
    {
        $client = new Client();
        $client->setHeader('user-agent', "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36");

        foreach($headers as $key=>$value) {
            $client->setHeader($key, $value);
        }

        return $client;
    }

}