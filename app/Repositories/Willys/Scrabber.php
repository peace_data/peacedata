<?php
namespace App\Repositories\Willys;

use App\Repositories\Scrabbers\Api\Scrabber as ApiScrabber;

class Scrabber extends ApiScrabber
{
    const SCRABER_NAME = 'Willys';
}
