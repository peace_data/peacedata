<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use LaravelFillableRelations\Eloquent\Concerns\HasFillableRelations;

class Nutritional extends Model
{
    use HasFillableRelations;
    protected $fillable_relations = ['product'];


    protected $fillable = [
        'product_id',
        'title',
        'value'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }
}
