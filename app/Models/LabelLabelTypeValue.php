<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LabelLabelTypeValue extends Model
{
    protected $fillable = ['label_id', 'label_type_id', 'value'];

    public function labelType(){
      return $this->belongsTo(LabelType::class);
    }

    public function labelTypeTitle(){
      return $this->labelType->title;
    }
}
