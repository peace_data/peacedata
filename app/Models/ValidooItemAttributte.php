<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ValidooItemAttributte extends Model
{
  protected $fillable = ['attribute_name', 'value'];

}
