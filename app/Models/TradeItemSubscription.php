<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\TradeItem;

class TradeItemSubscription extends Model
{
    protected $fillable = ['gtin', 'status_code', 'subscribe_id', 'message', 'type'];

    public function scopeByType($query, $type){
      return $query->where('type', $type);
    }
    public function scopeByGtin($query, $gtin){
      return $query->where('gtin', $gtin);
    }

    public function tradeItem(){
      return $this->belongsTo(TradeItem::class, 'gtin', 'gtin');
    }
}
