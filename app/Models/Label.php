<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\LabelLabelTypeValue;
use LaravelFillableRelations\Eloquent\Concerns\HasFillableRelations;

class Label extends Model
{

    use HasFillableRelations;
    protected $fillable_relations = ['values'];

    protected $fillable = ['title'];

    public function values(){
        return $this->hasMany(LabelLabelTypeValue::class);
    }

    public function valueByType($typeId){
      $value = $this->values()->where('label_type_id', $typeId)->first();
      return empty($value) ? 0 : $value->value;
    }
    
}
