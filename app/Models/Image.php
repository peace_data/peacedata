<?php

namespace App\Models;

use Sleimanx2\Plastic\Searchable;
use Illuminate\Database\Eloquent\Model;
use LaravelFillableRelations\Eloquent\Concerns\HasFillableRelations;


class Image extends Model
{
//    use Searchable;
    use HasFillableRelations;
    protected $fillable_relations = ['product'];


    protected $fillable = [
        'product_id',
        'src',
        'thumb'
    ];



    /**
     * Binding property for elasticsearch type
     * @return array
     */
//    public function buildDocument()
//    {
//        return [
//            'id' => $this->id,
//            'product_id'=> $this->product_id,
//            'src' => $this->src,
//            'thumb' => $this->thumb
//        ];
//    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }
}
