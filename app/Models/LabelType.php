<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LabelType extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'label_types';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title'];

    public function scopeGroupedByTradeItem($query, $tradeItemId){
      return $query
        ->selectRaw('sum(label_label_type_values.value) as sum, label_types.title as title')
        ->join('label_label_type_values', 'label_label_type_values.label_type_id', 'label_types.id')
        ->join('labels', 'label_label_type_values.label_id', 'labels.id')
        ->join('trade_item_label', 'trade_item_label.label_id', 'labels.id')
        ->join('trade_items', 'trade_items.id', 'trade_item_label.trade_item_id')
        ->groupBy('label_types.title')
        ->where('trade_items.id', $tradeItemId)
        ->orderBy('label_types.title');
    }
}
