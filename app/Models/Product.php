<?php

namespace App\Models;
use Illuminate\Support\Facades\DB;
use Sleimanx2\Plastic\Searchable;
use Illuminate\Database\Eloquent\Model;
use LaravelFillableRelations\Eloquent\Concerns\HasFillableRelations;
use App\Models\Validoo\TradeItemIdentity;
use App\Sorters\Sort;
use Illuminate\Database\Eloquent\Builder;

class Product extends Model
{
    // use Searchable;


    use HasFillableRelations;
    //for nested
    protected $fillable_relations = ['images', 'nutritionals', 'ingredients'];

    protected $fillable = [
        'title',
        'price', // Int
        'price_unit',
        'weight', // String
        'currency', // String
        'compare_price', // Int
        'compare_unit',
        'brand', // String
        'informations', // Array of Information classes
        'brand', // String
        'category', // String
        'isEko', // Int
        'external_key', // String
        'provider_id', // String
        'provider_version_id' //int
    ];


    /**
     * @param $query
     * @return mixed
     */
    public function scopeActual($query)
    {
        $ids = ProviderVersion::allProviders()
                ->pluck('id')->toArray();

        return $query->whereIn('provider_version_id', $ids);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function providerVersion()
    {
        return $this->belongsTo(ProviderVersion::class, 'provider_version_id', 'id');
    }


    public function scopeFindOrCreateByExternalKey($query, $externalKey)
    {
      $obj = $query->where('external_key', $externalKey)->first();
      return $obj ?: new static;
    }

    /**
     * 14 is standard of GTIN,
     * it is used on validoo trade item
     * @param $value
     */
    public function setExternalKeyAttribute($value)
    {
       $length = strlen($value);
       $prefix = '';

        for($i = 0; $i < 14 - $length; $i++){
            $prefix .='0';
        }

        $this->attributes['external_key'] =  $prefix.$value;
    }

    /**
     * Binding property for elasticsearch type
     * @return array
     */
    public function buildDocument()
    {
        return [
            'id' => $this->id,
            'provider'=> $this->provider,
            'title' => $this->title,
            'external_key' => $this->external_key,
            'category' => $this->category,
            'brand' => $this->brand,
            'images' => $this->images,
//            'nutritionals'=> $this->nutritionals,
//            'ingredients' => $this->ingredients,
        ];
    }

      public function scopeSort($query, $request){
        return (new Sort($query, $request))->call();
      }


    /**
     * @param $query
     * @param $category
     * @return mixed
     */
    public function scopeByCategory($query, $category)
    {
        return $query->where('category', $category);
    }

    public function scopeWithminimalPrice($query){
        $minPrice = $query->min('price');
        return $query->where('price', $minPrice);
        // return $query->where('price', 'MIN(price)');
    }
    
    public function scopeWithminimalComparePrice($query){
        $minPrice = $query->min('compare_price');
        return $query->where('compare_price', $minPrice);
    }
    /**
     * @param $query
     * @param $provider_id
     * @return mixed
     */
    public function scopeByProviderId($query, $provider_id)
    {
        return $query->where('provider_id', $provider_id);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function provider()
    {
        return $this->belongsTo('App\Models\Provider');
    }

    public function providerName(){
      return $this->provider->title;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany('App\Models\Image');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ingredients()
    {
        return $this->hasMany('App\Models\Ingredient');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function nutritionals()
    {
        return $this->hasMany('App\Models\Nutritional');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function tradeItemIdentity()
    {
        return $this->hasOne(TradeItemIdentity::class, 'gtin', 'external_key');
    }

    public function getTradeItemId()
    {
       if(! $item_identity = $this->tradeItemIdentity) {
           return null;
       }

       if(! $item_header = $item_identity->tradeItemHeader) {
           return null;
       }

       return $item_header->tradeItem->id;
    }

}
