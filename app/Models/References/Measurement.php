<?php

namespace App\Models\References;

use Illuminate\Database\Eloquent\Model;
use App\Sorters\Sort;

class Measurement extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'references_measurements';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    protected $fillable = ['title', 'code', 'description', 'isUsedInSweden', 'child_measurement', 'child_measurement_quantity'];

    public function scopeSort($query, $request){
        return (new Sort($query, $request))->call();
    }

    public function formatedTitle(){
        return str_replace("Per ", "", $this->title);
    }

    public function aliases(){
        return $this->morphToMany('App\Models\Alias', 'aliasable');
    }
}
