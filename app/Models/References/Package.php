<?php

namespace App\Models\References;

use Illuminate\Database\Eloquent\Model;
use App\Sorters\Sort;
use Elasticquent\ElasticquentTrait;

class Package extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    // protected $morphClass = 'App\Models\References\Package';
    protected $table = 'references_packages';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'code', 'description', 'isUsedInSweden'];
    
    public function scopeSort($query, $request){
        return (new Sort($query, $request))->call();
    }

    public function aliases()
    {
        return $this->morphToMany('App\Models\Alias', 'aliasable');
    }
}
