<?php
/**
 * Created by PhpStorm.
 * User: shesser
 * Date: 10.09.18
 * Time: 15:53
 */

namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class ProductCategoryLevel extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    protected $fillable = [
        'product_id',
        'category_id', // Int
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function validooItem()
    {
        return $this->belongsTo(ValidooItem::class, 'product_id');
    }
}