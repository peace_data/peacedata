<?php

namespace App\Models;
use Sleimanx2\Plastic\Searchable;
use Illuminate\Database\Eloquent\Model;
use App\Sorters\Sort;

class Provider extends Model
{
//    use Searchable;
   protected $fillable = ['title', 'url', 'crawler_name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }

    public function scopebyCrawlerName($query, $crawlerName){
        return $query->where('crawler_name', $crawlerName);
    }

    public function scopeSort($query, $request){
        return (new Sort($query, $request))->call();
    }

}
