<?php
/**
 * Created by PhpStorm.
 * User: shesser
 * Date: 04.10.18
 * Time: 16:00
 */
namespace App\Models\Concern\Relations;

trait TradeItemTrait
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function labels()
    {
        return $this->belongsToMany('App\Models\Label', 'trade_item_label')
            ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tradeItemIdentity()
    {
        return $this->belongsTo('App\Models\Validoo\TradeItemIdentity', 'gtin', 'gtin');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany('App\Models\Product', 'external_key', 'gtin');
    }

    /**
     * @return mixed
     */
    public function riseData()
    {
        return $this->belongsToMany('App\Models\RiseData', 'product_category_levels', 'product_id', 'category_id' )
            ->withPivot(['id','product_id', 'category_id']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function productCategoryLevel()
    {
        return $this->hasOne('App\Models\ProductCategoryLevel', 'product_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function validooItemAttributtes()
    {
        return $this->hasMany('App\Models\ValidooItemAttributte');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tradeItem()
    {
        return $this->belongsTo('App\Models\Validoo\TradeItem', 'validoo_trade_item_id');
    }
}