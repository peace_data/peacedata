<?php

namespace App\Models\Concern\Elasticsearch;

use App\Models\References\Package as PackageModel;
use Elasticquent\ElasticquentTrait;
use App\Models\Alias;

class Package extends PackageModel
{
    use ElasticquentTrait;

    function getIndexName()
    {
        return 'food_fighters_alfa_packages';
    }

    public function getIndexDocumentData(){
        return [
            'title' => $this->title,
            'code' => $this->code,
            'aliases' => $this->aliases()->pluck('title')
        ];
    }

    public function aliases()
    {
        return Alias::join('aliasables', 'aliasables.alias_id', 'aliases.id')
            ->where('aliasables.aliasable_type', parent::class)
            ->where('aliasables.aliasable_id', $this->id);
    }

    public static function findByPhrase($query_phrase){
      return self::searchByQuery(array(
        'multi_match' => array(
          "minimum_should_match"=> "20%",
          'query' => $query_phrase,
          "type" => "most_fields",
          'fuzziness' => "AUTO",
          'fields' => ['aliases^1.5', 'title']
            )
        ));
    }
}
