<?php
/**
 * Created by PhpStorm.
 * User: shesser
 * Date: 08.10.18
 * Time: 15:26
 */

namespace App\Models\Concern\Elasticsearch;

use App\Models\Views\TradeItemView as TradeItemViewModel;
use Elasticquent\ElasticquentTrait;
use App\Models\References\Measurement;
use App\Models\TradeItem;
use App\Models\Product;

class TradeItemView extends TradeItem
{
    use ElasticquentTrait;

    // protected $tradeItemView;
    /**
     * For Elascicsearch
     * For Indexing tradeItem to elastic Only for all reindex
     * @return array
     */
    public function getIndexDocumentData()
    {
        if($this->getTradeItemView()){
            $tradeItemView = $this->getTradeItemView();
            $this->category_name = $tradeItemView->category_name;
            $this->category_code = $tradeItemView->category_code;
            $this->product_name = $tradeItemView->product_name;
            $this->brand_name = $tradeItemView->brand_name;
            $this->measurement_unit_code = $tradeItemView->measurement_unit_code;
            $this->compare_measurement = $tradeItemView->compare_measurement;
            $this->package_title = $tradeItemView->package_title;
        }    
        if(!$this->products->count()){
            return;
        }
        $res = array(
            'id' => $this->id,
            'gtin' => $this->gtin,
            'category_name' => $this->category_name,
            // 'category_code' => $this->category_code,
            // 'product_name' => $this->product_name,
            'brand_name' => $this->brand_name,
            'measurement_unit_code' => $this->measurement_unit_code,
            'compare_measurement' => (int)$this->compare_measurement,
            'package_title' => $this->package_title,
            // 'search_name' => $this->fullName(),
            'products' => $this->products->map(function($product){
                return [
                    'title' => $product->title,
                    'category' => $product->category,
                    'brancd' => $product->brand
                ];
            })
            // 'package_aliases' => $this->package->aliases->pluck('title'),
        );
        // if($this->tradeItem){
        $cheapestProduct = $this->cheapestProduct();
        if($cheapestProduct){
            $res['chiepest_price'] = $cheapestProduct->price;
            $res['compare_price'] = $cheapestProduct->compare_price;
            $res['chiepest_product_provider_id'] = $cheapestProduct->provider_id;
        }
        $res['healthy_score'] = $this->healthyScore();
        // }

        return $res;
    }

    public function fullName(){
        return strtolower($this->formatedPriceCompareCode().' '.preg_replace('/"/', "", $this->product_name). ' '. preg_replace('/"/', "", $this->brand_name). ' '. $this->package_title);
    }


    private function formatedPriceCompareCode(){
        $res = $this->price_comparison_content_type_code;
        $measurement = Measurement::where('code', $this->price_comparison_content_type_code)->first();
        if(isset($measurement)){
            $res = $measurement->formatedTitle();
        }
        return $res;
    }

    public static function findByRequest($query){
        return self::searchByQuery($query);
      }

    public static function findByGtin($gtin){
      try{
        return self::where('gtin', $gtin)->first();

      }catch (\Illuminate\Database\QueryException $e){
        return null;
      } 
    }

}