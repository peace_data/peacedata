<?php

namespace App\Models\Concern\Elasticsearch;

// use App\Models\Views\Measurement as MeasurementModel;
use App\Models\References\Measurement as MeasurementModel;
use Elasticquent\ElasticquentTrait;

class Measurement extends MeasurementModel
{
    use ElasticquentTrait;

    // public function getTypeName()
    // {
    //     return 'package';
    // }
    function getIndexName()
    {
        return 'food_fighters_alfa_measurements';
    }

    public static function findByPhrase($query_phrase){
      return self::searchByQuery(array(
        'multi_match' => array(
          "minimum_should_match"=> "20%",
          'query' => $query_phrase,
          "type" => "most_fields",
          'fuzziness' => "AUTO",
          'fields' => ['aliases^1.5', 'title']
            )
        ));
    }
}


