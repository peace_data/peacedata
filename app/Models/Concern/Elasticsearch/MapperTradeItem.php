<?php
/**
 * Created by PhpStorm.
 * User: shesser
 * Date: 05.10.18
 * Time: 15:36
 */
namespace App\Models\Concern\Elasticsearch;


trait MapperTradeItem
{

    // protected $indexSettings = [
    //     'analysis' => [
    //         'char_filter' => [
    //             'replace' => [
    //                 'type' => 'mapping',
    //                 'mappings' => [
    //                     '&=> and '
    //                 ],
    //             ],
    //         ],
    //         'filter' => [
    //             'word_delimiter' => [
    //                 'type' => 'word_delimiter',
    //                 'split_on_numerics' => false,
    //                 'split_on_case_change' => true,
    //                 'generate_word_parts' => true,
    //                 'generate_number_parts' => true,
    //                 'catenate_all' => true,
    //                 'preserve_original' => true,
    //                 'catenate_numbers' => true,
    //             ]
    //         ],
    //         'analyzer' => [
    //             'default' => [
    //                 'type' => 'custom',
    //                 'char_filter' => [
    //                     'html_strip',
    //                     'replace',
    //                 ],
    //                 'tokenizer' => 'whitespace',
    //                 'filter' => [
    //                     'lowercase',
    //                     'word_delimiter',
    //                 ],
    //             ],
    //         ],
    //     ],
    // ];

    protected $indexSettings =
        array (
            // 'type' => 'my_type',
            'analysis' => 
            array (
            'index_analyzer' => 
            array (
                'my_index_analyzer' => 
                array (
                'type' => 'custom',
                'tokenizer' => 'standard',
                'filter' => 
                array (
                    0 => 'lowercase',
                    1 => 'mynGram',
                ),
                ),
            ),
            'search_analyzer' => 
            array (
                'my_search_analyzer' => 
                array (
                'type' => 'custom',
                'tokenizer' => 'standard',
                'filter' => 
                array (
                    0 => 'standard',
                    1 => 'lowercase',
                    2 => 'mynGram',
                ),
                ),
            ),
            'filter' => 
            array (
                'mynGram' => 
                array (
                'type' => 'nGram',
                'min_gram' => 2,
                'max_gram' => 50,
                ),
            ),
            )
        );
    protected $mappingProperties = array(
        'gtin' => array(
            'type' => 'text',
            'analyzer' => 'standard'
        ),
        'category_name' => array(
            'type' => 'text',
            'analyzer' => 'standard'
        ),
        'category_code' => array(
            'type' => 'integer'
        ),
        'product_name' => array(
            'type' => 'text',
            'analyzer' => 'standard'
        ),
        'brand_name' => array(
            'type' => 'text',
            'analyzer' => 'standard'
        ),
        'search_name' => array(
            'type' => 'text',
            'analyzer' => 'my_search_analyzer'
        )
    );

    public function getTypeName()
    {
        return 'trade_items';
    }
}
