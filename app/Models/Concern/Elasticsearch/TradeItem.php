<?php

namespace App\Models\Concern\Elasticsearch;

use App\Models\Views\TradeItemView as TradeItemViewModel;
use Elasticquent\ElasticquentTrait;
use App\Models\References\Measurement;
use App\Models\References\Package;
use App\Models\TradeItem as TradeItemModel;
use App\Models\Product;

class TradeItem extends TradeItemModel
{
    use ElasticquentTrait;

    // protected $tradeItemView;
    /**
     * For Elascicsearch
     * For Indexing tradeItem to elastic Only for all reindex
     * @return array
     */
    public function getIndexDocumentData()
    {
        if($this->getTradeItemView()){
            $tradeItemView = $this->getTradeItemView();
            $this->category_name = $tradeItemView->category_name;
            $this->category_code = $tradeItemView->category_code;
            $this->product_name = $tradeItemView->product_name;
            $this->brand_name = $tradeItemView->brand_name;
            $this->measurement_unit_code = $tradeItemView->measurement_unit_code;
            $this->compare_measurement = $tradeItemView->compare_measurement;
            $this->package_title = $tradeItemView->package_title;
            $this->price_comparison_content_type_code = $tradeItemView->price_comparison_content_type_code;
            $this->package = $tradeItemView->package;
        }    
        if(!$this->products->count()){
            return;
        }
        $res = array(
            'id' => $this->id,
            'gtin' => $this->gtin,
            'category_name' => $this->category_name,
            // 'category_code' => $this->category_code,
            'product_name' => $this->product_name,
            // 'brand_name' => $this->brand_name,
            // 'measurement_unit_code' => $this->measurement_unit_code,
            'compare_measurement' => (int)$this->compare_measurement,
            // 'package_title' => $this->package_title,
            // 'search_name' => $this->fullName(),
            // 'price_compare_code' => $this->formatedPriceCompareCode(),
            'measurement' => $this->formatedPriceCompareCode(),
            'package' => $this->formatedPackages(),
            'products' => $this->products->map(function($product){
                return [
                    'title' => $product->title,
                    'category' => $product->category,
                    'brand' => $product->brand
                ];
            })
            // 'package_aliases' => $this->package->aliases->pluck('title'),
        );
        // if($this->tradeItem){
        $cheapestProduct = $this->cheapestProduct();
        if($cheapestProduct){
            $res['chiepest_price'] = $cheapestProduct->price;
            $res['compare_price'] = $cheapestProduct->compare_price;
            $res['chiepest_product_provider_id'] = $cheapestProduct->provider_id;
        }
        $res['healthy_score'] = $this->healthyScore();
        // }

        return $res;
    }

    public function fullName(){
        return strtolower($this->formatedPriceCompareCode().' '.preg_replace('/"/', "", $this->product_name). ' '. preg_replace('/"/', "", $this->brand_name). ' '. $this->package_title);
    }


    
    public static function findByRequest($query){
        return self::searchByQuery($query);
    }

    public function getIndexName()
    {
        return 'food_fighters_alfa_trade_items';
    }

    private function formatedPriceCompareCode(){
        $res = $this->price_comparison_content_type_code;
        $measurement = Measurement::where('code', $this->price_comparison_content_type_code)->first();
        if(isset($measurement)){
            $res = [
                'title' => $measurement->formatedTitle(),
                'aliases' => $measurement->aliases->pluck('title')
            ];
        }
        return $res;
    }
    private function formatedPackages(){
        $res = $this->package_title;
        if(isset($this->package)){
            $res = [
                'title' => $this->package->title,
                'aliases' => $this->package->aliases->pluck('title')
            ];
            // $res = $measurement->formatedTitle();
        }
        return $res;
    }

}