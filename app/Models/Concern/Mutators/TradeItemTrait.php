<?php
/**
 * Created by PhpStorm.
 * User: shesser
 * Date: 08.10.18
 * Time: 12:09
 */
namespace App\Models\Concern\Mutators;

use App\Models\Views\TradeItemView;

trait TradeItemTrait
{
    private $tradeItemView = false;

    /**
     * See migration view
     * @return TradeItemView||null
     */
    public function getTradeItemView()
    {
        if($this->tradeItemView === false){
            $this->tradeItemView = TradeItemView::where('gtin', $this->gtin)->first();
        }

        return $this->tradeItemView;
    }

    /**
     * Because Gtin is not require for scribed site
     * @param $value
     * @return String||null
     */
    public function getGtinAttribute($value)
    {
        if(empty($value)) {
            return $this->getTradeItemView() !== null ?  $this->getTradeItemView()->gtin : null;
        }

        return $value;
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getInformationProviderOfTradeItemAttribute($value)
    {
        return $this->getTradeItemView() !== null ? $this->getTradeItemView()->information_provider_of_trade_item : null;
    }

    /**
     * @param $value
     * @return null
     */
    public function getBrandOwnerAttribute($value)
    {
        return $this->getTradeItemView() !== null ? $this->getTradeItemView()->brand_name : null;
    }

    /**
     * @return null
     */
    public function getCreationDateTimeAttribute()
    {
        return $this->getTradeItemView() !== null ? $this->getTradeItemView()->creation_date_time : null;
    }

    /**
     * @return mixed|null
     */
    public function getCategoryNameAttribute()
    {
        return $this->getTradeItemView() !== null ? $this->getTradeItemView()->category_name : null;
    }

    /**
     * @return mixed|null
     */
    public function getCategoryCodeAttribute()
    {
        return $this->getTradeItemView() !== null ? $this->getTradeItemView()->category_code : null;
    }

    /**
     * @return mixed|null
     */
    public function getProductNameAttribute()
    {
        return $this->getTradeItemView() !== null ? $this->getTradeItemView()->product_name : null;
    }
}