<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Sorters\Sort;

class AdditionalClimateScore extends Model
{
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
      'global_code',
      'global_name',
      'region_code',
      'region_name',
      'sub_region_code',
      'sub_region_name',
      'sub_region_score',
      'intermediate_region_code',
      'intermediate_region_name',
      'country_or_area',
      'm49_code',
      'iso_3166_1',
      'iso_alpha3_code',
      'country_score'
    ];

    public function scopeSort($query, $request){
      return (new Sort($query, $request))->call();
    }

}
