<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

/**
 * Class ProviderVersion
 * @package App\Models
 */
class ProviderVersion extends Model
{
    protected $fillable = [
        'provider_id',
        'version',
        'created_at'
    ];

    public $timestamps = false;

    /**
     * @param $value
     */
    public function setCreatedAtAttribute($value)
    {
        $this->attributes['created_at'] = \Carbon\Carbon::now();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }

    /**
     * @param $query
     * @param $provider_id
     * @return mixed
     */
    public function scopeByProviderIdActual($query, $provider_id)
    {
        $sub = $this->queryMaxVersion()
            ->where('provider_id', $provider_id);

        return $this->queryGetResources($query, $sub);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeAllProviders($query)
    {
        return $this->queryGetResources($query, $this->queryMaxVersion());
    }


    /**
     * @param $query
     * @param $sub
     * @return mixed
     */
    private function queryGetResources($query, $sub)
    {
        return $query->joinSub($sub, 'sub', function($join){
            $join->on('provider_versions.provider_id', '=', 'sub.provider_id');
            $join->on('provider_versions.version', '=', 'sub.version');
        });
    }

    /**
     * @return mixed
     */
    private function queryMaxVersion()
    {
        return DB::table('provider_versions')
            ->selectRaw('MAX(version) as version, provider_id')
            ->groupBy('provider_id');
    }
}
