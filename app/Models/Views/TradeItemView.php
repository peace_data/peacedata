<?php

namespace App\Models\Views;

use Illuminate\Database\Eloquent\Model;
use App\Models\AdditionalClimateScore;
use App\Models\TradeItem;
use App\Models\References\Package;
use App\Models\References\Measurement;

class TradeItemView extends Model
{

    protected $table = 'trade_items_view';

    /**
     * @return mixed
     */
    public function additionalClimateScore()
    {
      return AdditionalClimateScore::where('iso_3166_1', $this->target_market_country_code)->get();
    }

    public function tradeItem(){
      return $this->belongsTo(TradeItem::class, 'gtin', 'gtin');
    }

    public function package(){
      return $this->belongsTo(Package::class, 'package_code', 'code');
    }
    
    public function measurement(){
      return $this->belongsTo(Measurement::class, 'measurement_unit_code', 'code');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
    public function products()
    {
        return $this->hasMany('App\Models\Product', 'external_key', 'gtin');
    }

    public function cheapestProduct(){
        return $this->products()->withminimalComparePrice()->first();
    }
  
    public function healthyScore(){
        return rand(30, 2000);
    }

    public static function findByGtin($gtin){
      try{
        return self::where('gtin', $gtin)->first;

      }catch (\Illuminate\Database\QueryException $e){
        return null;
      } 
    }

}

