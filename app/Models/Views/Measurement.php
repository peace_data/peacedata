<?php

namespace App\Models\Views;

use Illuminate\Database\Eloquent\Model;

class Measurement extends Model
{
    protected $table = 'measurements_view';
}

