<?php

if (!function_exists('format_money')) {

    function format_money($value, $currency=null)
    {
        $currency = $currency==null ? '' : " ". $currency;
        if(is_int($value)) {
           $value =  round(((float) $value) / 100, 2) ;
           $value = sprintf("%01.2f", $value);
        }

        return $value.$currency;

    }
}