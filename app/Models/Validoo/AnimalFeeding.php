<?php

namespace App\Models\Validoo;

class AnimalFeeding extends ValidooModel
{

    protected $fillable_relations = ['animalFeedingDetails'];

    public function animalFeedingDetails(){
        return $this->hasMany(AnimalFeedingDetail::class);
    }
}
