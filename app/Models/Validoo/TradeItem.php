<?php

namespace App\Models\Validoo;

use App\Models\RiseData;
use Storage;
use App\Models\Validoo\Image;


class TradeItem extends ValidooModel
{

    protected $fillable_relations = [
     'tradeItemHeader',
     'dangerousGoods',
     'alocohol',
     'allergens',
     'animalFeedingInformation',
     'temperature',
     'battery',
     'consumerInstructions',
     'dairyFishMeatePoultryItems',
     'sizes',
     'dangerousSubstances',
     'descriptions',
     'nutritionalInformations',
     'salesInformation',
     'measurement',
     'packagings',
     'diet',
     'dutyFeeTax',
     'deliveryAndPurchasing',
     'preparationAndServing',
     'health',
     'healthAndWellnessMarkings',
     'marketingInformations',
     'season',
     'nonFoodIngredientInformation',
     'marking',
     'fileReferences',
     'itemRegulations',
     'safetyData',
     'sustainability',
     'dataCarriers',
     'disposal',
     'handling',
     'hierarchy',
     'humidities',
     'lifespan',
     'variableUnit',
   //   'fishReportingInformations',
   //   'riskPhrases',
   ];

   public static function findOrCreateByGtin($gtin)
   {
     $obj = self::findByGtin($gtin);
     return $obj ?: self::create();
   }

   public static function findByGtin($gtin){
     return self
      ::select('validoo_trade_items.id')
      ->join('validoo_trade_item_headers', 'validoo_trade_item_headers.trade_item_id', 'validoo_trade_items.id')
      ->join('validoo_trade_item_identities', 'validoo_trade_item_identities.trade_item_header_id', 'validoo_trade_item_headers.id')
      ->where('validoo_trade_item_identities.gtin', $gtin)
      ->first();
   }

   public function gtin()
   {
     return $this->tradeItemHeader->tradeItemIdentity->gtin;
   }

   public function images(){
     return Image::byGtin($this->gtin())->get();
   }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
   public function tradeItemHeader()
   {
     return $this->hasOne(TradeItemHeader::class);
   }
   public function salesInformation()
   {
     return $this->hasOne(SalesInformation::class);
   }
   public function battery()
   {
     return $this->hasOne(Battery::class);
   }
   public function alcohol()
   {
     return $this->hasOne(Alcohol::class);
   }
   public function consumerInstruction()
   {
     return $this->hasOne(ConsumerInstruction::class);
   }
   public function temperature()
   {
     return $this->hasOne('App\Models\Validoo\Temperature', 'trade_item_id');
   }

   public function animalFeedingInformation()
   {
     return $this->hasOne(AnimalFeedingInformation::class);
   }

   public function dairyFishMeatePoultryItems()
   {
     return $this->hasOne(DairyFishMeatePoultryItem::class);
   }

   public function dangerousGoods()
   {
     return $this->hasMany(DangerousGood::class);
   }

   public function allergens()
   {
     return $this->hasMany(Allergen::class);
   }

   public function sizes()
   {
     return $this->hasMany(Size::class);
   }

   public function dangerousSubstances()
   {
     return $this->hasMany(DangerousSubstance::class);
   }

   public function nutritionalInformations()
   {
     return $this->hasMany(NutritionalInformation::class);
   }

   public function descriptions()
   {
      return $this->hasOne(Description::class);
   }
   public function measurement()
   {
      return $this->hasOne(Measurement::class);
   }
   public function packagings()
   {
     return $this->hasMany(Packaging::class);
   }
   public function deliveryAndPurchasing()
   {
      return $this->hasOne(DeliveryAndPurchasing::class);
   }
   public function diet()
   {
      return $this->hasOne(Diet::class);
   }
   public function dutyFeeTax()
   {
      return $this->hasMany(DutyFeeTax::class);
   }
   public function preparationAndServing()
   {
      return $this->hasOne(PreparationAndServing::class);
   }
   public function health()
   {
      return $this->hasOne(Health::class);
   }
   public function healthAndWellnessMarkings()
   {
      return $this->hasMany(HealthAndWellnessMarkings::class);
   }
   public function marketingInformations()
   {
      return $this->hasOne(MarketingInformations::class);
   }
   public function season()
   {
      return $this->hasOne(Season::class);
   }
   public function nonFoodIngredientInformation()
   {
      return $this->hasOne(NonFoodIngredientInformation::class);
   }

   public function marking()
   {
      return $this->hasOne(Marking::class);
   }
   public function itemActivity()
   {
      return $this->hasOne(ItemActivity::class);
   }
   public function fileReferences()
   {
      return $this->hasMany(FileReference::class);
   }
   public function itemRegulations()
   {
      return $this->hasMany(ItemRegulation::class);
   }
   public function safetyData()
   {
      return $this->hasMany(SafetyData::class);
   }
   public function sustainability()
   {
      return $this->hasOne(Sustainability::class);
   }
   public function dataCarriers()
   {
      return $this->hasMany(DataCarrier::class);
   }
   public function disposal()
   {
      return $this->hasOne(Disposal::class);
   }
   public function handling()
   {
      return $this->hasOne(Handling::class);
   }
   public function hierarchy()
   {
      return $this->hasOne(Hierarchy::class);
   }
   public function humidities()
   {
      return $this->hasMany(Humidity::class);
   }
   public function lifespan()
   {
      return $this->hasOne(Lifespan::class);
   }
   public function variableUnit()
   {
      return $this->hasOne(VariableUnit::class);
   }
   public function warranties()
   {
      return $this->hasMany(Warranty::class);
   }
   // public function fishReportingInformations()
   // {
   //    return $this->hasMany(FishReportingInformation::class);
   // }

   public function ingredients()
   {
      return $this->hasOne(Ingredient::class);
   }
}