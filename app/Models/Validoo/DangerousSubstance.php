<?php

namespace App\Models\Validoo;

class DangerousSubstance extends ValidooModel
{
     protected $fillable_relations = ['riskPhrases'];

    /**
     * TODO to need checking
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function riskPhrases()
    {
        return $this->hasMany(RiskPhrase::class);
    }

}
