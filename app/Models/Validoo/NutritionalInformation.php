<?php

namespace App\Models\Validoo;


class NutritionalInformation extends ValidooModel
{
    protected $fillable_relations = [
        'nutritialHeaders',    
    ];
    public function nutritialHeaders(){
        return $this->hasMany(NutritialHeader::class);
    }
}
