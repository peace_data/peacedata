<?php

namespace App\Models\Validoo;


class TradeItemHeader extends ValidooModel
{
  protected $fillable_relations = [
    'tradeItemIdentity',
    'clasificationCategory',
    'additionalTradeItemClasifications',
    'tradeItemComponent',
    'packagingHierarchy',
    'tradeItem'
  ];

  public function tradeItemIdentity(){
    return $this->hasOne(TradeItemIdentity::class);
  }
  public function clasificationCategory(){
    return $this->hasOne(ClasificationCategory::class);
  }
  public function additionalTradeItemClasifications(){
    return $this->hasMany(AdditionalTradeItemClasification::class);
  }
  public function tradeItemComponent(){
    return $this->hasOne(TradeItemComponent::class);
  }
  public function packagingHierarchy(){
    return $this->hasOne(PackagingHierarchy::class);
  }
  public function tradeItem(){
    return $this->belongsTo(TradeItem::class);
  }
}
