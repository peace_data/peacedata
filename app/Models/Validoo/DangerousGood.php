<?php

namespace App\Models\Validoo;

class DangerousGood extends ValidooModel
{

    protected $fillable_relations = ['hazardousInformationDetails'];

    public function hazardousInformationDetails()
    {
        return $this->hasMany(HazardousInformationDetail::class);
    }
}
