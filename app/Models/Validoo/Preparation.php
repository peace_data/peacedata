<?php

namespace App\Models\Validoo;


class Preparation extends ValidooModel
{
    protected $fillable_relations = [
        'productYieldInformations',
    ];
    public function productYieldInformations()
    {
        return $this->hasMany(ProductYieldInformation::class);
    }
}
