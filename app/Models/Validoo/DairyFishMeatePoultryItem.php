<?php

namespace App\Models\Validoo;


class DairyFishMeatePoultryItem extends ValidooModel
{
  

  protected $fillable_relations = ['fishReportingInformations'];

    /**
     * TODO to need checking
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
  public function fishReportingInformations()
  {
    return $this->hasMany(FishReportingInformation::class);
  }

}
