<?php

namespace App\Models\Validoo;

class Description extends ValidooModel
{

    protected $fillable_relations = [
        'tradeItem',
        'colours'
    ];

    public function tradeItem(){
        return $this->belongsTo(TradeItem::class);
    }
    public function colours(){
        return $this->hasMany(Colour::class);
    }


    public function getDescriptionShortAttribute($value)
    {
        $value = json_decode($value);
        return $value[0]->{'0'};
    }

    public function getBrandNameInformationAttribute($value)
    {
        $value = json_decode($value);
        return $value[0]->brandName;
    }
}
