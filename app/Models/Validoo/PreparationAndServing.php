<?php

namespace App\Models\Validoo;

class PreparationAndServing extends ValidooModel
{
    protected $fillable_relations = [
        'preparations',
    ];
    public function preparations()
    {
        return $this->hasMany(Preparation::class);
    }
}
