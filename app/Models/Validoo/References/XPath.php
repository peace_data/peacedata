<?php

namespace App\Models\Validoo\References;

use Illuminate\Database\Eloquent\Model;
use App\Models\Validoo\ValidooModel;

class XPath extends ValidooModel
{
  protected $table;
  protected $fillable = ['model_name', 'x_path', 'code', 'field_name', 'cardinality'];

  

  public function isSingle(){
    return $this->cardinality == 'single';
  }

  public static function groupedByModel(){
    return self::all()->groupBy('model_name');
  }
}
