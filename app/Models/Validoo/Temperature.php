<?php

namespace App\Models\Validoo;


class Temperature extends ValidooModel
{

  protected $fillable = ['trade_item_id', 'trade_item_temperature_condition_type_code'];
  protected $fillable_relations = ['tradeItem', 'temperatureInformations'];

  public function temperatureInformations(){
    return $this->hasMany(TemperatureInformation::class);
  }

  public function tradeItem()
  {
      return $this->belongsTo('App\Models\Validoo\TradeItem');
  }

}
