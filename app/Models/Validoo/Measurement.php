<?php

namespace App\Models\Validoo;

class Measurement extends ValidooModel
{
    protected $fillable_relations = [
        'additionalTradeItemDimensions',
    ];
    public function additionalTradeItemDimensions()
    {
        return $this->hasMany(AdditionalTradeItemDimension::class);
    }
}
