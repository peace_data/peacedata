<?php

namespace App\Models\Validoo;


class Packaging extends ValidooModel
{
    protected $fillable_relations = [
        'packagingMaterials',
        'returnableAssets',
    ];
    public function packagingMaterials()
    {
        return $this->hasMany(PackagingMaterial::class);
    }
    public function returnableAssets()
    {
      return $this->hasMany(ReturnableAsset::class);
    } 
}
