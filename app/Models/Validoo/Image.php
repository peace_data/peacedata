<?php

namespace App\Models\Validoo;

use Illuminate\Database\Eloquent\Model;
use Storage;

class Image extends ValidooModel
{
    protected $fillable = ['src', 'middle', 'thumb', 'gtin'];

    public function scopeByGtin($query, $gtin){
      return $query->where('gtin', $gtin);
    }

    public function thumb(){
      return Storage::disk('public')->url($this->attributes['thumb']);
    }
    public function middle(){
      return Storage::disk('public')->url($this->attributes['middle']);
    }
    public function src(){
      return Storage::disk('public')->url($this->attributes['src']);
    }
}
