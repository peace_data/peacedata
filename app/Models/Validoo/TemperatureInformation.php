<?php

namespace App\Models\Validoo;


class TemperatureInformation extends ValidooModel
{
    protected $fillable_relations = [
        'temperatures',
    ];
    
}
