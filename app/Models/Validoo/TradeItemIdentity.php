<?php

namespace App\Models\Validoo;


class TradeItemIdentity extends ValidooModel
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tradeItemHeader()
    {
        return $this->belongsTo(TradeItemHeader::class);
    }
}
