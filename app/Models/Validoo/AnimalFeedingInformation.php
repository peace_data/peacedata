<?php

namespace App\Models\Validoo;

class AnimalFeedingInformation extends ValidooModel
{

    protected $fillable_relations = ['animalFeedings'];

    public function animalFeedings()
    {
        return $this->hasMany(AnimalFeeding::class);
    }
}
