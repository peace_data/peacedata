<?php

namespace App\Models\Validoo;


class NutritialHeader extends ValidooModel
{
    protected $fillable_relations = [
        'nutrientDetails',    
    ];
    public function nutrientDetails(){
        return $this->hasMany(NutrientDetail::class);
    }
}
