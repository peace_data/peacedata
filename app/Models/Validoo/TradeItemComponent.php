<?php

namespace App\Models\Validoo;

class TradeItemComponent extends ValidooModel
{
  protected $fillable_relations = ['component_informations'];

  public function componentInformations(){
    return $this->hasMany(ComponentInformation::class);
  }
}
