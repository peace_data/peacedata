<?php

namespace App\Models\Validoo;

use Illuminate\Database\Eloquent\Model;
use LaravelFillableRelations\Eloquent\Concerns\HasFillableRelations;
use App\RelationshipsTrait;
use Schema;
use DB;
class ValidooModel extends Model
{
    use RelationshipsTrait;
    use HasFillableRelations;

    public function __construct(array $attributes = array())
    {
      $this->table = env('DB_VALIDOO_TABLE_PREFIX').$this->getTable();
      // $this->with = $this->relationships();
      parent::__construct($attributes);
      $this->fillable = Schema::getColumnListing($this->table);;


    }
    // protected $fillable = [];
    protected $guarded = ['id'];

}
