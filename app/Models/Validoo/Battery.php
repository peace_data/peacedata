<?php

namespace App\Models\Validoo;

class Battery extends ValidooModel
{
  protected $fillable_relations = ['batteryInformations'];

  public function batteryInformations(){
    return $this->hasMany(BatteryInformation::class);
  }
}
