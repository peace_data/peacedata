<?php

namespace App\Models\Validoo;


class AnimalFeedingDetail extends ValidooModel
{

  protected $fillable_relations = ['animalNutrientDetails'];

  public function animalNutrientDetails(){
    return $this->hasMany(AnimalNutrientDetail::class);
  }
}
