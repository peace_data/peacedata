<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Sorters\TradeItem\TradeItemSorter;
use App\Models\Concern\Relations\TradeItemTrait as RelationsTrait;
use App\Models\Concern\Mutators\TradeItemTrait as MutatorsTrait;
use App\Models\Validoo\Nutrient;


class TradeItem extends Model
{
    use RelationsTrait;
    use MutatorsTrait;

    protected $fillable = ['validoo_trade_item_id', 'gtin'];

    /**
     * @return mixed
     */
    public function labelScores(){
      return LabelType::groupedByTradeItem($this->id)->pluck('sum', 'label_types.title');
    }

    /**
    * @param $query
    * @return mixed
    */
    public function scopeByNeedAssignedCategoryLevel($query)
    {
        # TODO add logic for query
        return $query;
    }

    public function nutritients()
    {
        $a = Nutrient::join('validoo_nutritial_headers', 'validoo_nutritial_headers.id', 'validoo_nutrients.nutritial_header_id')
          ->join('validoo_nutritional_informations', 'validoo_nutritional_informations.id', 'validoo_nutritial_headers.nutritional_information_id')
          ->join('validoo_trade_items', 'validoo_trade_items.id', 'validoo_nutritional_informations.trade_item_id')
          ->where('validoo_trade_items.id', $this->validoo_trade_item_id);
        return $a;
    }

    /**
     * @param $query
     * @param $tradeItem
     * @return mixed
     */
    public static function scopeFindOrCreateByTradeItem($query, $tradeItem)
    {
        $obj = $query->where('validoo_trade_item_id', $tradeItem->id)->first();
        return $obj ?: self::create(
          [
            'validoo_trade_item_id' => $tradeItem->id,
            'gtin' => $tradeItem->tradeItemHeader->tradeItemIdentity->gtin
          ]
        );
    }

    /**
     * @param $query
     * @param $gtin
     * @return mixed
     */
    public function scopeFindOrCreateByGtin($query, $gtin){
        $obj = $query->where('gtin', $gtin)->first();
        return $obj ?: self::create(
          [
            'gtin' => $gtin
          ]
        );
    }

    /**
     * @param $query
     * @param $request
     * @return mixed
     */
    public function scopeSort($query, $request)
    {
        return (new TradeItemSorter($query, $request))->call();
    }


    /**
     * @return array
     */
    public function images(){
        if(!empty($this->tradeItem)){
          return $this->tradeItem->images();
        }
        return [];
    }

    public function additionalClimateScore()
    {
        return $this->getTradeItemView() !== null ? $this->tradeItemView->additionalClimateScore() : [];
    }

    public function cheapestProduct(){
        return $this->products()->withminimalComparePrice()->first();
    }
  
    public function healthyScore(){
        return rand(30, 2000);
    }
}
