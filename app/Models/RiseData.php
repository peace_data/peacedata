<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Filters\RiseData\RiseDataFilter;
use Illuminate\Database\Eloquent\Builder;
use App\Sorters\Sort;

class RiseData extends Model
{
    protected $table = 'rise_data';

    protected $fillable = [
        'food_cat_1_id',
        'food_cat_1_name',
        'food_cat_2_id',
        'food_cat_2_name',
        'food_cat_3_id',
        'food_cat_3_name',
        'slv_id',
        'slv_name',
        'activity_id',
        'activity_name',
        'region_id',
        'region_name',
        'region_source',
        'production_type',
        'production_type_source',
        'ghg_total_value'
    ];

    /**
     * @return $this
     */
    public function trideItem()
    {
        return $this->belongsToMany(TrideItem::class, 'product_category_levels', 'booking_id', 'metatype_id')
            ->withPivot([ 'product_id', 'category_id']);
    }

    /**
     * @param Builder $builder
     * @param $request
     * @return Builder
     */
    public function scopeFilter(Builder $builder, $request)
    {
        return (new RiseDataFilter($request))->filter($builder);
    }

    public function scopeSort($query, $request){
        return (new Sort($query, $request))->call();
    }
}
