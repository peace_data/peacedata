<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Validoo\Image;
use Intervention\Image\ImageManager;
use Storage;
use File;
use App\Models\TradeItemSubscription;
use App\Validoo\ExternalApi\Subscribe\Requests\Delete;
use App\Validoo\ExternalApi\Subscribe\Requests\CreateImage;

class RemoveBrokenImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'images:remove_broken';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    private $manager;

    public function __construct()
    {
        $this->manager = new ImageManager(array('driver' => 'imagick'));
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $validCounter = 0;
        $brokenCounter = 0;
        
        Image::chunk(100, function($images) use(&$validCounter, &$brokenCounter){
            foreach($images as $image){
                try{
                    $filepath = Storage::disk('public')->getAdapter()->getPathPrefix().$image->middle;
                    $this->manager->make($filepath);
                    $percentWithFractalColor = system('convert '. $filepath. ' +opaque fractal -format "%[fx:mean*100]" info:');
                    echo(PHP_EOL);

                    if((float) $percentWithFractalColor < 99.0){
                        
                        $subscribe = TradeItemSubscription::where('gtin', $image->gtin)->where('type', 'image')->first();

                        File::delete(Storage::disk('public')->getAdapter()->getPathPrefix().$image->thumb);
                        File::delete(Storage::disk('public')->getAdapter()->getPathPrefix().$image->src);
                        File::delete(Storage::disk('public')->getAdapter()->getPathPrefix().$image->middle);
                        (new Delete($subscribe->subscribe_id))->call();
                        $subscribeRes = (new CreateImage($image->gtin))->call();
                        
                        $subscribe->subscribe_id = $subscribeRes->toArray()['subscribe_id'];
                        $subscribe->save();
                        echo(PHP_EOL);
                        $image->delete();
                        
                        $brokenCounter++;
                    }else{
                        $validCounter++;
                    }
                }catch(\Exception $e){
                    echo($e);
                }
                
            }
        });
        echo('valid images '.$validCounter.PHP_EOL);
        echo('broken images '.$brokenCounter.PHP_EOL);
        echo('all images '.($brokenCounter + $validCounter) .PHP_EOL);

    }
}
