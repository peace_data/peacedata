<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\TradeItem;

class AddGtinToValidooItem extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'trade_items:add_gtin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add gtin to existing validoo item models';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $validooItems = TradeItem::all();
        foreach ($validooItems as $validooItem) {
            $validooItem->gtin =  $validooItem->tradeItem->tradeItemHeader->tradeItemIdentity->gtin;
            $validooItem->save();
        }
        echo 'Gtin was added';
    }
}
