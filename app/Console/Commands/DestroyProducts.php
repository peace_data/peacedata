<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\Coop\CoopRepository;

class DestroyProducts extends Command
{
    protected $list_providers = ['Coop', 'Paradiset'];
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'products:destroy {provider}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete all product by provider';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $provider = $this->argument('provider');

        if(!in_array($provider, $this->list_providers)) {
            $this->error('Unknown provider!!!');
            return ;
        }

        $class = 'App\Repositories\\'.$provider.'\\'.$provider.'Repository';
        $result = $class::destroyAll();

        $this->info($result . ' of products are deleted!');
        $this->info('Task is done!!! ');
    }
}
