<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\ProviderVersion;
use App\Models\Product;

class InitProductVersion extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'init:product_versions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $products = Product::all();
        $versions = ProviderVersion::allProviders()->get();

        foreach($products as $product) {
            $product->provider_version_id = $versions->filter(function($item) use($product) {
                return $item->provider_id == $product->provider_id;
            })->first()->id;
            $product->save();
        }
    }
}
