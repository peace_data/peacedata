<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Product;
use App\Models\TradeItem;

class InitTradeItemByProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'trade_items:by_products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach(Product::all()->unique('external_key') as $product){
            $tradeItem = TradeItem::findOrCreateByGtin($product->external_key);
            if($tradeItem->wasRecentlyCreated){
                echo($product->title.' '. $product->external_key.PHP_EOL);
            }
        }

    }
}
