<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class ClearValidooItems extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'validoo_items:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      DB::statement('TRUNCATE validoo_trade_items CASCADE');
    }
}
