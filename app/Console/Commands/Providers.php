<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Provider;

class Providers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:provider {title} {crawler_name} {url}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create provider for crawling sites';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $title = $this->argument('title');
      $url = $this->argument('url');
      $crawler_name = $this->argument('crawler_name');
      Provider::create([
        'title' => $title,
        'url' => $url,
        'crawler_name' => $crawler_name
      ]);
    }
}
