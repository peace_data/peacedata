<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\Coop\CoopRepository as CoopRepository;
use App\Models\Product;
use App\Models\Provider;
use App\Models\TradeItemSubscription;
use Carbon\Carbon;
use App\Jobs\Validoo\CreateSubscriptionJob;
use App\Validoo\ExternalApi\Subscribe\Services\Create;
use App\Validoo\Services\HandlerAllFilesFromStorage;
use App\Models\ProviderVersion;
use App\Services\UpdaterProviderVersion;

class RunSpiders extends Command
{

    protected $list_spiders;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'run:spider {spider} {--save}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run spider';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     parent::__construct();
    // }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->list_spiders = $this->init_crawler_list();

        $start_time = Carbon::now();

        $spider = $this->argument('spider');
        if(!in_array($spider, $this->list_spiders)) {
            $this->error('Unknown spider!!!');
            return ;
        }

        $class = 'App\Repositories\\'.$spider.'\\'.$spider.'Repository';
        $this->info('Start retrieve data ');


        $results = $class::all();

        $this->info('Retrieved products is ' . $this->countProducts($results));
        $this->info('Duration of spinning is ' . Carbon::now()->diffInSeconds($start_time) . ' s');

        if($this->option('save')) {
            $this->all_save($results, $spider);
            $this->info( $this->countProducts($results) . ' of products are stored');
        }

        $this->info( 'Task is done! Duration is ' . Carbon::now()->diffInSeconds($start_time) . ' s');
    }


    /**
     * @return mixed
     */
    private function init_crawler_list()
    {
        return Provider::get()->pluck(['crawler_name'])->toArray();
    }


    /**
     * Save results of spinning to db
     * @param $result
     */
    private function all_save($result, $spider)
    {
        echo('Start saving'.PHP_EOL);

      $provider = Provider::byCrawlerName($spider)->first();
      $arr = array_filter(array_map(function($category){
          if(is_array($category)){
             return array_filter(array_map(function($item){
                  return $item->toArray();
              }, $category));
          }

      }, $result));
      $counter = 0;
      //For updating version
      $product_ids = [];

      foreach($arr as $category) {
          foreach($category as $item) {
            if($counter < 50){
                $product = $provider->products()->findOrCreateByExternalKey($item['external_key']);
                if($product->exists){
                    $product->update($item);
                }else{
                    $product->fill($item);
                    $product->save();
                }

                array_push($product_ids, $product->id);

                $subscribe = TradeItemSubscription::where('gtin', $item['external_key'])->first();
                if(empty($subscribe)){
                    
                    // sleep(2);
                    echo($item['external_key']);
                    echo('----------------------------------------');

                    // before creating next subscription we try to parse all existing tradeItem of previous subscriptions
                    // $handler = new HandlerAllFilesFromStorage;
                    // $handler->execute();

                    dispatch(new CreateSubscriptionJob($item['external_key']));
                }
            }
            $counter++;
          }


      }

      //Update all Products with new version
        $updater_version = new UpdaterProviderVersion($provider);
        $updater_version->execute($product_ids);
    }

    private function countProducts($results)
    {
        $count = 0;
        foreach($results as $category) {
            if (is_null($category)) {
                continue;
            }
            $count += count($category);
        }

        return $count;
    }

}
