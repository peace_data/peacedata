<?php

namespace App\Console\Commands\Validoo;

use Illuminate\Console\Command;
use App\Models\TradeItemSubscription;
use App\Jobs\Validoo\CreateSubscriptionJob;
use App\Validoo\ExternalApi\Subscribe\Requests\Delete;


class ReinitSubscribe extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'validoo:reinit_subscribe {gtin}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $gtin = $this->argument('gtin');

        if(!$gtin) {
            $this->error('Unknown gtin subscriber!!!');
            return ;
        }

        $subscribes = TradeItemSubscription::where('gtin', $gtin)->get();
        if(!empty($subscribes)){
            foreach($subscribes as $subscribe){
                $r = (new Delete($subscribe->subscribe_id))->call();

                $subscribe->delete();   
            }
        }else{
        }
        $this->info('Created subscription for ' . $gtin);
        dispatch(new CreateSubscriptionJob($gtin));
        $this->info('See logs workers on /var/log/worker.out.log');
        
    }
}
// 08437000542278