<?php

namespace App\Console\Commands\Validoo;

use Illuminate\Console\Command;
use App\Validoo\Services\HandlerAllFilesFromStorage;

class ParseFromValidooStorage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'validoo:storage_parse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $handler = new HandlerAllFilesFromStorage;
        $handler->execute();
    }
}
