<?php

namespace App\Console\Commands\Validoo;

use Illuminate\Console\Command;
use App\Jobs\Validoo\CreateSubscriptionJob;

class CreateSubscription extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'validoo:create_subscription {gtin}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Validoo description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $gtin = $this->argument('gtin');

        if(!$gtin) {
            $this->error('Unknown gtin subscriber!!!');
            return ;
        }

        $this->info('Created subscription for ' . $gtin);
        dispatch(new CreateSubscriptionJob($gtin));
        $this->info('See logs workers on /var/log/worker.out.log');

    }
}
