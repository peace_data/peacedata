<?php

namespace App\Console\Commands\Validoo;

use Illuminate\Console\Command;
use Artisan;
use DB;
use Database\Views\TradeItemViewV7;
use App\Models\Views\TradeItemView;
use App\Validoo\Services\HandlerAllFilesFromStorage;
class ReinitXPaths extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'validoo:reinit_x_paths';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'add gpc category name to exist validoo items';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::statement('TRUNCATE validoo_x_paths');
        
        DB::statement('TRUNCATE validoo_trade_items CASCADE');
        
        Artisan::call('db:seed', ['--class' => 'ValidooXPathSeeder']);

        // TradeItemViewV7::create();
        (new HandlerAllFilesFromStorage(config('validoo.stores.archive_store_path')))->execute();
    }
}
