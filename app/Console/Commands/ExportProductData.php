<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Product;
use Excel;
use App\Models\Validoo\TradeItemIdentity;

class ExportProductData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'products:export';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Excel::create('Products', function($excel) {


            $excel->sheet('Sheet 1', function ($sheet) {
                $products = Product::with('provider')->get();
                $sheet->setOrientation('landscape');
                $sheet->appendRow($this->xlsxHeader());  

                foreach($products->chunk(1000) as $chunk){
                    foreach($chunk as $product){
                        $data = $product->toArray();
                        $data['store'] = $data['provider']['title'];
                        $data['has_validoo_data'] = TradeItemIdentity::where('gtin', $data['external_key'])->exists() ? 'yes' : 'no';

                        unset($data['informations']);
                        unset($data['provider']);
                        unset($data['provider_id']);
                        unset($data['provider_version_id']);
                        $sheet->appendRow($data);    
                    }
                    
                }
                
            });

        })->store('xlsx');
    }

    private function xlsxHeader(){
        return[
            'id','title','weight','price','price_unit','currency','compare_price',
            'compare_unit','brand','category','isEko','external_key',
            'created_at','updated_at','store', 'has_validoo_data'
        ];
    }
}
