<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use File;
use XmlParser;
use App\Validoo\Parsers\Validoo as ValidooParser;
use App\Builders\Validoo\XmlTree;
use DB;
use Artisan;
use App\Validoo\Parsers\Xml\Validoo as ValidooXmlParser;
use App\Validoo\Handlers\Handler;


class ParseTestXml extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'xml:parse_test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse Test Xml from Validoo';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // $productXml = file('public/test_xml/nutr.XML');

        // DB::statement('TRUNCATE validoo_x_paths');
        // DB::statement('TRUNCATE validoo_trade_items CASCADE');
        // Artisan::call('db:seed', ['--class' => 'ValidooXPathSeeder']); 

        Handler::call('test.xml', 'public/test_xml/test.xml');
    
    }
}
