<?php

namespace App\Console\Commands\Elasticsearch;

use Illuminate\Console\Command;
use App\Services\Elasticsearch\CreateIndexPackages;
use Carbon\Carbon;


class CreatePackageIndex extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'elastic_search:create_index_packages';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Initialise CreatePackageIndex Service');
        $start_time = Carbon::now();
        $handler = new CreateIndexPackages;
        $handler->execute();

        $this->info( 'Duration of indexing is ' . Carbon::now()->diffInSeconds($start_time) . ' s');
    }
}
