<?php

namespace App\Console\Commands\Elasticsearch;

use Illuminate\Console\Command;
use App\Services\ElasticSearch\ReindexTradeItems as ReindexTradeItemsService;
use Carbon\Carbon;

class ReindexTradeItems extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'elastic_search:reindex_trade_items';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Initialise ReindexTradeItems Service');
        $start_time = Carbon::now();

        try {
            $handler = new ReindexTradeItemsService();
            $handler->execute();
        }catch (\Elasticsearch\Common\Exceptions\Missing404Exception $e) {
            $this->error( 'Undefined index ' );
            $this->info( 'Run command:  ' . 'php artisan elastic_search:create_index_trade_items');
            return ;
        }

        $diff = Carbon::now()->diffInSeconds($start_time);
        $this->info( 'Before numbers is ' . (json_encode($handler->getInfoBeforeIndexing())));

        /** TODO it is hack */
        sleep(5);
        $this->info( 'After numbers is ' . (json_encode($handler->getInfoAfterIndexing())));

        $this->info( 'Duration of indexing is ' . $diff . ' s');
    }
}
