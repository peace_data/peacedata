<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\TradeItemSubscription;
use App\Models\TradeItem;
use App\Validoo\ExternalApi\Subscribe\Requests\Delete;

class ClearUndownloadedSubscribes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subscribers:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $failedSubcribes = TradeItemSubscription::whereNotIn('gtin', TradeItem::pluck('gtin'))->get();
      echo('failed subscribes'.$failedSubcribes->count());
      foreach ($failedSubcribes as $subscribe) {
        (new Delete($subscribe->subscribe_id))->call();
        echo($subscribe->gtin);
        echo('--------------------------------------------');
        $subscribe->delete();
      }
    }
}
