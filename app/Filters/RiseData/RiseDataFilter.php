<?php
/**
 * Created by PhpStorm.
 * User: shesser
 * Date: 11.09.18
 * Time: 10:31
 */
namespace App\Filters\RiseData;

use App\Filters\AbstractFilter;
use Illuminate\Database\Eloquent\Builder;

class RiseDataFilter extends AbstractFilter
{
    protected $filters = [
        'food_cat_1_name' => FoodCat1Name::class,
        'food_cat_2_name' => FoodCat2Name::class,
        'food_cat_3_name' => FoodCat3Name::class,
        'slv_name' => SlvName::class,
        'activity_name' => ActivityName::class
    ];
}