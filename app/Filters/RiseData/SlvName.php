<?php
/**
 * Created by PhpStorm.
 * User: shesser
 * Date: 11.09.18
 * Time: 11:12
 */
namespace App\Filters\RiseData;

class SlvName
{
    /**
     * @param $builder
     * @param $value
     * @return mixed
     */
    public function filter($builder, $value)
    {
        return $builder->where('slv_name', 'LIKE', "$value%");
    }
}