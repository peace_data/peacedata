<?php
/**
 * Created by PhpStorm.
 * User: shesser
 * Date: 11.09.18
 * Time: 11:14
 */
namespace App\Filters\RiseData;

class ActivityName
{
    /**
     * @param $builder
     * @param $value
     * @return mixed
     */
    public function filter($builder, $value)
    {
        return $builder->where('activity_name', 'LIKE', "$value%");
    }
}