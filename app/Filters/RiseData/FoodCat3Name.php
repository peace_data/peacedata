<?php
/**
 * Created by PhpStorm.
 * User: shesser
 * Date: 11.09.18
 * Time: 10:38
 */
namespace App\Filters\RiseData;

class FoodCat3Name
{
    /**
     * @param $builder
     * @param $value
     * @return mixed
     */
    public function filter($builder, $value)
    {
        return $builder->where('food_cat_3_name', 'LIKE', "$value%");
    }
}