<?php
/**
 * Created by PhpStorm.
 * User: shesser
 * Date: 05.10.18
 * Time: 19:11
 */

namespace App\Services\ElasticSearch;

use App\Models\Concern\Elasticsearch\TradeItemView;
use App\Models\TradeItem;

class CreateIndexTradeItems
{
    public function execute()
    {
        // TradeItemView::createIndex($shards = null, $replicas = null);
        $data = TradeItemView::all();
        $data->addToIndex();
    }
}
