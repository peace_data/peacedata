<?php

namespace App\Services\Elasticsearch;

use App\Models\Concern\Elasticsearch\Measurement;

class CreateIndexMeasurements
{
    public function execute()
    {
        // Measurement::addAllToIndex();
        // Measurement::elastic_search:create_index_packages();
        Measurement::createIndex($shards = null, $replicas = null);
        // $data = Measurement::typeExists();
        // Measurement::putMapping($ignoreConflicts = true);


        $data = Measurement::all();
        $data->addToIndex();
    }
}