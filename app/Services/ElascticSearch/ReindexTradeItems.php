<?php
/**
 * Created by PhpStorm.
 * User: shesser
 * Date: 05.10.18
 * Time: 16:55
 */
namespace App\Services\ElasticSearch;

use App\Models\Concern\Elasticsearch\TradeItemView;
use App\Models\Concern\Elasticsearch\TradeItem;

class ReindexTradeItems
{
    private $before_execute = null;

    public function __construct()
    {
        $this->before_execute = $this->getInfo();
    }

    /**
     *
     */
    public function execute()
    {
        try {
            TradeItem::createIndex($shards = null, $replicas = null);
        } catch (\Exception $e) {
            TradeItem::deleteIndex();
            TradeItem::createIndex($shards = null, $replicas = null);
        } finally {
            TradeItem::chunk(1000, function($data){
                $data->addToIndex();
            });
        }
    }


    /**
     * @return array|null
     */
    public function getInfoBeforeIndexing()
    {
        return  $this->before_execute;
    }

    /**
     * @return array|null
     */
    public function getInfoAfterIndexing()
    {
        return $this->getInfo();
    }

    /**
     * @return array
     */
    private function getInfo()
    {
        return [
            'db' =>   TradeItem::all()->count(),
            // 'elastic' => TradeItem::searchByQuery()->totalHits()
        ];
    }
}