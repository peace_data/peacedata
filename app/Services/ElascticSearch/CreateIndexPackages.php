<?php

namespace App\Services\Elasticsearch;

use App\Models\Concern\Elasticsearch\Package;

class CreateIndexPackages
{
    public function execute()
    {
        Package::deleteIndex();
        Package::createIndex($shards = null, $replicas = null);

        // Package::rebuildMapping();
        Package::addAllToIndex();
    }
}