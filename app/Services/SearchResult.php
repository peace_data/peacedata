<?php

namespace App\Services;

use App\Models\TradeItem;
use App\Models\Product;
use App\Models\Views\TradeItemView;

class SearchResult
{
    private $hits;
    private $sorter;
    
    public function __construct($hits, $sorter){
        $this->hits = $this->filterHits((new \ArrayObject($hits))->getArrayCopy());
        $this->sorter = $sorter;
    }

    public function call($measurement, $quantity = 1){
        $bestChoice = (new $this->sorter($this->hits))->getBestChoice();
        if(isset($bestChoice)){ 
            $tradeItem = TradeItem::where('gtin', $bestChoice['_source']['gtin'])->first();
            $tmp = $tradeItem->toArray();
            if($tradeItem->getTradeItemView()){
                $tmp = array_merge($tmp, $tradeItem->getTradeItemView()->toArray());
            }
            $tmp['images'] = $tradeItem->images();
            $tmp['product_info'] = $tradeItem->products()->withminimalPrice()->with('images')->first();
            $tmp['quantity'] = $quantity;
            if($measurement){
                if(isset($tmp['compare_measurement']) && !is_null($tmp['compare_measurement'])){
                    $tmp['quantity'] = (int)($measurement->child_measurement_quantity * $quantity / (int)$tmp['compare_measurement']);
                }
            }
            return $tmp;
        }
    }

    private function filterHits($hits){
        if(!empty($hits)){
            $maxScore = max(array_column($hits, '_score'));
            $minScore = ($maxScore - $maxScore * 0.1);
            return array_filter($hits, function($item) use ($minScore){
                return $item['_score'] > $minScore;
            });
        }
        return [];
        
    }

}