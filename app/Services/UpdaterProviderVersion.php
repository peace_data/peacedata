<?php
/**
 * Created by PhpStorm.
 * User: shesser
 * Date: 25.10.18
 * Time: 11:51
 */

namespace App\Services;
use App\Models\Provider;
use App\Models\ProviderVersion;
use App\Models\Product;

class UpdaterProviderVersion
{

    private $provider = null;


    /**
     * UpdaterProviderVersion constructor.
     * @param Provider $provider
     */
    public function __construct(Provider $provider)
    {
       $this->provider = $provider;
    }

    /**
     * @param array $ids
     */
    public function execute(Array $ids)
    {
        if(count($ids) == 0){
            return ;
        }

        $provider_version = ProviderVersion::create([
            'provider_id' => $this->provider->id,
            'version' => $this->getCurrentVersion() + 1,
            'created_at' => \Carbon\Carbon::now()
        ]);

        Product::whereIn('id', $ids)->update(['provider_version_id' => $provider_version->id]);
    }

    /**
     * @return mixed
     */
    private function getCurrentVersion()
    {
        $provider_version = ProviderVersion::byProviderIdActual($this->provider->id)->first();

        return $provider_version->version;
    }
}