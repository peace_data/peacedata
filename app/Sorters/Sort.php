<?php

namespace App\Sorters;

use Illuminate\Http\Request;

class Sort
{

  protected $queryBuilder;
  protected $column;
  protected $direction;

  protected $sorters = [];

  public function __construct($queryBuilder, $request){
    $this->queryBuilder = $queryBuilder;
    $this->column = $request->get('sort_column');
    $this->direction = $request->get('sort_direction', 'asc');
  }


  public function call(){
    if(!empty($this->column)){
      return $this->queryBuilder->orderBy($this->column, $this->direction);
    }
    return $this->queryBuilder;
  }

}