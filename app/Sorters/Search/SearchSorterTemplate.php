<?php

namespace App\Sorters\Search;
use App\Models\TradeItem;
use App\Models\Product;

abstract class SearchSorterTemplate
{
    private $hits;
    private $bestChoice;
    private $tradeItemIds;
    
    public function __construct($hits){
        $this->hits = $hits;
        $this->bestChoice = $this->findBestChoice($hits);
    }

    public function getBestChoice(){
        return $this->bestChoice;
    }

    protected function findBestChoice($hits){
        throw new \Exception('in class SearchSorterTemplate not implented method findBestChoice');
    }

}