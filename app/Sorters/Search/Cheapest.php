<?php

namespace App\Sorters\Search;
use App\Models\TradeItem;
use App\Models\Product;
use App\Sorters\Search\SearchSorterTemplate;

class Cheapest extends SearchSorterTemplate
{
    protected function findBestChoice($hits){
        usort($hits, function ($item1, $item2) {
            return $item1['_source']['compare_price'] <=> $item2['_source']['compare_price'];
        });
        return $hits[0];
        // $tradeItemIds = array_map(function($hit){
        //     return $hit['_source']['gtin'];
        // }, $hits);

        // $minComparePrice = Product::whereIn('external_key', $tradeItemIds)->min('compare_price');
        // return TradeItem::join('products', function($join) use ($minComparePrice){
        //     $join->on('products.external_key', 'trade_items.gtin')
        //         ->where('products.compare_price', $minComparePrice);
        //     })->whereIn('trade_items.gtin', $tradeItemIds)
        //     ->first();

    }

}
