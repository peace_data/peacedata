<?php

namespace App\Sorters\TradeItem;

use Illuminate\Http\Request;
use App\Sorters\Sort;

class Gtin
{

  public function call($queryBuilder, $direction){
    return $queryBuilder->orderBy('trade_items.gtin', $direction);
  }

}