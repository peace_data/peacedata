<?php

namespace App\Sorters\TradeItem;

use Illuminate\Http\Request;
use App\Sorters\Sort;

class TradeItemSorter extends Sort
{

  protected $sorters = [
    'gtin' => Gtin::class,
  ];

  public function call(){
    if(!empty($this->column)){
      if(in_array($this->column, array_keys($this->sorters))){
        return (new $this->sorters[$this->column])->call($this->queryBuilder, $this->direction);
      }else{
        return $this->queryBuilder
          ->join('trade_items_view', 'trade_items.gtin', 'trade_items_view.gtin')
          ->orderBy($this->column, $this->direction);
      }
    }
    return $this->queryBuilder;
  }

}