<?php
/**
 * Created by PhpStorm.
 * User: shesser
 * Date: 25.08.18
 * Time: 12:45
 */

function get_list_models($path){
    $out = [];
    $results = scandir($path);
    foreach ($results as $result) {

        if ($result === '.' or $result === '..' or !strpos($result, '.php')) continue;
        $out[] = substr($result,0,-4);
    }

    return $out;
}

/**
 * @param bool $is_empty_relations
 * @return array
 */
function get_list_models_relations($show_empty_relations = false)
{
    $ns="/Models/Validoo/";
    $path = app_path() . $ns;
    $listModels = get_list_models($path);

    $ns = 'App'.$ns;

    $res = [];

    foreach($listModels as $model)
    {
        $model_name = str_replace('/','\\', $ns.$model) ;

        $item = new $model_name;
        $relations = $item->relationships();
        if(!empty($relations) || $show_empty_relations === TRUE){
            $res[$model] = $relations;
        }

    }

    return $res;
}
