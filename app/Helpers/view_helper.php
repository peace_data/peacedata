<?php

if (!function_exists('format_money')) {

    function format_money($value, $currency=null)
    {
        $currency = $currency==null ? '' : " ". $currency;
        if(is_int($value)) {
           $value =  round(((float) $value) / 100, 2) ;
           $value = sprintf("%01.2f", $value);
        }

        return $value.$currency;

    }
}


function show_boolean($val)
{
    if(is_bool($val) !== true){
        return 'UNKNOWN FORMAT in ' . __FUNCTION__ . 'of helper';
    }

    return $val ? 'Yes' : 'No';
}

function show_json($value)
{
    $arr = json_decode($value);

    if(!is_array($arr)) {
        return 'Unknown data';
    }

    $res = '';
    try{
        foreach($arr[0] as $key=>$value){
            $res .= $key. ' : ' . $value . PHP_EOL;
        }
    }catch(\ErrorException $e){

    }

    echo nl2br($res);
}