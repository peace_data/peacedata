<?php
/**
 * Created by PhpStorm.
 * User: shesser
 * Date: 04.10.18
 * Time: 11:39
 */
namespace App\Validoo\Services;

use File;
use App\Validoo\Handlers\Handler;

class HandlerAllFilesFromStorage
{
    private $validoo_sftp_store = null;
    private $validoo_storage = null;


    public function __construct($input_path=null)
    {
      $this->init_storages($input_path);
    }

    private function init_storages($input_path)
    {
        $this->validoo_sftp_store = $input_path === null ? config('validoo.stores.sftp_store_path') : $input_path;
        $this->validoo_storage = '/var/www/storage/app/validoo/';
    }

    public function execute()
    {   
        $counter = 0;

        foreach (scandir($this->validoo_sftp_store) as $fileName) {
            if($fileName != '.' && $fileName != '..'){
                $file = new \SplFileObject($this->validoo_sftp_store.$fileName, 'r');
                $destFilePath = $this->validoo_storage. $file->getFilename();
                File::move($file->getPathname(), $destFilePath);
                Handler::call($file->getFilename(), storage_path('app/validoo/'.$file->getFilename()));
                $counter++;
                echo($counter.PHP_EOL);
            }
            
        }
    }
}