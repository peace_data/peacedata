<?php
namespace App\Validoo\Handlers;
use File;
use Storage;
use Intervention\Image\ImageManager;
use App\Models\Validoo\Image as ValidooImage;
class Image
{
  private $gtin;
  private $itemFolderPath;
  private $manager;

  public function __construct($fileName){
    $this->itemFolderPath = $this->folderPath($fileName);
    $this->gtin = $this->gtin($fileName);
    $this->manager = new ImageManager(array('driver' => 'imagick'));
  }

  public static function parse($fileName, $filePath){

    $instance = new self($fileName);

    $imageAttributes = $instance->makeVersions($fileName, $filePath);
    $imageAttributes['gtin'] = $instance->gtin;
    if(!ValidooImage::where($imageAttributes)->exists()){
      ValidooImage::create($imageAttributes);
    }
  }

  private function makeVersions($fileName, $filePath){

    $image = $this->manager->make($filePath);

    $image->resize(50, 50, function ($constraint) {
        $constraint->aspectRatio();
    });

    $thumbImagePath = $this->itemFolderPath.'/thumb_'.$fileName;
    Storage::disk('public')->put($thumbImagePath, (string) $image->encode());

    $image = $this->manager->make($filePath);
    $image->resize(200, 300, function ($constraint) {
        $constraint->aspectRatio();
    });
    $mediumImagePath = $this->itemFolderPath.'/medium_'.$fileName;
    Storage::disk('public')->put($mediumImagePath, (string) $image->encode());
    File::move($filePath, Storage::disk('public')->getAdapter()->getPathPrefix().$this->itemFolderPath.'/'.$fileName);

    return [
      'src' => $this->itemFolderPath.'/'.$fileName,
      'thumb' => $thumbImagePath,
      'middle' => $mediumImagePath
    ];
  }

  private function gtin($fileName){
    $explodedFileName = explode('_', $fileName);
    return $explodedFileName[0];
  }

  private function folderPath($fileName){
    $gtin = $this->gtin($fileName);
    $folder = substr($gtin, 0, 6);
    return $folder.'/'.$gtin;
  }
}
