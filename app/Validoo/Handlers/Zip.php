<?php

namespace App\Validoo\Handlers;

use Chumper\Zipper\Zipper;
use Storage;
use File;
use App\Validoo\Handlers\Handler;


class Zip
{

  private $file;
  private $filePath;

  public function __construct($fileName, $filePath){
    $this->file = $fileName;
    $this->filePath = $filePath;
    $this->tmpFolderName = $this->tmpFolderNameByPath($filePath);
  }

  public static function parse($file, $filePath){
    $instance = new self($file, $filePath);
    $instance->extractFiles();
    foreach (File::files($instance->tmpFolderName) as $file) {
      Handler::call($file->getFilename(), $file->getPathname());
    }
    File::deleteDirectory($instance->tmpFolderName);
    File::delete($filePath);

  }
  private function extractFiles(){
    $zipper = new Zipper;
    $zipper->make($this->filePath)->extractTo($this->tmpFolderName);
  }

  private function tmpFolderNameByPath($filePath){
    $pathParts = pathinfo($filePath);
    return Storage::disk('validoo_tmp')->getAdapter()->getPathPrefix().$pathParts['filename'];
  }
}
