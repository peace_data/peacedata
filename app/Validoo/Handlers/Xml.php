<?php

namespace App\Validoo\Handlers;

use App\Validoo\Parsers\Xml\Validoo as ValidooXmlParser;
use App\Validoo\Archiv\Xml as ValidooXmlArchiv;

class Xml
{
  public static function parse($fileName, $filePath){
    $xml = new \SplFileObject($filePath, 'r');
    $xmlParser = new ValidooXmlParser($xml);
    $xmlParser->store();
    ValidooXmlArchiv::move($filePath, $fileName);
  }

}
