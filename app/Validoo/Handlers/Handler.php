<?php
namespace App\Validoo\Handlers;
use App\Validoo\Handlers\Image as ValidooImageHandler;
use App\Validoo\Handlers\Zip as ValidooZipHandler;
use App\Validoo\Handlers\Xml as ValidooXmlHandler;
use File;
use Log;


class Handler
{
  public static function call($fileName, $filePath){
    self::logRequest($fileName.'in '.$filePath.' start parse');
    try{
      switch (File::mimeType($filePath)) {
        case 'text/xml':
          ValidooXmlHandler::parse($fileName, $filePath);
          break;
        case 'application/zip':
          ValidooZipHandler::parse($fileName, $filePath);
          break;
        case 'image/jpeg':
          ValidooImageHandler::parse($fileName, $filePath);
          break;
        default:
          // TODO
          break;
      }
      self::logRequest($fileName.'in '.$filePath.' end parse');
    }catch(\Exception $e){
      self::logRequest($e);
    }


  }

  private static function logRequest($message){
    Log::channel('validoo')->info($message);
  }
}
