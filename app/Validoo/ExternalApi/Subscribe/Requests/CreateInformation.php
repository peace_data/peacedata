<?php
namespace App\Validoo\ExternalApi\Subscribe\Requests;

class CreateInformation extends Create
{
  protected $viewPath = 'admin.validoo.external_api.subscribe.create_information';
}
