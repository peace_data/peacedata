<?php
/**
 * Created by PhpStorm.
 * User: shesser
 * Date: 26.09.18
 * Time: 16:24
 */

namespace App\Validoo\ExternalApi\Subscribe\Requests;

use View;
use App\Validoo\ExternalApi\Subscribe\Requests\Request as SubscribeRequest;
use App\Validoo\ExternalApi\Subscribe\Parsers\Delete as Parser;


/*
 * TODO It is not ready
 */
class Delete extends SubscribeRequest
{

    public function __construct($subscribeId){
      $this->subscribeId = $subscribeId;
      parent::__construct();
    }
    public function call(){
      $response = $this->retrieveData();

      $subscribes = $this->digInArray($response, [
          'SearchSubscriptionsResponse',
          'SearchSubscriptionsResult',
          'aSubscriptions',
          'aSubscriptionType',
          'DeleteSubscriptionResult'
      ]);

      return array_map(function($item){
          return Parser::call($item);
      }, $subscribes);
    }

    protected function getAction(){
        return config('validoo.subscription.delete.soap_action');
    }

    protected function getBody(){
        return View::make('admin.validoo.external_api.subscribe.delete', [
          'subscribeId' => $this->subscribeId
          ])->render();
    }

}
