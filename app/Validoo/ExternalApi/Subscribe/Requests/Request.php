<?php
namespace App\Validoo\ExternalApi\Subscribe\Requests;

use View;
use Goutte\Client;

abstract class Request
{
  private $client;
  private $action;
  private $url;

  public function __construct(){
    $this->client = new Client();
    $this->password = getenv('VALIDOO_PASSWORD');
    $this->username = getenv('VALIDOO_USER_NAME');
    $this->entity_code = getenv('VALIDOO_ENTITY');
    $this->action = $this->getAction();
    $this->url = config('validoo.subscription.endpoint');
  }

  public function call(){
    throw new \Exception('Not implemented');
  }

  protected function getUrl(){
    throw new \Exception('Not implemented');
  }

  protected function getAction(){
    throw new \Exception('Not implemented');
  }

  protected function getBody(){
    throw new \Exception('Not implemented');
  }

  protected function digInArray($arr, $keys){
    $tmp = $arr;
    for($i = 0; $i < count($keys); $i++){
      if(!empty($tmp[$keys[$i]])){
        $tmp = $tmp[$keys[$i]];
      }
    }
    return $tmp;
  }

  protected function retrieveData(){
    $this->client->request('POST',
      $this->url,
      [],
      [],
      [
        'HTTP_CONTENT_TYPE' => 'text/xml; charset=utf-8',
        'HTTP_SOAPACTION' => $this->action
      ],
      $this->getBody()
    );
    $data = $this->parseResponseContent($this->client->getResponse()->getContent());
    return $data[0];
  }

  protected function parseResponseContent($content){
    $response = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $content);
    $xml = new \SimpleXMLElement($response);
    $body = $xml->xpath('//sBody');
    return json_decode(json_encode((array)$body), TRUE);
  }

}
