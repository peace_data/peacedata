<?php
namespace App\Validoo\ExternalApi\Subscribe\Requests;

class CreateImage extends Create
{
  public $viewPath = 'admin.validoo.external_api.subscribe.create_image';
}
