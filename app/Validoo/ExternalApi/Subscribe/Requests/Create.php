<?php
namespace App\Validoo\ExternalApi\Subscribe\Requests;

use View;
use Goutte\Client;
use App\Validoo\ExternalApi\Subscribe\Parsers\Create as CreateParser;
use App\Validoo\ExternalApi\Subscribe\Requests\Request as SubscribeRequest;
use App\Models\TradeItemSubscription;

abstract class Create extends SubscribeRequest
{
  private $gtin;

  public function __construct($gtin){
    $this->gtin = $gtin;
    parent::__construct();
  }

  public function call(){
    $response = $this->retrieveData();

    $subscribe = $this->digInArray($response, [
      'CreateSubscriptionResponse',
      'CreateSubscriptionResult'
    ]);
    $subscribe['gtin'] = $this->gtin;

    return CreateParser::call($subscribe);
  }

  protected function getAction(){
    return config('validoo.subscription.create.soap_action');
  }

  protected function getBody(){
    return View::make($this->viewPath, [
      'gtin' => $this->gtin,
      'pass' => $this->password,
      'username' => $this->username,
      'entity_code' => $this->entity_code
    ])
      ->render();
  }

}
