<?php
namespace App\Validoo\ExternalApi\Subscribe\Requests;

use View;
use Goutte\Client;
use App\Validoo\ExternalApi\Subscribe\Parsers\Search as SearchParser;
use App\Validoo\ExternalApi\Subscribe\Requests\Request as SubscribeRequest;

class Search extends SubscribeRequest
{

  public function call(){
    $response = $this->retrieveData();

    $subscribes = $this->digInArray($response, [
      'SearchSubscriptionsResponse',
      'SearchSubscriptionsResult',
      'aSubscriptions',
      'aSubscriptionType'
    ]);
    
    return array_map(function($item){
      return SearchParser::call($item);
    }, $subscribes);
  }

  protected function getAction(){
    return config('validoo.subscription.search.soap_action');
  }

  protected function getBody(){
    return View::make('admin.validoo.external_api.subscribe.search', [
      'pass' => $this->password,
      'username' => $this->username,
      'entity_code' => $this->entity_code
    ])->render();
  }

}
