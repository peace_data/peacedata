<?php
namespace App\Validoo\ExternalApi\Subscribe\Services;
use App\Validoo\ExternalApi\Subscribe\Requests\CreateImage;
use App\Validoo\ExternalApi\Subscribe\Requests\CreateInformation;
use App\Models\TradeItemSubscription;
use App\Models\TradeItem;
use Log;

class Create
{

  public static function call($gtin){
    self::handleByType('image', $gtin);
    self::handleByType('info', $gtin);
  }

  private static function handleByType($subscribeType, $gtin){
    $informationType = TradeItemSubscription::byType($subscribeType)->byGtin($gtin)->first();
    if(empty($informationType)){
      $subscribeResponse = self::requestByType($subscribeType, $gtin);
      if($subscribeResponse->success()){
        self::logRequest('subscribe for'.$subscribeResponse->gtin.' successfully created');

        $subscribeResponseArr = $subscribeResponse->toArray();
        $subscribeResponseArr['type'] = $subscribeType;
        if(TradeItemSubscription::create($subscribeResponseArr)){
          TradeItem::findOrCreateByGtin($gtin);
          self::logRequest('trade item subscription successfully created');
        }else{
          self::logRequest('--------------------Invalid trade item subscride store--------------------------');
          self::logRequest('trade item subscription for '.$subscribeResponse->gtin.' failed');
          self::logRequest('--------------------End Invalid trade item subscride store----------------------');
        }
      }else{
        if($subscribeResponse->isDuplicate()){
          $subscribeResponse->setDuplicateSubscribeId();
          $subscribeResponseArr = $subscribeResponse->toArray();
          $subscribeResponseArr['type'] = $subscribeType;
          TradeItemSubscription::create($subscribeResponseArr);
        }
        self::logRequest('--------------------Invalid subscribe request--------------------------');
        self::logRequest('response status code: '.$subscribeResponse->getStatusCode());
        self::logRequest('response gtin: '.$subscribeResponse->gtin);
        self::logRequest('response error message: '.$subscribeResponse->aErrorMessage);
        self::logRequest('request type: '.$subscribeType);
        self::logRequest('--------------------End Invalid subscribe request----------------------');
      }
    }else{
      self::logRequest('subscribe for '.$gtin.' already exists'.$subscribeType);
    }
  }

  private static function requestByType($type, $gtin){
    switch ($type) {
      case 'info':
        return (new CreateInformation($gtin))->call();
        break;
      case 'image':
        return (new CreateImage($gtin))->call();
        break;
      default:
        throw new \Exception("Wrong type for subscribe", 1);
        break;
    }
  }

  private static function logRequest($message){
    Log::channel('validoo_subscribe')->info($message);
  }

}
