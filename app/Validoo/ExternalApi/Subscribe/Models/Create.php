<?php
namespace App\Validoo\ExternalApi\Subscribe\Models;

use View;
use Goutte\Client;

class Create
{
  const DUPLICATE_STATUS_CODE = 3105;
  public $gtin;
  public $aErrorMessage;
  public $aStatusCode;
  public $aSubscriptionId;

  public function toArray(){
    return [
      'gtin' => $this->gtin,
      'message' => $this->aErrorMessage,
      'status_code' => $this->getStatusCode(),
      'subscribe_id' => $this->aSubscriptionId
    ];
  }

  public function success(){
    return $this->aErrorMessage == 'OK';
  }

  public function getStatusCode(){
    if(is_array($this->aStatusCode)){
      return '';
    }
    return $this->aStatusCode;
  }

  public function isDuplicate(){
    return $this->aStatusCode == self::DUPLICATE_STATUS_CODE;
  }

  public function setDuplicateSubscribeId(){
    $matches=[];
    preg_match('/[\d]{7,9}\\040/', $this->aErrorMessage, $matches);
    if(!empty($matches)){
      $this->aSubscriptionId = trim($matches[0]);
    }
    return $this->aStatusCode;
  }
}
