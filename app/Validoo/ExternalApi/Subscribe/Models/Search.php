<?php
namespace App\Validoo\ExternalApi\Subscribe\Models;

use View;
use Goutte\Client;

class Search
{
  public $aDownloadAlternativeFilename;
  public $aDownloadFieldOfApplication;
  public $aDownloadFileFormat;
  public $aDownloadIncludeAllTradeItemInformation;
  public $aNotificationSubscriptionSetup;
  public $aSearchBrandName;
  public $aSearchFaceIndicator;
  public $aSearchFileNature;
  public $aSearchGLN;
  public $aSearchGPC;
  public $aSearchGTIN;
  public $aSearchSupplierName;
  public $aSearchTargetMarket;
  public $aSubcriptionContentType;
  public $aSubscriptionId;
  public $aSubscriptionName;

  public function toArray(){
    return get_object_vars($this);
  }
}
