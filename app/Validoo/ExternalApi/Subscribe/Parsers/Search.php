<?php
namespace App\Validoo\ExternalApi\Subscribe\Parsers;

use View;
use Goutte\Client;
use  App\Validoo\ExternalApi\Subscribe\Models\Search as SearchModel;

class Search
{

  public static function call($arr){
    $searchModel = new SearchModel();
    $searchModel->aDownloadAlternativeFilename = $arr['aDownloadAlternativeFilename'];
    $searchModel->aDownloadFieldOfApplication = $arr['aDownloadFieldOfApplication'];
    $searchModel->aNotificationSubscriptionSetup = $arr['aNotificationSubscriptionSetup'];
    $searchModel->aSearchBrandName = $arr['aSearchBrandName'];
    $searchModel->aSearchFaceIndicator = $arr['aSearchFaceIndicator'];
    $searchModel->aSearchFileNature = $arr['aSearchFileNature'];
    $searchModel->aSearchGLN = $arr['aSearchGLN'];
    $searchModel->aSearchGPC = $arr['aSearchGPC'];
    $searchModel->aSearchGTIN = $arr['aSearchGTIN'];
    $searchModel->aSearchSupplierName = $arr['aSearchSupplierName'];
    $searchModel->aSearchTargetMarket = $arr['aSearchTargetMarket'];
    $searchModel->aSubcriptionContentType = $arr['aSubcriptionContentType'];
    $searchModel->aSubscriptionId = $arr['aSubscriptionId'];
    $searchModel->aSubscriptionName = $arr['aSubscriptionName'];
    return $searchModel;
  }

}
