<?php
namespace App\Validoo\ExternalApi\Subscribe\Parsers;

use View;
use Goutte\Client;
use  App\Validoo\ExternalApi\Subscribe\Models\Create as CreateModel;

class Create
{

  public static function call($arr){
    $searchModel = new CreateModel();
    $searchModel->gtin = $arr['gtin'];
    $searchModel->aErrorMessage = $arr['aErrorMessage'];
    $searchModel->aStatusCode = $arr['aStatusCode'];
    $searchModel->aSubscriptionId = $arr['aSubscriptionId'];
    return $searchModel;
  }

}
