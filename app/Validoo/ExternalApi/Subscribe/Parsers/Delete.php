<?php
namespace App\Validoo\ExternalApi\Subscribe\Parsers;

use View;
use Goutte\Client;
use  App\Validoo\ExternalApi\Subscribe\Models\Delete as DeleteModel;

/*
 * TODO It is not ready
 */
class Delete
{

    public static function call($arr){
        $deleteModel = new DeleteModel();
        $deleteModel->aErrorMessage = $arr['DeleteSubscriptionResult']['aErrorMessage'];

        return $deleteModel;
    }

}
