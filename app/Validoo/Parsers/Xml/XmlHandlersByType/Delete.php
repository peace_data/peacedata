<?php

namespace App\Validoo\Parsers\Xml\XmlHandlersByType;

use App\Validoo\Parsers\Xml\Validoo as ValidooXmlParser;
use App\Models\TradeItem;
use App\Models\Concern\Elasticsearch\TradeItemView;

class Delete
{

  const GTIN_XPATH = '//catalogue_item_hierarchical_withdrawal:catalogueItemHierarchicalWithdrawal/catalogueItemReference/gtin';

  public static function handle($xml){
    $gtin = (string)$xml->xpath(self::GTIN_XPATH)[0];
    $generalTradeItem = TradeItem::where('gtin', $gtin)->first();
    if($generalTradeItem){
      $validooTradeItem = $generalTradeItem->tradeItem;
      if($validooTradeItem){
        $elasticSearchView = TradeItemView::where('gtin', $gtin)->first();
        $elasticSearchView->removeFromIndex();
        $validooTradeItem->deleted = true;
        $validooTradeItem->save();

        echo 'trade item with gtin: '. $gtin .' deleted set as true';
      }
    }else{
      echo 'trade item with gtin: '. $gtin .' not founded';
    }
  }

}
