<?php

namespace App\Validoo\Parsers\Xml\XmlHandlersByType;

use App\Models\Validoo\References\XPath;
use App\Builders\Validoo\XmlTree;
use App\Validoo\Parsers\Validoo as ValidooParser;
use App\Validoo\Parsers\Xml\HandlerByType;
use App\Models\Validoo\TradeItem;
use App\Models\TradeItem as PlainTradeItem;
use App\Models\Concern\Elasticsearch\TradeItemView;
use DB;
class Add
{

  private $tree;
  private $xml;

  private function __construct($xml){
    $this->xml = $xml;

    $parser = new ValidooParser($this->xml);
    $this->tree = new XmlTree($parser->parse());
  }

  public static function handle($xml){
    $ns = $xml->getNamespaces(true);
    foreach ($ns as $nameSpace => $url) {
      $xml->registerXPathNamespace($nameSpace, $url);
    }
    $gtin = (string)$xml->xpath('//catalogue_item_notification:catalogueItemNotification/catalogueItem/tradeItem/gtin')[0];
    $validooTradeItem = TradeItem::findByGtin($gtin);

    if($validooTradeItem){
      PlainTradeItem::findOrCreateByGtin($gtin)->delete();
      $validooTradeItem->delete();
    }    
    
    $instance = new self($xml);
  
    $tradeItem = TradeItem::findOrCreateByGtin($instance->tree->gtin());
    foreach($instance->tree->getTree() as $modelName => $node){
      $instance->storeInDb($modelName, $node, $tradeItem);
    }
    PlainTradeItem::findOrCreateByTradeItem($tradeItem);
    $tradeItemElasticView = TradeItemView::findByGtin($instance->tree->gtin());
    if($tradeItemElasticView){
      $tradeItemElasticView->addToIndex();
    }
    
  }

  private function storeInDb($modelName, $node, $parent){
    if(!$node) return;
    $childClasses = array_filter($node, function($item){
      if(is_array($item)){
        return is_array($item);
      }
    });
    try{
      if(!$this->isAssocArr($node)){
        
        foreach($node as $nodeEl){
       
           $childClasses = array_filter($nodeEl, function($item){
            if(is_array($item)){
              return is_array($item);
            }
          });
          $this->saveModel($nodeEl, $parent, $modelName, $childClasses);  

        }
      }else{
        $this->saveModel(array_diff_key($node, $childClasses), $parent, $modelName, $childClasses);
    }
    }catch(QueryException $e){
      var_dump($e);
    }catch(\Exception $e){
      echo($modelName.PHP_EOL);
      var_dump($e->getMessage());
    }

  }

  private function saveModel($data, $parent, $modelName, $childClasses){
      if(empty($modelName)){
        return;
      }
      if(strlen($modelName)< 5){
        return;
      }
      try{
        $model = new $modelName;
        foreach($model->relationships() as $name => $value){
          unset($data[$value['model']]);
        }
        $model->fill($data);
        
        $relationName = $this->findRelationName($parent, $modelName);
        
        if(!$relationName){
          $model->save();
        }else{
          $parent->$relationName()->save($model);
        }
        if($this->isAssocArr($childClasses)){
          foreach ($childClasses as $childClassName => $childClass) {

            if(!empty($childClass)){
              $this->storeInDb($childClassName, $childClass, $model);
            }
          }
        }
      }catch(\FatalThrowableError $e){
        var_dump($e);
      }
      
  }

  private function findRelationName($parent, $modelName){
    foreach($parent->relationships() as $relation => $data) {
      if($data['model'] == $modelName) return $relation;
    }
  }

  private function isAssocArr($arr){
    return array_keys($arr) !== range(0, count($arr) - 1);
  }

}
