<?php

namespace App\Validoo\Parsers\Xml\XmlHandlersByType;

use App\Validoo\Parsers\Xml\Validoo as ValidooXmlParser;
use Carbon\Carbon;
use Storage;
use Mail;
use File;

class Update
{
  public static function handle($xml, $file){
    $filename = $file->getFilename();
    File::move($file->getPathname(), Storage::disk('validoo_unhandled')->getAdapter()->getPathPrefix().'/'.$filename);

    // Storage::disk('validoo_unhandled')->move($file->getPathname(), $file);
    Mail::send( 'mailers.validoo.unhandled', ['filename' => $filename], function( $message ) use ($filename)
    {
        $message->to( '1989alpan@gmail.com' )->subject( 'Unhandled!' );
    });
  }

}
