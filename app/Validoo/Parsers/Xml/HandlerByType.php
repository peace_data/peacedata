<?php
/**
 * Created by PhpStorm.
 * User: shesser
 * Date: 21.08.18
 * Time: 11:15
 */
namespace App\Validoo\Parsers\Xml;
use App\Validoo\Parsers\Xml\XmlHandlersByType\Add;
use App\Validoo\Parsers\Xml\XmlHandlersByType\Delete;
use App\Validoo\Parsers\Xml\XmlHandlersByType\Update;

class HandlerByType
{
    const COMMAND_TYPE_XPATH = '//transaction/documentCommand/documentCommandHeader/@type';

    public function call($xml, $file){
        switch($this->xmlType($xml)){
            case 'ADD': 
                Add::handle($xml);
                break;
            case 'DELETE': 
                Delete::handle($xml);
                break;
            case 'CHANGE_BY_REFRESH': 
                Update::handle($xml,$file);
                break;
            case 'CORRECT': 
                // throw('this type is not implemented yet'); 
                Update::handle($xml,$file);
                break;
            default: 
                throw('Unsuported type of xml parser type in HandlerByType file'); 
                break;
        }
    }
    private function xmlType($xml){
        return (string)$xml->xpath(self::COMMAND_TYPE_XPATH)[0]['type'];
    }

}