<?php
/**
 * Created by PhpStorm.
 * User: shesser
 * Date: 21.08.18
 * Time: 11:15
 */
namespace App\Validoo\Parsers\Xml;
use App\Validoo\Parsers\Xml\HandlerByType;

class Validoo
{
  private $file;
  private $xml;

  public function __construct($file){
    $this->xml = simplexml_load_string($file->fread($file->getSize()));
    $this->file = $file;
  }

  public function store(){
    
    $handler = new HandlerByType;
    $handler->call($this->xml, $this->file);
  }
}
