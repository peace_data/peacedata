<?php
/**
 * Created by PhpStorm.
 * User: shesser
 * Date: 21.08.18
 * Time: 11:15
 */
namespace App\Validoo\Parsers;
use App\Models\Validoo\References\XPath;
use DB;
class Validoo
{
    private $xml;

    public function __construct($xml)
    {
        $this->xml = $xml;
        $this->xpaths = XPath::groupedByModel();
    }

    public function parse(){

      $res = [];
      $extensions = $this->xml->xpath('.//extension');
      $lastExtension = end($extensions);
      $ns = $lastExtension->getNamespaces(true);
      foreach ($ns as $nameSpace => $url) {
        $lastExtension->registerXPathNamespace($nameSpace, $url);
      }

      foreach ($this->xpaths as $modelName => $rows) {
        $res[$modelName] = [];
        $record = [];
        try{
          $parent = $this->findParentNode($rows->first()->x_path, $lastExtension);
          
          if(count($parent) > 1){
            $record = $this->parseParent($parent, $rows, $modelName);
          }else{
            $record = $this->parseSingleNode($rows, $lastExtension);
            
          }
          
        }catch(\Exception $e){
          // echo($e);
        }
        
        $res[$modelName] = $record;

      }
      return $res;
    }

    private function findNode($x_path, $node){
      try{
        $outNode = $node->xpath($x_path);
        if(empty($outNode)){
          $outNode = $this->xml->xpath('//'.$x_path);
        }
        return $outNode;
      } catch (\ErrorException $e) {
        return $this->xml->xpath('//'.$x_path);
      }
    }

    private function findDataInNode($row, $node){
      $res = '';
      if(empty($node)) return $res;
    
      if($row->isSingle()){
        if($node[0]->children() && $node[0]->children()->count()){
          $res = json_encode($node);
        }elseif(!empty($node[0]->attributes())){
          $modelClass = 'App\Models\Validoo\\'.$row->model_name;
          $columnType = DB::getSchemaBuilder()->getColumnType((new $modelClass)->getTable(), $row->field_name);
          if($columnType == 'json'){
            $res = json_encode($node[0]);
          }else{
             $res = (string)$node[0];
          }
        }
        else{
          $res = (string)$node[0];
        }
      }else{
        
        $res = json_encode($node);
      }
      return $res;
    }

    private function findParentNode($x_path, $lastExtension){
      $a = explode('/', $x_path);
      $b = array_slice($a, 0, -1);
      
      $c = implode("/", $b);
      return $this->findNode($c, $lastExtension);
    }

    private function camelize($str, $separator = '_'){
      $res = str_replace($separator, '', ucwords($str, $separator));
      return lcfirst($res);
    }

    private function parseParent($parent, $rows, $modelName = null){
      $res = [];
      foreach($parent as $node){
        $tmp = [];

        foreach($rows as $row){

          $fieldNameInCamelCase = $this->camelize($row->field_name);
          $fieldData = $this->findDataInNode($row, $node->$fieldNameInCamelCase);
          if($fieldData){
            $tmp[$row->field_name] = $fieldData;
          }

        }

        if(!empty($modelName)){
          $fullClassName = 'App\Models\Validoo\\'.$modelName;
          $class = new $fullClassName();

          foreach($class->relationships() as $name => $value){
            $relationClassPath = explode('\\',$value['model']);
            $tmp[$value['model']] = $this->parseParent($node, $this->xpaths[end($relationClassPath)]);
          }
        }
        
        if(!empty($tmp)){
          array_push($res, $tmp);
        }
      }
      return $res;
    }

    private function parseSingleNode($rows, $lastExtension){
      $record = [];
      foreach ($rows as $row) {
          
        try{
          $node = $this->findNode($row->x_path, $lastExtension);
          $fieldData = $this->findDataInNode($row, $node);

          if($fieldData){
            $record[$row->field_name] = $fieldData;
          }

        } catch (\ErrorException $e) {
            // echo 'For '.$modelName.' was not found node '.$row->x_path, "\n";
        }
      }
      return $record;
    }
}
