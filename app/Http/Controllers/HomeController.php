<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Guzzle\Stream\PhpStreamRequestFactory;

//use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\ClientInterface as GuzzleClientInterface;
use App\Jobs\Validoo\CreateSubscriptionJob;
use App\Models\TradeItem;
use App\Models\Views\TradeItemView;
use App\Models\Validoo\Packaging;
use App\Models\Concern\Elasticsearch\Package;
use App\Models\References\Package as PackageRef;
use App\Models\Product;
// use App\Models\Views\TradeItemView;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('index');
    }

    public function debug()
    {
        $r = Package::first()->aliases;
        $ref = PackageRef::first()->aliases;
        $instanse = TradeItem::find(1);

        $xml_post_string = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sap="http://www.saphety.com" xmlns:med="http://www.gs1services.net/MediaStore">
                            <soapenv:Header/>
                            <soapenv:Body>
                              <sap:SearchSubscriptions>
                                 <sap:response>
                                    <med:Password>ІрфкувМфдшвщщ2018</med:Password>
                                    <med:Username>1989alpan@gmail.com</med:Username>
                                    <med:EntityCode>7381020800003</med:EntityCode>
                                    <med:NumberOfResults>100</med:NumberOfResults>
                                    <med:Page>1</med:Page>
                                 </sap:response>
                              </sap:SearchSubscriptions>
                            </soapenv:Body>
                            </soapenv:Envelope>';

        $client = new Client(['timeout' => 20, 'verify' => true, 'headers' => [
            'Content-Type' => 'application/soap+xml',
        ]]);

        $soapUrl = "https://www.holidaywebservice.com//HolidayService_v2/HolidayService2.asmx?wsdl";


        $res = $client->post( $soapUrl, [$xml_post_string])->send();

        //        $dataFromTheForm = $_POST['fieldName']; // request data from the form
        $soapUrl = "https://validoomediastore-ctst.saphety.com/SyncMediaWS/SyncMediaSubscriptionWS.svc"; // asmx URL of WSDL
//        $soapUser = "username";  //  username
//        $soapPassword = "password"; // password

        // xml post structure

        $xml_post_string = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sap="http://www.saphety.com" xmlns:med="http://www.gs1services.net/MediaStore">
                               <soapenv:Header/>
                               <soapenv:Body>
                                  <sap:SearchSubscriptions>
                                     <sap:response>
                                        <med:Password>SharedValidoo2018</med:Password>
                                        <med:Username>1989alpan@gmail.com</med:Username>
                                        <med:EntityCode>7381020800003</med:EntityCode>
                                        <med:NumberOfResults>100</med:NumberOfResults>
                                        <med:Page>1</med:Page>
                                     </sap:response>
                                  </sap:SearchSubscriptions>
                               </soapenv:Body>
                            </soapenv:Envelope>';   // data from the form, e.g. some ID number

           $headers = array(
               "Content-type: aplication/soap+xml;charset=\"utf-8\"",
               "Accept: text/xml",
               "Cache-Control: no-cache",
               "Pragma: no-cache",
               "SOAPAction: https://validoomediastore-ctst.saphety.com/SyncMediaWS/SyncMediaSubscriptionWS.svc",
               "Content-length: ".strlen($xml_post_string)); //SOAPAction: your op URL

            $url = $soapUrl;

            // PHP cURL  for https connection with auth
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            // converting
            $response = curl_exec($ch);

        var_dump($response);
            curl_close($ch);


            // converting
            $response1 = str_replace("<soap:Body>","",$response);
            $response2 = str_replace("</soap:Body>","",$response1);

            // convertingc to XML
            $parser = simplexml_load_string($response2);
        var_dump($parser);
    }
}
