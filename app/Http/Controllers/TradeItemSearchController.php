<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Concern\Elasticsearch\TradeItemView;
use App\Models\Concern\Elasticsearch\TradeItem;
use App\Services\SearchResult;
use App\Sorters\Search\Cheapest;
use App\Sorters\Search\Healthiest;
use App\Models\Views\TradeItemView as TRView;
use App\Models\Validoo\SalesInformation;
use App\Models\Concern\Elasticsearch\Measurement;
use App\Builders\Search\QueryBuilder;


class TradeItemSearchController extends Controller
{

    public function byQuery(Request $request){
        return TradeItem::findByRequest(QueryBuilder::call($request->search_name));
        
    }

    public function list(Request $request){
        
        $res = TradeItem::findByRequest(QueryBuilder::call($request->search_name, $this->findMeasurement($request->search_name), $request->quantity));
        return [
            'healthiest' => (new SearchResult($res->getHits()['hits'], Healthiest::class))->call($this->findMeasurement($request->search_name), $request->quantity),
            'chipiest' => (new SearchResult($res->getHits()['hits'], Cheapest::class))->call($this->findMeasurement($request->search_name), $request->quantity)
        ];
    }

    private function findMeasurement($queryPhrase){
        $measurement = Measurement::searchByQuery(array(
            'multi_match' => array(
                'query' => $queryPhrase,
                "type" => "most_fields",
                'fuzziness' => "AUTO",
                'fields' => ['title^1.5']
            )
            )
        );
        if(!empty($measurement[0])){
            return $measurement[0];
        }
    }
}
