<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Models\TradeItem;
use App\Http\Requests\ValidooItemAttributeRequest;

class ValidooItemAttributeLevelsController extends Controller
{
    public function index(Request $request)
    {
      $validooItems = TradeItem::with('validooItemAttributtes')
          ->sort($request)
          ->paginate(25);

      if($request->ajax()){
        return response()->json(
            $validooItems->map(function($item){
              $res = $item->toArray();
              $res['category'] = $item->category_name;
              $res['brandName'] = $item->brand_owner;
              $res['productName'] = $item->product_name;
          return $res;
        }));
      }
      return view('admin.product_attribute_levels.index', compact('validooItems'));
    }

    public function store($validooItemId, ValidooItemAttributeRequest $request)
    {
      $validooItem = TradeItem::find($validooItemId);
      $attribute = $validooItem->validooItemAttributtes()
        ->firstOrCreate(['attribute_name' => $request->attribute_name]);
      $attribute->update(['value' => $request->value]);

      return response()->json($attribute, 200);
    }

}
