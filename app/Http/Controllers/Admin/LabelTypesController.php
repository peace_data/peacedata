<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\LabelType;
use Illuminate\Http\Request;

class LabelTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $labeltypes = LabelType::where('title', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $labeltypes = LabelType::latest()->paginate($perPage);
        }

        return view('admin.label-types.index', compact('labeltypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.label-types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        LabelType::create($requestData);

        return redirect('admin/label-types')->with('flash_message', 'LabelType added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $labeltype = LabelType::findOrFail($id);

        return view('admin.label-types.show', compact('labeltype'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $labeltype = LabelType::findOrFail($id);

        return view('admin.label-types.edit', compact('labeltype'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $labeltype = LabelType::findOrFail($id);
        $labeltype->update($requestData);

        return redirect('admin/label-types')->with('flash_message', 'LabelType updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        LabelType::destroy($id);

        return redirect('admin/label-types')->with('flash_message', 'LabelType deleted!');
    }
}
