<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\LabelType;
use App\Models\Label;
use Illuminate\Http\Request;

class LabelsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $labels = Label::where('title', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $labels = Label::latest()->paginate($perPage);
        }

        if($request->ajax()){
            return response()->json($labels);
        }
        $labelTypes = LabelType::all();
        return view('admin.labels.index', compact(['labels', 'labelTypes']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $label = new Label;
        $labelTypes = LabelType::all();
        return view('admin.labels.create', compact(['labelTypes', 'label']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $requestData = $request->all();

        Label::create($requestData);

        return redirect('admin/labels')->with('flash_message', 'Label added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $label = Label::findOrFail($id);

        return view('admin.labels.show', compact('label'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $label = Label::findOrFail($id);
        $labelTypes = LabelType::all();

        return view('admin.labels.edit', compact(['label', 'labelTypes']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();

        $label = Label::findOrFail($id);
        $label->update($requestData);

        return redirect('admin/labels')->with('flash_message', 'Label updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Label::destroy($id);

        return redirect('admin/labels')->with('flash_message', 'Label deleted!');
    }
}
