<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\AdditionalClimateScore;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Builders\AdditionalClimateScore as AdditionalClimateScoreBuilder;
use Validator;
use App\Http\Requests\XlsxImportRequest;

class AdditionalClimateScoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $additionalclimatescores = AdditionalClimateScore
                ::orWhere('global_name', 'LIKE', "%$keyword%")
                ->orWhere('region_name', 'LIKE', "%$keyword%")
                ->orWhere('sub_region_name', 'LIKE', "%$keyword%")
                ->orWhere('intermediate_region_name', 'LIKE', "%$keyword%")
                ->orWhere('country_or_area', 'LIKE', "%$keyword%")
                ->orWhere('iso_alpha3_code', 'LIKE', "%$keyword%");
        } else {
            $additionalclimatescores = AdditionalClimateScore::query();
        }

        $additionalclimatescores = $additionalclimatescores->sort($request);
        $additionalclimatescores = $additionalclimatescores->paginate($perPage);

        return view('admin.additional-climate-score.index', compact('additionalclimatescores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.additional-climate-score.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();

        AdditionalClimateScore::create($requestData);

        return redirect('admin/additional-climate-scores')->with('flash_message', 'AdditionalClimateScore added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $additionalclimatescore = AdditionalClimateScore::findOrFail($id);

        return view('admin.additional-climate-score.show', compact('additionalclimatescore'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $additionalclimatescore = AdditionalClimateScore::findOrFail($id);

        return view('admin.additional-climate-score.edit', compact('additionalclimatescore'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();

        $additionalclimatescore = AdditionalClimateScore::findOrFail($id);
        $additionalclimatescore->update($requestData);

        return redirect('admin/additional-climate-scores')->with('flash_message', 'AdditionalClimateScore updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        AdditionalClimateScore::destroy($id);

        return redirect('admin/additional-climate-scores')->with('flash_message', 'AdditionalClimateScore deleted!');
    }

    public function upload(XlsxImportRequest $request){
      $file = $request->file('xlsx_file');
      $file->move(storage_path('imports'), 'import.' . $file-> getClientOriginalExtension());
      Excel::load(storage_path('imports/import.xlsx'), function ($reader) {
          $reader->each(function ($row) {
            $parsedData = (new AdditionalClimateScoreBuilder)->fromXlsx($row->toArray());
            $validator = Validator::make($parsedData, [
                'm49_code' => 'required|unique:additional_climate_scores',
                'iso_alpha3_code' => 'required|unique:additional_climate_scores',
            ]);
            if(!$validator->fails()){
              AdditionalClimateScore::create($parsedData);
            }
          });
      });

      return redirect()->route('additional-climate-scores.index')
        ->with('flash_message', 'AdditionalClimateScore updated!');
    }
}
