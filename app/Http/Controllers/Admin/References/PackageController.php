<?php

namespace App\Http\Controllers\Admin\References;

use App\Http\Requests\XlsxImportRequest;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Validator;

use App\Models\References\Package;
use App\Models\Alias;
use App\Builders\Package as PackageBuilder;
use DB;
use Illuminate\Http\Request;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;
        if (!empty($keyword)) {
            $references_package = Package
                ::where('title', 'LIKE', "%$keyword%")
                ->orWhere('code', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%");
        } else {
            $references_package = Package::query();
        }

        $references_package = $references_package->sort($request);
        $references_package = $references_package->paginate($perPage);
        return view('admin.references.package.index', compact('references_package'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.references.package.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Package::create($requestData);

        return redirect('admin/references/package')->with('flash_message', 'References/Package added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $references_package = Package::findOrFail($id);

        return view('admin.references.package.show', compact('references/package'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $references_package = Package::findOrFail($id);

        return view('admin.references.package.edit', compact('references/package'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $references_package = Package::findOrFail($id);
        $references_package->update($requestData);

        return redirect('admin/references/package')->with('flash_message', 'References/Package updated!');
    }
    
    public function upload(XlsxImportRequest $request){
        $path = $request->file('xlsx_file')->getRealPath();
        $data_csv = Excel::load($path, function($reader) {})->get()->toArray();
        Package::truncate();
        foreach($data_csv as $row) {
            $builder = new PackageBuilder();
            $parsedData = $builder->fromCsv($row);
            $validator = Validator::make($parsedData, [
                'code' => 'required|unique:references_packages',
            ]);
            if(!$validator->fails()){
                $package = Package::create($parsedData);
                foreach($parsedData['aliases'] as $alias){
                    $alias = Alias::firstOrCreate(['title' => $alias]);
                    if($alias->title){
                        $package->aliases()->attach($alias);
                    }
                    
                }
            }
        }

        return redirect()->route('admin.references.package.index');
      }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Package::destroy($id);

        return redirect('admin/references/package')->with('flash_message', 'References/Package deleted!');
    }
}
