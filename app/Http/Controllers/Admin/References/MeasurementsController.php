<?php

namespace App\Http\Controllers\Admin\References;

use App\Http\Requests;
use App\Http\Requests\XlsxImportRequest;
use Maatwebsite\Excel\Facades\Excel;
use Validator;


use App\Http\Controllers\Controller;

use App\Models\References\Measurement;
use App\Builders\Measurement as Builder;

use Illuminate\Http\Request;

class MeasurementsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $measurements = Measurement::where('title', 'LIKE', "%$keyword%")
                ->orWhere('code', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%");
        } else {
            $measurements = Measurement::query();
        }

        $measurements = $measurements->sort($request);
        $measurements = $measurements->paginate($perPage);
        
        return view('admin/references.measurements.index', compact('measurements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin/references.measurements.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Measurement::create($requestData);

        return redirect('admin/references/measurements')->with('flash_message', 'Measurement added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $measurement = Measurement::findOrFail($id);

        return view('admin/references.measurements.show', compact('measurement'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $measurement = Measurement::findOrFail($id);

        return view('admin/references.measurements.edit', compact('measurement'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $measurement = Measurement::findOrFail($id);
        $measurement->update($requestData);

        return redirect('admin/references/measurements')->with('flash_message', 'Measurement updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Measurement::destroy($id);

        return redirect('admin/references/measurements')->with('flash_message', 'Measurement deleted!');
    }

    public function upload(XlsxImportRequest $request){
        
        $path = $request->file('xlsx_file')->getRealPath();
        $data_csv = Excel::load($path, function($reader) {})->get()->toArray();
        Measurement::truncate();
        foreach($data_csv as $row) {
            $builder = new Builder();
            $parsedData = $builder->fromCsv($row);
            $validator = Validator::make($parsedData, [
                'code' => 'required|unique:references_measurements',
            ]);
            if(!$validator->fails()){
                Measurement::create($parsedData);
            }
        }

        return redirect('admin/references/measurements');
      }
}
