<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProviderRequest;
use App\Http\Controllers\Controller;

use App\Models\Provider;
use Illuminate\Http\Request;
use App\Sorters\Sort;


class ProviderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $providers = Provider::where('title', 'LIKE', "%$keyword%")
                ->orWhere('url', 'LIKE', "%$keyword%");
        } else {
            $providers = Provider::query();
        }
        $providers = $providers->sort($request);

        $providers = $providers->paginate($perPage);

        return view('admin.providers.index', compact('providers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.providers.create');
    }

    /**
     * @param ProviderRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ProviderRequest $request)
    {

        $requestData = $request->all();

        Provider::create($requestData);

        return redirect('admin/providers')->with('flash_message', 'Provider added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $provider = Provider::findOrFail($id);

        return view('admin.providers.show', compact('provider'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $provider = Provider::findOrFail($id);

        return view('admin.providers.edit', compact('provider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(ProviderRequest $request, $id)
    {

        $requestData = $request->all();

        $provider = Provider::findOrFail($id);
        $provider->update($requestData);

        return redirect('admin/providers')->with('flash_message', 'Provider updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Provider::destroy($id);

        return redirect('admin/providers')->with('flash_message', 'Provider deleted!');
    }
}
