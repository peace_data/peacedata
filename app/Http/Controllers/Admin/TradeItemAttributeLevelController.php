<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TradeItem;

class TradeItemAttributeLevelController extends Controller
{
  public function index(Request $request){
      $tradeItems = TradeItem::with('labels')
          ->sort($request)
          ->paginate(25);
      if($request->ajax()){
        return response()->json($tradeItems->map(function($item){
          $res = $item->toArray();
          $res['category'] = $item->category();
          $res['brandName'] = $item->brand_owner();
          $res['productName'] = $item->productName();
          return $res;
        }));
      }
      return view('admin.trade_items.attribute_levels.index', compact('tradeItems'));
    }

    public function store(Request $request){
      foreach($request->trade_items as $tradeItemData){
        TradeItem::find($tradeItemData['id'])->labels()->sync($tradeItemData['labels']);
      }
      
      return response()->json('ok', 200);
    }
}
