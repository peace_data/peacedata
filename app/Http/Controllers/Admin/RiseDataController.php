<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\RiseData;
use Illuminate\Http\Request;
use App\Http\Requests\CsvImportRequest;
use App\Http\Requests\StoreRiseData;
use Maatwebsite\Excel\Facades\Excel;
use App\Builders\RiseData as Builder;
use App\Sorters\Sort;


class RiseDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {

        $keyword = $request->get('search');
        $perPage = 25;

        if($request->ajax()) {
            $result = RiseData::filter($request)->get();
            return response()->json($result->toArray());
        }


        if (!empty($keyword)) {
            $rise_data = RiseData::where('food_cat_1_name', 'LIKE', "%$keyword%")
                ->orWhere('food_cat_2_name', 'LIKE', "%$keyword%")
                ->orWhere('food_cat_3_name', 'LIKE', "%$keyword%")
                ->orWhere('region_name', 'LIKE', "%$keyword%");
        } else {
            $rise_data = RiseData::query();
        }

        $rise_data = $rise_data->sort($request);
        $rise_data = $rise_data->paginate($perPage);

        return view('admin.rise_data.index', compact('rise_data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.rise_data.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(StoreRiseData $request)
    {

        $requestData = $request->all();

        $obj = RiseData::create($requestData);
        $id = $obj->id;
        return redirect()->route('rise_datum.show', compact('id'))->with('flash_message', 'RiseData added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $rise_datum = RiseData::findOrFail($id);

        return view('admin.rise_data.show', compact('rise_datum'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $rise_datum = RiseData::findOrFail($id);

        return view('admin.rise_data.edit', compact('rise_datum'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreRiseData $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(StoreRiseData $request, $id)
    {

        $requestData = $request->all();

        $rise_datum = RiseData::findOrFail($id);
        $rise_datum->update($requestData);

        return redirect()
            ->route('rise_datum.index')
            ->with('flash_message', 'RiseData updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        RiseData::destroy($id);

        return redirect()
            ->route('rise_datum.index')
            ->with('flash_message', 'RiseData deleted!');
    }

    /**
     * @param CsvImportRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function upload(CsvImportRequest $request)
    {
        RiseData::truncate();
        $path = $request->file('csv_file')->getRealPath();
        $data_csv = Excel::load($path, function($reader) {})->get()->toArray();

        foreach($data_csv as $row) {
            $builder = new Builder();
            RiseData::create($builder->fromCsv($row));
        }

        return redirect()->route('rise_datum.index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function selected(Request $request)
    {
        $keyword = $request->get('search');
        $attribute = $request->get('type');


        $rise_data = RiseData::where($attribute, 'LIKE', "%$keyword%")
            ->groupBy($attribute)
            ->get([$attribute]);

        return response()->json($rise_data->toArray());
    }

    public function for_assigned_validoo_item(Request $request)
    {
        if($request->ajax()) {
            $rise_data = RiseData::get([
                    'id',
                    'food_cat_3_name',
                    'slv_name',
                    'region_name',
                    'ghg_total_value'
                ]);


            return response()->json($rise_data->toArray());
        }
    }
}
