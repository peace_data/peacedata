<?php
/**
 * Created by PhpStorm.
 * User: shesser
 * Date: 25.08.18
 * Time: 13:39
 */

namespace App\Http\Controllers\Admin\Validoo;

use App\Http\Controllers\Controller;
use App\Models\TradeItem;
use App\Models\Validoo\Nutrient;
use App\Models\TradeItemSubscription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\Builders\Validoo\TradeItemTree;
use App\Models\Validoo\TradeItem as ValidooItems;
use App\Validoo\Parsers\Xml\Validoo as ValidooXmlParser;


class TradeItemsController extends Controller
{
    public function index(Request $request)
    {
        $numbersItems = [];
        $keyword = $request->get('search');

        if (!empty($keyword)) {
            $tradeItems = TradeItem::where('gtin', 'LIKE', "%$keyword%");
        } else {
            $tradeItems = TradeItem::query();
        }

        $tradeItems = $tradeItems->with([
         'tradeItem.tradeItemHeader',
         'tradeItem.tradeItemHeader.tradeItemIdentity',
         'tradeItem.descriptions',
        ]);
        
        
        $numbersItems['tradeItem'] = $tradeItems->count();
        $numbersItems['validooTradeItem'] = ValidooItems::count();
        $numbersItems['validooTradeItemSubscriptions'] = TradeItemSubscription::count();

        $tradeItems =  $tradeItems->sort($request);

        $tradeItems = $tradeItems->paginate(25);

        return view('admin.validoo.trade_items.index')
            ->with(compact('tradeItems', 'numbersItems'));
    }

    public function show($id, Request $request)
    {
      $validooItem = TradeItem::find($id);
      if($request->ajax()){
        $tradeItemTree = new TradeItemTree($validooItem->validoo_trade_item_id);
        return $tradeItemTree->build()->getTree();
      }
      return view('admin.validoo.trade_items.show', [
        'tradeItem' => $validooItem,
      ]);
    }

    public function upload(Request $request){
      $xmlParser = new ValidooXmlParser(Input::file('xml')->openFile());
      $xmlParser->store();
      return redirect()->route('trade_items.index');
    }

}

