<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductCategoryLevel;
use App\Http\Requests\StoreProductCategoryLevelRequest;
use App\Models\TradeItem;

class ProductCategoryLevelsController extends Controller
{
    const PER_PAGE = 25;
    /**
     * Get Validoo items for making  category level
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {

        $validooItems = TradeItem::byNeedAssignedCategoryLevel()
            ->sort($request)
            ->with([
                'tradeItem' ,
                'tradeItem.tradeItemHeader',
                'tradeItem.descriptions',
                'tradeItem.tradeItemHeader.tradeItemIdentity',
                'riseData'
            ])->paginate(self::PER_PAGE);
        if($request->ajax()) {
            return response()->json($validooItems);
        }

        return view('admin.validoo.category_level.index', compact('validooItems'));
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id, Request $request)
    {
        if($request->ajax()) {
           ProductCategoryLevel::destroy($id);
           return response()->json(['message' => $id],200);
        }

    }

    /**
     * @param StoreProductCategoryLevelRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreProductCategoryLevelRequest $request)
    {
        // TODO to need refactor / move to another class
        if($request->ajax()) {
            $data = $request->get('data');
            foreach($data as $obj) {
                $item = TradeItem::find($obj['product']['id']);
                $item->food = $obj['product']['food'];
                $item->published = true;
                # TODO need refactor
                if($obj['category_id']){
                  if($item->productCategoryLevel){
                    $item->productCategoryLevel()->update(['product_id' => $item->id, 'category_id' => $obj['category_id']]);
                  }else{
                    ProductCategoryLevel::create(['product_id' => $item->id, 'category_id' => $obj['category_id']]);
                  }
                }
                $item->save();
            }

            return response()->json(['status' => 'ok'], 200);
        }
    }
}
