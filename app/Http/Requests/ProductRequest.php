<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'price'=> 'required', // Int
            'price_unit'=> 'required',
            'weight'=> 'required', // String
            'currency'=> 'required', // String
            'compare_price'=> 'required', // Int
            'compare_unit'=> 'required',
            'brand'=> 'required', // String
            'informations'=> 'required', // Array of Information classes
            'category'=> 'required', // String
            'isEko'=> 'required', // Int
            'external_key'=> 'required', // String
            'provider_id' => 'required'
        ];
    }
}
