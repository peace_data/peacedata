<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRiseData extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "food_cat_1_id" => 'required|integer',
            "food_cat_2_id" => 'required|integer',
            "food_cat_3_id" => 'required|integer',
            "food_cat_1_name" => 'required',
            "food_cat_2_name" => 'required',
            "food_cat_3_name" => 'required',
            'slv_id' => 'required|integer',
            'slv_name' => 'required',
            'activity_id' => 'required|integer',
            'activity_name' => 'required',
            'ghg_total_value' => 'required',
            'region_id' => 'required|integer',
            'region_name' => 'required',
            'region_source' => 'required',
            'production_type' => 'required',
            'production_type_source' => 'required'
        ];
    }
}
