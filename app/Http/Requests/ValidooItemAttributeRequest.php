<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidooItemAttributeRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
      return [
        "attribute_name" => 'required',
      ];
    }
}

