<?php
/**
 * Created by PhpStorm.
 * User: shesser
 * Date: 25.08.18
 * Time: 11:19
 */

namespace App;

use ErrorException;
use Illuminate\Database\Eloquent\Relations\Relation;
use ReflectionClass;
use ReflectionMethod;

trait RelationshipsTrait
{
    public function relationships($skip_belong_to = false) {

        $model = new static;

        $relationships = [];

        foreach((new ReflectionClass($model))->getMethods(ReflectionMethod::IS_PUBLIC) as $method)
        {
            
            if ($method->class != get_class($model) ||
                !empty($method->getParameters()) ||
                $method->getName() == __FUNCTION__) {
                continue;
            }

            try {
                $return = $method->invoke($model);
                if($skip_belong_to === true && get_class($return) ===  "Illuminate\Database\Eloquent\Relations\BelongsTo"){
                    continue;
                }
                if ($return instanceof Relation) {
                    $relationships[$method->getName()] = [
                        'type' => (new ReflectionClass($return))->getShortName(),
                        'model' => (new ReflectionClass($return->getRelated()))->getName()
                    ];
                }
            } catch(ErrorException $e) {
                
            }
        }

        return $relationships;
    }
}