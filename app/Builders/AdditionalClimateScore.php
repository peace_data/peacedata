<?php

namespace App\Builders;


class AdditionalClimateScore
{
    public function fromXlsx(Array $data)
    {
        return [
            'global_code' =>(int) $data['Global Code'],
            'global_name' => $data['Global Name'],
            'region_code' =>(int) $data['Region Code'],
            'region_name' => $data['Region Name'],
            'sub_region_code' =>(int) $data['Sub-region Code'],
            'sub_region_name' => $data['Sub-region Name'],
            'sub_region_score' =>(int) $data['Sub-region Score'],
            'intermediate_region_code' => (int)$data['Intermediate Region Code'],
            'intermediate_region_name' => $data['Intermediate Region Name'],
            'country_or_area' => $data['Country or Area'],
            'm49_code' => $data['M49 Code'],
            'iso_3166_1' => str_pad($data['M49 Code'], 3, "0", STR_PAD_LEFT),
            'iso_alpha3_code' => $data['ISO-alpha3 Code'],
            'country_score' => (int)$data['Country Score'],
        ];
    }
}
