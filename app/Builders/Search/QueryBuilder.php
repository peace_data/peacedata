<?php

namespace App\Builders\Search;

use App\Models\Concern\Elasticsearch\Package;
use App\Models\Concern\Elasticsearch\Measurement;

class QueryBuilder 
{

    public static function call($search_name, $measurement = null, $quantity = null){
        $words = explode(' ',$search_name);
        foreach($words as $key => $word){
            if($word == $quantity){
                unset($words[$key]);
            }
        }
        
        $res = array(
            'bool' => array(
                'must' => array(
                    'multi_match' => array(
                        "minimum_should_match"=> "20%",
                        'query' => join(' ',$words),
                        "type" => "most_fields",
                        'fuzziness' => "AUTO",
                        'fields' => ['products.category', 'products.title', 'products.brand'],
                    )
                ),
                'should' => array(
                    'multi_match' => array(
                        "minimum_should_match"=> "20%",
                        'query' => $search_name,
                        "type" => "most_fields",
                        'fuzziness' => "AUTO",
                        'fields' => ['measurement.title', 'measurement.aliases', 'package.title', 'package.aliases'],
                    )
                )
            ),
        );

        if(!empty($measurement)){
            if(is_null($quantity)){
                $quantity = 1;
            }
            $totalQuantity = $measurement->child_measurement_quantity * $quantity;
            $res['bool']['filter'] = array(
                "range" => array(
                    'compare_measurement' => array(
                        'lte' => $totalQuantity
                    ),
                ),
                             
            );
        }
        
        return $res;
    }

}


        