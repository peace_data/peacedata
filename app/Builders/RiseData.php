<?php
/**
 * Created by PhpStorm.
 * User: shesser
 * Date: 03.09.18
 * Time: 19:52
 */
namespace App\Builders;


/**
 * To need refactor
 * Class RiseData
 * @package App\Builders
 */
class RiseData
{
    public function fromCsv(Array $data)
    {
        return [
            'food_cat_1_id' =>(int) $data['FoodCat1ID'],
            'food_cat_1_name' => $data['FoodCat1Name'],
            'food_cat_2_id' =>(int) $data['FoodCat2ID'],
            'food_cat_2_name' => $data['FoodCat2Name'],
            'food_cat_3_id' =>(int) $data['FoodCat3ID'],
            'food_cat_3_name' => $data['FoodCat3Name'],
            'slv_id' =>(int) $data['SLVID'],
            'slv_name' => $data['SLVName'],
            'activity_id'=>(int) $data['ActivityID'],
            'activity_name'=> $data['ActivityName'],
            'region_id'=>(int) $data['RegionID'],
            'region_name'=> $data['RegionName'],
            'region_source'=> $data['RegionSource'],
            'production_type'=> $data['ProductionType'],
            'production_type_source'=> $data['ProductionTypeSource'],
            'ghg_total_value'=> floatval(str_replace(',', '.', $data['GHGTotalValue']))
        ];
    }
}