<?php

namespace App\Builders;

class Measurement
{
    public function fromCsv(Array $data)
    {
        return [
            'title' =>$data['Namn'],
            'description' => $data['Beskrivning'],
            'code' =>$data['Kod'],
            'isUsedInSweden' =>$data['Används i Sverige'] == 'X',
            'child_measurement' => (string) $data['child_measurement'],
            'child_measurement_quantity' => (int)$data['child_measurement_quantity'],
        ];
    }
}