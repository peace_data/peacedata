<?php

namespace App\Builders\Validoo;

use App\Models\Validoo\TradeItem;

class TradeItemTree
{

  private $relations;
  private $tradeItem;
  private $tree;

  public function __construct($tradeItemId){
    $this->tradeItem = TradeItem::find($tradeItemId);
  }

  public function build(){

    if($this->tradeItem === null) {
        return [];
    }

    $this->buildRelationTree();
    $tradeItem = TradeItem::with($this->relations)->find($this->tradeItem->id);
    $this->tree = $this->makeJson($tradeItem);

    return $this;
  }

  public function getTree(){
    return $this->tree;
  }

  private function makeJson($instance, $name = 'TradeItem')
  {
      $res = [];
      if(is_a($instance, 'Illuminate\Database\Eloquent\Collection')){
          $res['class_name'] = $name;
          $res['children'] = [];
          foreach($instance as $item) {
            $tmp = [
                'class_name'=> class_basename($item),
                'attributes' => $item->getAttributes(),
            ];
            $tmp['children'] = [];

            foreach($item->relationships() as $child => $v){

                if(!$item->$child->isEmpty()){
                    $tmp['children'][] = $this->makeJson($item->$child, $child);
                }
            }
            if(!empty($tmp)){
                $res['children'][] = $tmp;
            }            
          }
      }else{
          $res = [
              'class_name'=> class_basename($instance),
              'attributes' => $instance->getAttributes(),
          ];
          $res['children'] = [];
          foreach($instance->getRelations() as $name => $child){
              if(!empty($child)){
                  $res['children'][] = $this->makeJson($child, $name);
            }
          }
      }
      if(!$res) { return [];  }

      return $res;
  }


  private function buildRelationTree(){
    $this->relations = array_keys($this->tradeItem->relationships(true));

    foreach($this->getRelations($this->tradeItem) as $item){
        array_push($this->relations, $item);
    };
  }


  private function getRelations($model)
  {
      $res =[];
      $rels  = $model->relationships(true);


      if(count($rels) == 0) {
          return [];
      }

      foreach($rels as $key=>$val) {
          if($model->$key === null ) {
              continue;
          }

          try {
              $relations =  array_keys($model->$key->relationships(true));
              foreach($relations as $relation){
                  array_push($res,$key.'.'.$relation);
              }
          }catch(\BadMethodCallException $e) {

          }
      }
      return $res;
  }



}
