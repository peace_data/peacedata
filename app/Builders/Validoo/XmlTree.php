<?php

namespace App\Builders\Validoo;

use App\Models\Validoo\TradeItem;
use App\Models\Validoo\Temperature;

class XmlTree
{

  private $tree;
  private $xmlData;

  public function __construct($xmlData){
    $this->xmlData = $xmlData;
    $this->models = get_list_models_relations(true);
    $this->tree = $this->buildTree(new TradeItem);
  }

  public function getTree(){
    return $this->tree;
  }

  public function gtin(){
    return $this->xmlData['TradeItemIdentity']['gtin'];
  }

  private function buildTree($node){
    $res = [];
    $nodeRelations = $node->relationships();
    $nodeClassName = class_basename($node);
    $res = $this->getXmlData($nodeClassName);
    foreach($nodeRelations as $relation => $model){
      $modelName = $model['model'];
      if($model['type'] != 'BelongsTo'){
        $res[$modelName] = $this->getXmlData($this->findModelName($model['model']));

        $children = $this->models[$this->findModelName($model['model'])];
        foreach ($children as $childRelation => $child) {
          if($child['type'] != 'BelongsTo'){
            $xmlData = $this->getXmlData($this->findModelName($child['model']));
            if(!$this->isAssocArr($xmlData)){
              $res[$modelName][$child['model']] = $xmlData;
            }else{
              $res[$modelName][$child['model']] = $this->buildTree(new $child['model']);
            }  
          }
        }
      }

    }
    return $res;
  }

  private function findModelName($str){
    $modelNameArr = explode('\\', $str);
    return end($modelNameArr);
  }

  private function getXmlData($className){
    try{
      return $this->xmlData[$className];
    }catch(\ErrorException $e){
      // var_dump($className);
    }

  }
  private function isAssocArr($arr){
    return array_keys($arr) !== range(0, count($arr) - 1);
  }

}
