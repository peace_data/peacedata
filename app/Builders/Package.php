<?php

namespace App\Builders;

class Package
{
    public function fromCsv(Array $data)
    {
        return [
            'title' =>$data['Title'],
            'description' => $data['Description'],
            'code' =>$data['Code'],
            'isUsedInSweden' =>$data['Relevant'] == 'yes',
            'aliases' => explode(',',$data['Alias'])
        ];
    }
}