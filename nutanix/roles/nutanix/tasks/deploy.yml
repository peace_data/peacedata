---

    - name: set hostname
      hostname:
        name: "{{ inventory_hostname }}"

    - name: set /etc/hosts
      lineinfile:
        dest: "/etc/hosts"
        regexp: '.*{{ host }}$'
        line: "{{ hostvars[host].ansible_host }} {{ host }}"
        state: present
      when: hostvars[host].ansible_default_ipv4.address is defined
      loop: "{{ groups['deployment_vms'] }}"
      loop_control:
        loop_var: host
        label: "{{ host }}"

    - name: set root password
      user:
        name: root
        password: "{{ '%s' | format(vms_root_password) | password_hash('sha512') }}"

    - name: copy authorized_keys for root
      copy:
        src: "~/.ssh/id_rsa.pub"
        dest: "/root/.ssh/authorized_keys"
        mode: 0600

    - name: enable ssh for root
      lineinfile:
        dest: "/etc/ssh/sshd_config"
        regexp: "^[#]*PermitRootLogin"
        line: "PermitRootLogin yes"
        state: present

    - name: enable password authentication
      lineinfile:
        dest: "/etc/ssh/sshd_config"
        regexp: "^[#]*PasswordAuthentication"
        line: "PasswordAuthentication yes"
        state: present
      notify:
        - restart-sshd

###
#
# OS-dependent tasks
#
###

    - name: include os dependent variables
      include_vars: "{{ item }}"
      with_first_found:
        - "{{ ansible_os_family | lower }}.yml"

    - name: rhel setup
      include_role:
        name: rhel
      when: ansible_os_family == "RedHat"

    - name: install packages
      package:
        name: "{{ package }}"
        update_cache: yes
      register: result
      retries: 5
      delay: 5
      until: result is succeeded
      loop: "{{ packages }}"
      loop_control:
        loop_var: package
        label: "{{ package }}"
      