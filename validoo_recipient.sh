#!/usr/bin/env bash

#Write log to /var/log/syslog
logger -s "File name $1"

cd /var/www/
mv /tmp/validoo_store/$1 /var/www/storage/app/validoo/
/usr/local/bin/php artisan run:validoo_recipient $1


#Write returned code lto /var/log/syslog
logger -s $?
