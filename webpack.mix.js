let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/validoo_item_category_level.js', 'public/js')
   .js('resources/assets/js/trade_item_attribute_level.js', 'public/js')
   .js('resources/assets/js/trade_items/attribute_levels.js', 'public/js/trade_items')
   .js('resources/assets/js/public/products/search.js', 'public/js/public/products/search.js')


mix.sass('resources/assets/sass/content.scss', 'public/css/admin/app.css')
