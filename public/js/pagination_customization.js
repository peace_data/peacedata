var customPagination = (new function(){
  var sendBtnSelector = '.pagination-page-input .change-page';
  var inputSelector = '.pagination-page-input .page-value';
  function reloadPage(page){
    var url_string = window.location.href; //window.location.href
    var url = new URL(url_string);
    var c = url.searchParams.set("page", page);
    window.location.href = url;
  }

  this.init = function(){
    $(document).on('click', sendBtnSelector, function(){
      reloadPage($(inputSelector).val())
    })
    $(document).on('keydown', inputSelector, function(e) {
      if (e.which == 13) {
          e.preventDefault();
          reloadPage(e.target.value)
      }
    });
  }
  this.init();
}())
