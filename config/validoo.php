<?php
/**
 * Created by PhpStorm.
 * User: shesser
 * Date: 26.09.18
 * Time: 17:13
 */


$config = [];

if(env('VALIDOO_ENVIRONMENT') === 'Production') {
    $config = [
      'endpoint' => 'https://mediastore.validoo.se/WSMediaStore/SyncMediaSubscriptionWS.svc/basichttp',
      'entity_code' => '7300009059959',
      'create_soap_action' => 'http://www.saphety.com/SyncMediaSubscriptionWS/CreateSubscription',
      'search_soap_action' => 'http://www.saphety.com/SyncMediaSubscriptionWS/SearchSubscriptions',
      'delete_soap_action' => 'http://www.saphety.com/SyncMediaSubscriptionWS/DeleteSubscription',
    ];
}elseif(env('VALIDOO_ENVIRONMENT') === 'Pre-Production'){
    $config = [
        'endpoint' => 'https://mediastore-preprod.validoo.se/WSMediaStore/SyncMediaSubscriptionWS.svc/basichttp',
        'entity_code' => '7381020800003',
        'create_soap_action' => 'http://www.saphety.com/SyncMediaSubscriptionWS/CreateSubscription',
        'search_soap_action' => 'http://www.saphety.com/SyncMediaSubscriptionWS/SearchSubscriptions',
        'delete_soap_action' => 'http://www.saphety.com/SyncMediaSubscriptionWS/DeleteSubscription',
    ];
}else {
//    throw new \Exception('Validoo environment does not set or is incorrect');
}


return [
  'user_name' => env('VALIDOO_USER_NAME'),
  'password'  => env('VALIDOO_PASSWORD'),

//    Our GLN on validoo service
  'entity_code' => $config['entity_code'],

  'subscription' => [
      'endpoint' => $config['endpoint'],

      'create' => [
          'soap_action' => $config['create_soap_action']
      ],

      'search' => [
          'soap_action' => $config['search_soap_action']
      ],

      'delete' => [
          'soap_action' => $config['delete_soap_action']
      ]
  ],

  'stores' => [
      'sftp_store_path' => env('VALIDOO_CONTAINER_STORE_PATH'),
      'archive_store_path' => env('VALIDOO_CONTAINER_ARCHIVE_PATH')
  ]
];
